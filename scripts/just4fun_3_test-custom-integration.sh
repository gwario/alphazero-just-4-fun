#!/bin/sh

OUTPUT_DIR="sessions"
TIMESTAMP=$(date -uI'seconds')
HOSTNAME=$(hostname)

OUTPUT="./"$OUTPUT_DIR"/"$(basename $0)"_"$HOSTNAME"_"$TIMESTAMP".txt"
TIME_FORMAT=$(cat ./scripts/time-format.txt)

mkdir -p $OUTPUT_DIR
# Test targets
# (none = all)
# framework
# unit
# integration
# dummy-run
# grad-updates
#
# Modes
# (none = original)
# benchmark
# time-allocated
# profile { text | html | svg | interactive }
#
# /bin/time -f "$TIME_FORMAT" -o "$OUTPUT" julia --compiled-modules=no --project -e 'import Pkg; Pkg.test("Just4Fun"; test_args = ARGS)' [<test_target> [<mode> [<mode param>]]] 

JULIA_PARAMETERS="--code-coverage=user"
MODE_PARAMETERS=""

if [ "$1" = "mem-analysis" ]
then
    JULIA_PARAMETERS=$JULIA_PARAMETERS" --track-allocation=user --inline=no"
    echo "Collecting memory allocation data... Expect this task to run much slower!"    2>&1 | tee --append "$OUTPUT"
    echo "Ignoring mode parameters"                                                     2>&1 | tee --append "$OUTPUT"
else
    MODE_PARAMETERS=$@
fi


echo "Starting script exection at "$TIMESTAMP                                   2>&1 | tee --append "$OUTPUT"
echo "Instantiating..."                                                         2>&1 | tee --append "$OUTPUT"
~/bin/julia --project \
    -e 'import Pkg; Pkg.instantiate();'                                         2>&1 | tee --append "$OUTPUT"
echo "Running..."                                                               2>&1 | tee --append "$OUTPUT"
unbuffer ~/bin/julia --project test/clean_coverage.jl                           2>&1 | tee --append "$OUTPUT"
( /bin/time --verbose unbuffer ~/bin/julia --project \
    $JULIA_PARAMETERS \
    -e 'import Just4Fun; Just4Fun.run_tests()' integration $MODE_PARAMETERS )   2>&1 | tee --append "$OUTPUT"
unbuffer ~/bin/julia --project test/display_coverage.jl                         2>&1 | tee --append "$OUTPUT"