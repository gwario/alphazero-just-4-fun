#!/bin/sh

OUTPUT_DIR="sessions"
TIMESTAMP=$(date -uI'seconds')
HOSTNAME=$(hostname)

OUTPUT="./"$OUTPUT_DIR"/"$(basename $0)"_"$HOSTNAME"_"$TIMESTAMP".txt"
TIME_FORMAT=$(cat ./scripts/time-format.txt)


mkdir -p $OUTPUT_DIR

~/bin/julia --project -e 'import Pkg; Pkg.instantiate();' 2>&1 | tee --append "$OUTPUT"

echo "Start a command interpreter to explore the internals of a player
through interactive play.

The 'memory' argument is an optional reference to a memory buffer.
When provided, additional state statistics are displayed.

# Commands

The following commands are currently implemented:

  - 'do [action]': make the current player perform 'action'.
    By default, the action of highest score is played.
  - 'explore [num_sims]': run 'num_sims' MCTS simulations from the current
    state (for MCTS players only).
  - 'state { <>(load) | load | save }': Writes or reads the state of the game.
  - 'memory { <>(describe) | describe | clear | add | self-play }': Manupulation of the memory.
  - 'training { <>(batch_size <>) | batch_size { <> | <i> } | train { <> | <i> } }': Performing training iterations using the samples in memory.
  - 'reset': Resets the MCTS tree.
  - 'go': query the user for a state description and go to this state.
  - 'flip': flip the board according to a random symmetry.
  - 'undo': undo the effect of the previous command.
  - 'restart': restart the explorer.
" 2>&1 | tee --append "$OUTPUT"

# unbuffered not working if stdin is required!
#( /bin/time --verbose unbuffer julia --project -e 'import Just4Fun; Just4Fun.run()' explore $@ ) 2>&1 | tee --append "$OUTPUT"
( /bin/time --verbose ~/bin/julia --project -e 'import Just4Fun; Just4Fun.run()' explore $@ ) 2>&1 | tee --append "$OUTPUT"