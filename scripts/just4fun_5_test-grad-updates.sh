#!/bin/sh

OUTPUT_DIR="sessions"
TIMESTAMP=$(date -uI'seconds')
HOSTNAME=$(hostname)

OUTPUT="./"$OUTPUT_DIR"/"$(basename $0)"_"$HOSTNAME"_"$TIMESTAMP".txt"
TIME_FORMAT=$(cat ./scripts/time-format.txt)


mkdir -p $OUTPUT_DIR
# Test targets
# (none = all)
# framework
# unit
# integration
# dummy-run
# grad-updates
#
# Modes
# (none = original)
# benchmark
# time-allocated
# profile { text | html | svg }
#
# /bin/time -f "$TIME_FORMAT" -o "$OUTPUT" julia --compiled-modules=no --project -e 'import Pkg; Pkg.test("Just4Fun"; test_args = ARGS)' [<test_target> [<mode> [<mode param>]]] 

~/bin/julia --project -e 'import Pkg; Pkg.instantiate();' 2>&1 | tee --append "$OUTPUT"

( /bin/time --verbose unbuffer ~/bin/julia --project -e 'import Just4Fun; Just4Fun.run_tests()' grad-updates $@ ) 2>&1 | tee --append "$OUTPUT"