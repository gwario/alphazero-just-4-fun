#!/bin/sh

OUTPUT_DIR="sessions"
TIMESTAMP=$(date -uI'seconds')
HOSTNAME=$(hostname)

OUTPUT="./"$OUTPUT_DIR"/"$(basename $0)"_"$HOSTNAME"_"$TIMESTAMP".txt"
TIME_FORMAT=$(cat ./scripts/time-format.txt)

ANALYSIS_PARAMS="--track-allocation=user --inline=no"

mkdir -p $OUTPUT_DIR

JULIA_PARAMETERS=""
MODE_PARAMETERS=""

if [ "$1" = "mem-analysis" ]
then
    JULIA_PARAMETERS=$JULIA_PARAMETERS" --track-allocation=user --inline=no"
    echo "Collecting memory allocation data... Expect this task to run much slower!"    2>&1 | tee --append "$OUTPUT"
    echo "Ignoring mode parameters"                                                     2>&1 | tee --append "$OUTPUT"
else
    MODE_PARAMETERS=$@
fi


echo "Starting script exection at "$TIMESTAMP                       2>&1 | tee --append "$OUTPUT"
echo "Instantiating..."                                             2>&1 | tee --append "$OUTPUT"
~/bin/julia --project \
    -e 'import Pkg; Pkg.instantiate();'                             2>&1 | tee --append "$OUTPUT"
echo "Running..."                                                   2>&1 | tee --append "$OUTPUT"
unbuffer ~/bin/julia --project test/clean_coverage.jl               2>&1 | tee --append "$OUTPUT"
( /bin/time --verbose unbuffer ~/bin/julia --project \
    $JULIA_PARAMETERS \
    -e 'import Just4Fun; Just4Fun.run()' train $MODE_PARAMETERS )   2>&1 | tee --append "$OUTPUT"
# TODO: info which log file to copy to the session dir
