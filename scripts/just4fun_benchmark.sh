#!/bin/sh

OUTPUT_DIR="sessions"
TIMESTAMP=$(date -uI'seconds')
HOSTNAME=$(hostname)

OUTPUT="./"$OUTPUT_DIR"/"$(basename $0)"_"$HOSTNAME"_"$TIMESTAMP".txt"
TIME_FORMAT=$(cat ./scripts/time-format.txt)


mkdir -p $OUTPUT_DIR

~/bin/julia --project -e 'import Pkg; Pkg.instantiate();' 2>&1 | tee --append "$OUTPUT"

# unbuffered not working if stdin is required!
#( /bin/time --verbose unbuffer julia --project -e 'import Just4Fun; Just4Fun.run()' play $@ ) 2>&1 | tee "$OUTPUT"
( /bin/time --verbose ~/bin/julia --project -e 'import Just4Fun; Just4Fun.run()' benchmark $@ ) 2>&1 | tee --append "$OUTPUT"