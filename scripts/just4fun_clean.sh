#!/bin/sh

find ./sessions/ -type f -print
find . -type f -name '*.cov' -print
find . -type f -name '*.info' -print
find . -type f -name '*.mem' -print
echo -n "Delete all the above files (sessions, logs and test coverage) (y/n)? "

read answer
# if echo "$answer" | grep -iq "^y" ;then
if [ "$answer" != "${answer#[Yy]}" ] ;then # this grammar (the #[] operator) means that the variable $answer where any Y or y in 1st position will be dropped if they exist.
    echo Yes
    sleep 3s

    rm -rv ./sessions/*
    find . -type f -name '*.cov' -print -delete
    find . -type f -name '*.info' -print -delete
    find . -type f -name '*.mem' -print -delete
fi

