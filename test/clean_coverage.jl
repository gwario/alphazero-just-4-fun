include("utils/utils.jl")

@info ""
@info "Cleaning up coverage info..."
@info ""

custom_clean_folder("src")
custom_clean_folder("test")

@info ""