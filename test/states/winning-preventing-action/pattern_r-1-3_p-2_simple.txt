# For Red
#------------------------------------------------
#     1 |    14 |    30 |    24 |    19 |     8 |
# (0,0) | (0,0) | (1,0) | (1,0) | (1,0) | (0,0) |
#------------------------------------------------
#    33 |    11 |     9 |    16 |    35 |    21 |
# (0,0) | (0,0) | (0,0) | (0,0) | (0,0) | (0,0) |
#------------------------------------------------
#     6 |    27 |    31 |    20 |     3 |    12 |
# (0,0) | (0,1) | (0,0) | (0,0) | (0,0) | (0,0) |
#------------------------------------------------
#    15 |    32 |     5 |    29 |    17 |    26 |
# (0,0) | (0,0) | (0,0) | (0,0) | (0,0) | (0,0) |
#------------------------------------------------
#    22 |    10 |    18 |    36 |    25 |     2 |
# (0,0) | (0,0) | (0,1) | (0,0) | (0,0) | (0,0) |
#------------------------------------------------
#    28 |     7 |    23 |     4 |    13 |    34 |
# (1,1) | (0,0) | (0,0) | (0,0) | (0,0) | (0,0) |
#------------------------------------------------
#
## Fields ###############
#- column 1 ---------
0 0 0 0 0 1
0 0 0 0 0 1
#- column 2 ---------
0 0 0 0 0 0
0 0 1 0 0 0
#- column 3 ---------
1 0 0 0 0 0
0 0 0 0 1 0
#- column 4 ---------
1 0 0 0 0 0
0 0 0 0 0 0
#- column 5 ---------
1 0 0 0 0 0
0 0 0 0 0 0
#- column 6 ---------
0 0 0 0 0 0
0 0 0 0 0 0
## Hands ###############
# Yellow
1 4 10 10
# Red
1 4 10 10
## stack ###############
9 10 11 12 13 14 15 16 17 18 19
## Used cards###########
1 2 3 4 5 6 7 8 9 1 11 12
########################
#
# Red should want to prevent the pattern by placing at 1, 14, 30, 24 (or 19)
#