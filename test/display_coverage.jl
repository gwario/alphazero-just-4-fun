using Logging
using Coverage

include("utils/utils.jl")

const COVERAGE_WARN_PCT = 75

# process '*.cov' files
#coverages = process_folder("src") # defaults to src/; alternatively, supply the folder name as argument
# process '*.info' files, if you collected them
#coverages = merge_coverage_counts(
#    coverages,
#    filter!(
#        let prefixes = (joinpath(pwd(), "src", ""))
#            c -> any(p -> startswith(c.filename, p), prefixes) 
#        end,
#        LCOV.readfolder("src")
#    )
#)

allocations = analyze_malloc("src")
@info "============================================================"
@info ""
if isempty(allocations)
    @info "No allocation data found!"
else
    @info "Memory allocations"
    @info ""
    for malloc in filter(alloc -> alloc.bytes > 0, allocations)
        print_allocation(malloc)
    end
end
@info ""

isFileToIgnore(filename) = contains(filename, "/params/") || endswith(filename, "experiments.jl")

#coverage = process_folder()
#mkpath("coverage")
#LCOV.writefile("coverage/coverage-lcov.info", coverage)
coverages = process_folder("src")
@info "============================================================"
@info ""
if isempty(coverages)
    @info "No line coverage data found!"
else
    @info "Line Coverage"
    @info ""
    for coverage in coverages
        
        if isFileToIgnore(coverage.filename)
            @info "Skipping $(coverage.filename)"
            continue
        end

        covered_lines, total_lines = get_summary(coverage)
        
        total_lines != 0 || continue
    
        line_coverage_pct = 100 * covered_lines / total_lines
    
        if line_coverage_pct < COVERAGE_WARN_PCT
            @warn "$(coverage_fmt(coverage)) coverage in $(coverage.filename)" _file=nothing _line=nothing _module=nothing
        else
            @info "$(coverage_fmt(coverage)) coverage in $(coverage.filename)"
        end
    end
    @info ""
    coverages = filter(coverage -> !isFileToIgnore(coverage.filename), coverages)
    covered_lines_total, total_lines_total = get_summary(coverages)
    line_coverage_pct_total = 100 * covered_lines_total / total_lines_total
    
    if line_coverage_pct_total < COVERAGE_WARN_PCT
        @warn "Overall test coverage: $(coverage_fmt(coverages))" _file=nothing _line=nothing _module=nothing
    else
        @info "Overall test coverage: $(coverage_fmt(coverages))"
    end
end
@info ""