using JSON3

"""
The stats for each each player of game
"""
struct EvaluationStats
    legend :: String
    # actions per player
    action_regular  :: Vector{Vector{Int64}}
    action_redraw   :: Vector{Vector{Int64}}
    result          :: Vector{GameState}
end

# stats of one evaluation, i.e. duel (game)
const BenchmarkStats = Vector{EvaluationStats}


for T in [
    EvaluationStats
] @eval JSON3.StructType(::Type{<:$T}) = JSON3.Struct()
end