using ProgressMeter
using AlphaZero: Report, Benchmark, UserInterface, Log

function run_benchmark(session::Session)
    report = Report.Benchmark()
    benchs = BenchmarkStats()
    for duel in session.benchmark
        outcome, bench = run_duel(session.env, duel, logger=session.logger)
        push!(report, outcome)
        push!(benchs, bench)
    end
    return (report, benchs)
end

function run_duel(env::Env, duel; logger)
    if isa(duel, Benchmark.Duel)
      player_name = Benchmark.name(duel.player)
      baseline_name = Benchmark.name(duel.baseline)
      legend = "$player_name against $baseline_name"
    else
      @assert isa(duel, Benchmark.Single)
      legend = Benchmark.name(duel.player)
    end
    Log.section(logger, 2, "Running benchmark: $legend")
    progress = Log.Progress(logger, duel.sim.num_games)
    report, stats = run(env, duel, progress)
    UserInterface.show_space_after_progress_bar(logger)
    UserInterface.print_report(
        logger, report, ternary_rewards=env.params.ternary_rewards)
    print_stats(logger, stats)
    return (report, stats)
end

# Copy of AlphaZero.jl to retrieve samples for custom stats
function run(env::Env, eval::Benchmark.Evaluation, progress=nothing)
    net() = Network.copy(env.bestnn, on_gpu=eval.sim.use_gpu, test_mode=true)
    if isa(eval, Benchmark.Single)
        simulator = Simulator(net, record_trace) do net
            Benchmark.instantiate(eval.player, env.gspec, net)
        end
    else
        @assert isa(eval, Benchmark.Duel)
        simulator = Simulator(net, record_trace) do net
            player = Benchmark.instantiate(eval.player, env.gspec, net)
            baseline = Benchmark.instantiate(eval.baseline, env.gspec, net)
            return TwoPlayers(player, baseline)
        end
    end
    samples, elapsed = @timed simulate(simulator, env.gspec, eval.sim, game_simulated=(() -> next!(progress)))

    actions_regular = []
    actions_redraw = []
    results = []
    for duel in samples
        trace, _ = duel
        states = trace.states
        per_game_actions_regular = repeat([0], NUM_PLAYERS)
        per_game_actions_redraw = repeat([0], NUM_PLAYERS)
        result = states[end].state
        for  state in states[2:end]
            # first is initial - no action
            action_index = state.action_indices[end]
            action = ACTIONS[action_index]
            player = previous_player(env.gspec, state.curplayer)
            player_index = to_index(player)
            if isredraw(action)
                per_game_actions_redraw[player_index] += 1
            else
                per_game_actions_regular[player_index] += 1
            end
        end
        push!(actions_redraw, per_game_actions_redraw)
        push!(actions_regular, per_game_actions_regular)
        push!(results, result)
    end
    
    gamma = env.params.self_play.mcts.gamma
    rewards, redundancy = rewards_and_redundancy(samples, gamma=gamma)
    return (
        Report.Evaluation(Benchmark.name(eval), AlphaZero.mean(rewards), redundancy, rewards, nothing, elapsed),
        EvaluationStats(Benchmark.name(eval), actions_regular, actions_redraw, results)
    )
end
