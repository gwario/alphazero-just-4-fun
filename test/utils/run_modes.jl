#using Profile
#using StatProfilerHTML
#using ProfileSVG
#using ProfileView
#using BenchmarkTools
using Dates


const TIMESTAMP = Dates.format(Dates.now(), "yyyy-mm-ddTHH:MM:SS+00:00")
const HOSTNAME = gethostname()
const SESSIONS_PATH = joinpath(pwd(), "sessions")


function get_session_dir(experiment)::String
    joinpath(SESSIONS_PATH, experiment.name)
end

function get_output_file(target, mode, ext)::String
    target_name = String(Symbol(target))
    "$(target_name)_$(mode)_$(HOSTNAME)_$(TIMESTAMP).$(ext)"
end

function get_output_dir(target, mode)::String
    target_name = String(Symbol(target))
    "$(target_name)_$(mode)_$(HOSTNAME)_$(TIMESTAMP)"
end

function run_mode_benchmark(target, experiment, param)
    @assert false "Functionality has been deactivated!"
#
#    out_file_path = joinpath(get_session_dir(experiment), get_output_file(target, "benchmark", "txt"))
#    io_stream = open(out_file_path, "w")
#    io_context = IOContext(io_stream, :histmin=>0.5, :histmax=>8, :logbins=>true)
#    
#    n_evaluations = param != "" ? parse(Int64, param) : 2
#    
#    @info "Performing $n_evaluations evaluations..."
#    @show experiment
#    b = @benchmark target(data) setup=(data=experiment) evals=n_evaluations
#    show(io_context, MIME("text/plain"), b)
#    close(io_stream)
#
#    @info "Results saved to $out_file_path"
end

function run_mode_profile(target, experiment, param)
    @assert false "Functionality has been deactivated!"
#
#    Profile.clear()
#    Profile.init(delay=0.1)
#
#    if param == "html"
#
#        out_path = joinpath(get_session_dir(experiment), get_output_dir(target, "profile-html"))
#        
#        @profile target(experiment)
#        StatProfilerHTML.statprofilehtml(; path=out_path)
#
#        @debug "Results saved to $out_path/index.html"
#        
#    elseif param == "interactive"
#
#        ProfileView.@profview target(experiment)
#
#    elseif param == "svg"
#        
#        out_file_path = joinpath(get_session_dir(experiment), get_output_file(target, "profile", "svg"))
#
#        ProfileSVG.@profview target(experiment)
#        ProfileSVG.save(out_file_path)
#        
#        @info "Saving results to $out_file_path"
#
#    elseif param == "text"
#
#        out_file_path = joinpath(get_session_dir(experiment), get_output_file(target, "profile", "txt"))
#        
#        @profile target(experiment)
#
#        io_stream = open(out_file_path, "w")
#        io_context = IOContext(io_stream, :displaysize => (24, 500))
#
#        Profile.print(io_context, combine=true, mincount=3, maxdepth=20)
#        
#        close(io_stream)
#        @info "Results saved to $out_file_path"
#
#    else
#        @error "Unknown mode param '$param' use text, svg or html!"
#    end
end

function run_mode_time_allocated(target, experiment, param)
    @assert false "Functionality has been removed!"
#    TimerOutputs.enable_debug_timings(Just4Fun)
#    @timeit_debug "run_mode_plain" begin
#        target(experiment)
#    end
#    print_timer(IOContext(stdout, :displaysize => (24, 500)), TimerOutputs.get_defaulttimer())
end

function execute_op_in(operation, experiment, mode, param)

    if mode == "benchmark"
        run_mode_benchmark(operation, experiment, param)
    elseif mode == "time-allocated"
        run_mode_time_allocated(operation, experiment, param)
    elseif mode == "profile"
        run_mode_profile(operation, experiment, param)
    else
        operation(experiment)
    end
end
