using AlphaZero: UserInterface

"""
    dummy_run(experiment; args...)::Session

Clone of Scripts.dummy_run that returns the session
"""
function dummy_run(e::Experiment; args...)
  de = Scripts.dummy_run_experiment(e)
  dir = "sessions/dummy-$(de.name)"
  rm(dir, force=true, recursive=true)
  session = Session(de; dir, nostdout=false)
  resume!(session)
  return session
end


"""
    test_grad_updates(experiment; [num_games])

Clone of Scripts.test_grad_updates that returns the session
"""
function test_grad_updates(exp::Experiment; num_games=5000)
  session = Session(exp, autosave=false, dir="sessions/grad-updates-$(exp.name)")
  UI.Log.section(session.logger, 1, "Generating $num_games some random traces")
  for i in 1:num_games
    trace = play_game(exp.gspec, RandomPlayer())
    AlphaZero.push_trace!(session.env.memory, trace, 1.0)
  end
  UI.Log.section(session.logger, 1, "Starting gradient updates")
  AlphaZero.learning_step!(session.env, session)
  return session
end

"""
    train(experiment; [dir, autosave, save_intermediate])::Session

Clone of Scripts.train that returns the session
"""
function train(e::Experiment; args...)
    session = Session(e; args...)
    UserInterface.resume!(session)
    return session
end

"""
    explore(experiment; [dir])::Session

Clone of Scripts.explore that returns the session
"""
function explore(e::Experiment; args...)
    session = Session(e; args...)
    explore(session; mcts_params=session.env.params.self_play.mcts)
    return session
end

"""
    play(experiment; [dir])

Clone of Scripts.play that returns the session
"""
function play(e::Experiment; args...)
    session = Session(e; args...)
    if GI.two_players(e.gspec)
        interactive!(session.env.gspec, AlphaZeroPlayer(session), Human())
    else
        interactive!(session.env.gspec, Human())
    end
    return session
end

function run_benchmark(e::Experiment; args...)
    session = Session(e; args...)
    for duel in session.benchmark
        run_duel(session.env, duel, logger=session.logger)
    end
    return session
end