using Printf

"""
load_state(gspec::Just4FunSpec, exp::Explorer)::Bool

Loads a state.
"""
function load_state(gspec::Just4FunSpec, exp::Explorer)::Bool

    st = GI.read_state(gspec)

    if isnothing(st)
        println("Invalid state description.")
        return false
    end
    g = GI.init(gspec, st)
    if !isnothing(st)
        UI.save_game!(exp)
        exp.game = g
        exp.turn = 0
        return true
    end
end

"""
write_state(spec::Just4FunSpec, exp::Explorer)::Bool

Writes the state to the standard output or a file.
"""
function write_state(spec::Just4FunSpec, exp::Explorer)::Bool
    
    valid_command(input::String) = input != "" ? input != "yes" ? input != "no" ? false : true : true : true
    
    print("Enter interacive mode? (yes/NO) ")
    input = "-"
    while !valid_command(input)
        input = lowercase(strip(readline()))
    end
 
    if input == "yes"
        state = write_state_interactive(spec, exp.game)
    elseif isempty(input) || input == "no"
        state = write_state_non_interactive(spec, exp.game)
    end
    return !isnothing(state)
end

"""
set_batch_size(spec::Just4FunSpec, session::Session, args)::Bool

Sets the batch size in the session's environment's memory.
"""
function set_batch_size(spec::Just4FunSpec, session::Session, args)::Bool
    try
        new_batch_size = parse(Int, args[2])
        if new_batch_size > 0
            session.env.memory.cur_batch_size = new_batch_size
            @printf "Batch size set to %i\n" session.env.memory.cur_batch_size
            return true
        end
    catch
    end
    println("Invalid batch size!")
    return false # invalid command, do nothing
end

"""
get_action(spec::Just4FunSpec, exp::Explorer, stats, args)::Union{Any, Nothing}

Returns the entered action from args or returns the action with the highest
probability if no action was supplied.
If the supplied action is invalid, nothing is returned.
"""
function get_action(spec::Just4FunSpec, exp::Explorer, stats, args)::Union{Any, Nothing}
    if length(args) == 0
        a = stats.actions[1][1]
    elseif 1 <= length(args) && length(args) <= 4
        a = GI.parse_action(GI.spec(exp.game), join(args, CARD_ACTION_SEPARATOR))
    end

    if a ∈ GI.available_actions(exp.game)
        return a
    else
        return nothing
    end
end

"""
get_n_iters(spec::Just4FunSpec, exp::Explorer, args)

Returns the supplied number of iterations or if nothing was supplied the
player's `niters`.
"""
function get_n_iters(spec::Just4FunSpec, exp::Explorer, args)
    try
        if isempty(args)
            n = exp.player.niters
        else
            n = parse(Int, args[1])
        end
        return n
    catch e
        isa(e, ArgumentError) && (return false)
        rethrow(e)
    end
end