using AlphaZero: UI, GI, Log, MCTS, Report
using AlphaZero.UserInterface: Explorer, StateActionStats

include("../../../src/networks/flux/common.jl")
include("./utils.jl")

#####
##### Interactive exploration of the environment
#####

# NOTE: session is not saved on modification
# Return true if the command is valid, false otherwise
function UI.interpret!(session::Session, exp::Explorer, stats, cmd, args=[])
  gspec = GI.spec(exp.game)
  if cmd == "go"
    return load_state(gspec, exp)

  elseif cmd == "state"
    if length(args) == 0 || args[1] == "save"
      return write_state(gspec, exp)
    elseif length(args) == 1 && args[1] == "load"
      return load_state(gspec, exp)
    else
      @warn "Invalid argument $args"
      return false
    end

  elseif cmd == "restart"
    UI.restart!(exp)
    return true

  elseif cmd == "reset"
    AlphaZero.reset_player!(exp.player)
    empty!(exp.history)
    return true
    
  elseif cmd == "undo"
    if !isempty(exp.history)
      exp.game = pop!(exp.history)
      exp.turn -= 1
      return true
    else
      @warn "History is empty. Unable to undo action!"
      return false
    end

  elseif cmd == "memory"
    if length(args) == 0 || args[1] == "describe"
      @printf "%i samples in memory\n" length(session.env.memory)
      return true
    #elseif args[1] == "show" # TODO: show the sample in memory
    elseif length(args) == 1 && args[1] == "clear"
      empty!(session.env.memory)
      println("Memory cleared.")
      return true
    elseif length(args) == 1 && args[1] == "add"
      tr = read_trace(gspec)
      AlphaZero.push_trace!(session.env.memory, tr, exp.player.mcts.gamma)
      return true
    elseif args[1] == "self-play"
      @warn "Not yet implemented"
      return true
      Handlers.iteration_started(session)
      #resize_memory!(session.env, session.env.params.mem_buffer_size[env.itc]) # no modification of memory size for now
      sprep, spperfs = Report.@timed AlphaZero.self_play_step!(session.env, session)
      #rep = Report.Iteration(spperfs, mperfs, lperfs, sprep, mrep, lrep) # TODO: add report
      #Handlers.iteration_finished(handler, rep) # TODO: add report

      #Handlers.iteration_started(session) # TODO: add report
      #mrep, mperfs = Report.@timed memory_report(session.env, session) # TODO: not needed - move to memory
      #rep = Report.Iteration(spperfs, mperfs, lperfs, sprep, mrep, lrep) # TODO: add report
      #Handlers.iteration_finished(handler, rep) # TODO: add report

      # TODO: consider creating a new player and setting in exp
      return true
    else
      @warn "Invalid argument $args"
      return false # invalid command, do nothing
    end

  elseif cmd == "training"
    if length(session.env.memory) == 0
      println("Memory is empty! You need to add samples before being able to train.")
      return false
    end

    if length(args) == 1 && args[1] == "batch_size"
      @printf "Memory batch_size: %i\n" AlphaZero.cur_batch_size(session.env.memory)
      return true
    elseif length(args) == 2 && args[1] == "batch_size"
      return set_batch_size(gspec, session, args)
    elseif args[1] == "train"
      n_iter = 1
      try
        if length(args) >= 2
          n_iter = parse(Int, args[2])
        else
          n_iter = 1
        end
      catch
        @warn "Invalid argument $(args[2])"
        return false
      end

      if length(args) == 3
        if args[3] == "force"
          always_replace = true
          println("Always updating the network after each iteration.")
          batch_size_override = AlphaZero.cur_batch_size(session.env.memory)
          p_backup = session.env.params
          lp_backup = p_backup.learning
          session.env.params = Params(
            self_play=p_backup.self_play,
            memory_analysis=p_backup.memory_analysis,
            # Set the learning parameter's batch_size to the memory's
            learning=LearningParams(
              use_gpu=lp_backup.use_gpu,
              use_position_averaging=lp_backup.use_position_averaging,
              samples_weighing_policy=lp_backup.samples_weighing_policy,
              optimiser=lp_backup.optimiser,
              l2_regularization=lp_backup.l2_regularization,
              rewards_renormalization=lp_backup.rewards_renormalization,
              nonvalidity_penalty=lp_backup.nonvalidity_penalty,
              batch_size=batch_size_override,
              loss_computation_batch_size=lp_backup.loss_computation_batch_size,
              min_checkpoints_per_epoch=lp_backup.min_checkpoints_per_epoch,
              max_batches_per_checkpoint=lp_backup.max_batches_per_checkpoint,
              num_checkpoints=lp_backup.num_checkpoints,
            ),
            # Set arena params to nothing in order to skip arena and always replace the network
            arena=nothing,
            num_iters=p_backup.num_iters,
            use_symmetries=p_backup.use_symmetries,
            ternary_rewards=p_backup.ternary_rewards,
            mem_buffer_size=p_backup.mem_buffer_size
          )
        else
          @warn "Illegal argument $(args[3])!"
          return false
        end
      else
        always_replace = false
      end

      @printf "Starting %i training iterations with %i samples in memory\n\n" n_iter length(session.env.memory)
      @printf "Memory batch_size: %i\n" AlphaZero.cur_batch_size(session.env.memory)
      println()
      JSON3.write(stdout, session.env.params.learning)
      println("\n")
      while n_iter > 0
        #Handlers.iteration_started(session)
        lrep, lperfs = Report.@timed AlphaZero.learning_step!(session.env, session)
        # FIXME: type error: Cannot `convert` an object of type AlphaZero.MemoryBuffer  to an object of type AlphaZero.Report.Memory
        #rep = Report.Iteration(Report.Perfs(0.0, 0, 0.0), Report.Perfs(0.0, 0, 0.0), lperfs, Report.SelfPlay(0.0, 0.0, 0, 0, 0), session.env.memory, lrep)
        #Handlers.iteration_finished(session, rep)# does save increment what we dont want....
        n_iter -= 1
      end

      @assert exp.player.mcts.oracle != session.env.bestnn "Training did not change the network!"

      exp.player.mcts.oracle = session.env.bestnn

      # restore arena params
      if always_replace
        session.env.params = p_backup
      end
      return true
    else
      @warn "Invalid argument $args"
      return false # invalid command, do nothing
    end

  elseif cmd == "do"
    GI.game_terminated(exp.game) && return false
    a = get_action(gspec, exp, stats, args)
    if !isnothing(a)
      UI.save_game!(exp)
      GI.play!(exp.game, a)
      exp.turn += 1
      return true
    else
      @warn "Invalid action $args"
      return false
    end

  elseif cmd == "flip"
    UI.save_game!(exp)
    GI.apply_random_symmetry!(exp.game)
    return true

  elseif cmd == "explore"
    isa(exp.player, MctsPlayer) || (return false)

    try
      n = parse(Int, args[2])
      MCTS.explore!(exp.player.mcts, exp.game, n)
      return true
    catch
      @warn "Invalid argument $(args[2])"
      return false
    end
  end

  @warn "Invalid command $cmd"
  return false
end

function UI.explore(
  session::Session,
  player::AbstractPlayer,
  game::AbstractGameEnv)
  return UI.start_explorer(session, Explorer(player, game))
end

function UI.start_explorer(session::Session, exp::Explorer)
  cmd = nothing
  args = nothing
  while true
    # Print the state
    GI.render(exp.game; debug=true)
    # this does some tree boulding
    if !GI.game_terminated(exp.game)
       # this does build a tree (because it calls think(playe) to provide a policy and value)
      stats = UI.state_statistics(exp.game, exp.player, exp.turn, exp.memory)
      UI.print_state_statistics(GI.spec(exp.game), stats)
      save_state_statistics(GI.spec(exp.game), stats)
      #@show exp.player
    else
      stats = nothing
    end
    # Interpret command
    while true
      print("> ")
      inp = readline() |> lowercase |> split
      isempty(inp) && return
      cmd = inp[1]
      args = inp[2:end]
      UI.interpret!(session, exp, stats, cmd, args) && break
    end
    println("")
  end
end

function UI.state_statistics(game, player, turn, memory=nothing)
  @assert !GI.game_terminated(game)
  state = GI.current_state(game)
  # Make the player think
  actions, π = think(player, game)
  
  network_actions = map(a -> a.value, actions)
  all_network_actions = NN_OUTPUT_FIELD_VALUE_MAPPING
  missing_network_actions = setdiff(all_network_actions, network_actions)
  missing_dummy_actions = map(v -> (cards=nothing, value=v), missing_network_actions)
  
  actions = vcat(actions, missing_dummy_actions)

  τ = player_temperature(player, game, turn)
  π = apply_temperature(π, τ)
  report = ExtendedStateStats(actions)
  for i in eachindex(non_dummy_actions(actions))
    report.actions[i][2].index = i
    report.actions[i][2].P = π[i]
  end
  # Collect MCTS Statistics
  if isa(player, MctsPlayer) && haskey(player.mcts.tree, state)
    mcts = player.mcts
    info = mcts.tree[state]
    ucts = MCTS.uct_scores(info, mcts.cpuct, 0., nothing)
    report.Nmcts = MCTS.Ntot(info)
    for (i, a) in enumerate(non_dummy_actions(actions))
      arep = report.actions[i][2]
      astats = info.stats[i]
      arep.Pmcts = astats.N / max(1, report.Nmcts)
      arep.Qmcts = astats.N > 0 ? astats.W / astats.N : 0.
      arep.UCT = ucts[i]

      # Nmcts
      arep.Nmcts = astats.N

      # Nmem
      if !isnothing(memory)
        mem = AlphaZero.merge_by_state(get_experience(memory))
        relevant = findall((ex -> ex.s == state), mem)
        if !isempty(relevant)
          @assert length(relevant) == 1
          e = mem[relevant[1]]
          arep.Nmem = e.n
        end
      end
    end
  end
  # Collect memory statistics
  if !isnothing(memory)
    mem = AlphaZero.merge_by_state(get_experience(memory))
    relevant = findall((ex -> ex.s == state), mem)
    if !isempty(relevant)
      @assert length(relevant) == 1
      e = mem[relevant[1]]
      report.Nmem = e.n
      report.Vmem = e.z
      for i in eachindex(non_dummy_actions(actions))
        report.actions[i][2].Pmem = e.π[i]
      end
    end
  end
  # Collect network statistics
  oracle = UI.player_oracle(player)
  if isa(oracle, AbstractNetwork)
    Pnet, Vnet = oracle(state)
    report.Vnet = Vnet
    for (i, a) in enumerate(non_dummy_actions(actions))
      arep = report.actions[i][2]
      arep.Pnet = Pnet[i]
      arep.Qnet = UI.evaluate_qnet(oracle, game, actions[i], UI.player_gamma(player))
    end

    # Collect raw network statistics
    allPnet, _ = oracle(state, true, false)
    allPreMaskPnet, _ = oracle(state, false, false)
    for (i, a) in enumerate(actions)
      nrep = report.actions[i][3]
      action_proba_index = findfirst(isequal(a.value), NN_OUTPUT_FIELD_VALUE_MAPPING)
      nrep.Pnet = allPnet[action_proba_index]
      nrep.prePnet = allPreMaskPnet[action_proba_index]
    end
  end
  return report
end

#####
##### Displaying state statistics
#####

function UI.print_state_statistics(gspec, stats::ExtendedStateStats)
  prob   = Log.ColType(nothing, x -> fmt(".1f", 100 * x) * "%")
  val    = Log.ColType(nothing, x -> fmt("+.2f", x))
  bigint = Log.ColType(nothing, n -> format(ceil(Int, n), commas=true))
  alabel = Log.ColType(nothing, identity)
  btable = Log.Table([
    ("Nmcts", bigint, r -> r.Nmcts),
    ("Nmem",  bigint, r -> r.Nmem),
    ("Vmem",  val,    r -> r.Vmem),
    ("Vnet",  val,    r -> r.Vnet)],
    header_style=Log.BOLD)
  atable = Log.Table([
    ("action index",  bigint, r -> r[2].index),
    ("field value",   bigint, r -> r[1].value),
    ("action",        alabel, r -> GI.action_string(gspec, r[1])),
    ("",              prob,   r -> r[2].P),
    ("Pmcts",         prob,   r -> r[2].Pmcts),
    ("Pnet",          prob,   r -> r[2].Pnet),
    ("UCT",           val,    r -> r[2].UCT),
    ("Pmem",          prob,   r -> r[2].Pmem),
    ("Qmcts",         val,    r -> r[2].Qmcts),
    ("Qnet",          val,    r -> r[2].Qnet),
    ("Nmcts",         bigint, r -> r[2].Nmcts),
    ("Nmem",          bigint, r -> r[2].Nmem)],
    header_style=Log.BOLD)
  ntable = Log.Table([
    ("action index",  bigint, r -> r[2].index),
    ("field value",   bigint, r -> r[1].value),
    ("Pnet",          prob,   r -> r[3].Pnet),
    ("prePnet",       prob,   r -> r[3].prePnet)],
    header_style=Log.BOLD)
  logger = Logger()

  # Sort the actions from best to worst for btable and atable
  sort!(stats.actions, rev=true, by=t -> !is_dummy_action(t[1]) ? t[2].P : -1.0)

  if !all(isnothing, [stats.Nmcts, stats.Nmem, stats.Vmem, stats.Vnet])
    Log.table(logger, btable, [stats])
    Log.sep(logger)
  end
  
  Log.table(logger, atable, filter(t -> !is_dummy_action(t[1]), stats.actions))
  Log.sep(logger)

  # Sort the actions by (field_)value for stable order for better comparability
  sort!(stats.actions, rev=true, by=t -> t[1].value)

  Log.table(logger, ntable, stats.actions)
  Log.sep(logger)
end