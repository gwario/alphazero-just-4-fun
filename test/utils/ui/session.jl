using AlphaZero: Handlers, Log, UserInterface, Benchmark, GI
using JSON3

const BENCHMARK_STATS_FILE = "benchmark_stats.json"

#function Handlers.iteration_started(session::Session)
#function Handlers.self_play_started(session::Session)
#function Handlers.game_played(session::Session)
#function Handlers.self_play_finished(session::Session, report)
#function Handlers.memory_analyzed(session::Session, report)
#function Handlers.learning_started(session::Session)
#function Handlers.updates_started(session::Session, status)
#function Handlers.updates_finished(session::Session, report)
#function Handlers.checkpoint_started(session::Session)
#function Handlers.checkpoint_game_played(session::Session)
#function Handlers.checkpoint_finished(session::Session, report)
#function Handlers.learning_finished(session::Session, report)
#function Handlers.training_finished(session::Session)

function Handlers.iteration_finished(session::Session, report)
    bench, bench_stats = run_benchmark(session)
    Log.section(session.logger, 2, "Iteration game end statistics:")
    print_stats(session.logger, bench_stats, session.env.itc)
    save_increment!(session, bench, bench_stats, report)
    flush(Log.logfile(session.logger))
end


# return whether or not critical problems were found
function UserInterface.zeroth_iteration!(session::Session)
    @assert session.env.itc == 0
    Log.section(session.logger, 2, "Initial report")
    report = AlphaZero.initial_report(session.env)
    UserInterface.print_report(session.logger, report)
    isempty(report.errors) || return false
    bench, bench_stats = run_benchmark(session)
    Log.section(session.logger, 2, "Iteration game end statistics:")
    print_stats(session.logger, bench_stats, session.env.itc)
    save_increment!(session, bench, bench_stats)
    return true
end
  

#####
##### Incremental saving
#####

function save_increment!(session::Session, bench, bench_stats, itrep=nothing)
    push!(session.report.benchmark, bench)
    push!(Just4Fun.benchmark_stats, bench_stats)
    isnothing(itrep) || push!(session.report.iterations, itrep)
    itc = session.env.itc
    if UserInterface.autosave_enabled(session)
        idir = UserInterface.iterdir(session.dir, itc)
        isdir(idir) || mkpath(idir)
        # Save the environment state,
        # both at the root and in the last iteration folder
        save(session, session.dir)
        session.save_intermediate && save(session, idir)
        # Save the collected statistics
        save_report_increment(session, bench, bench_stats, itrep, idir)
        # Do the plotting
        params = session.env.params
        plotsdir = joinpath(session.dir, UserInterface.PLOTS_DIR)
        isnothing(itrep) || UserInterface.plot_iteration(itrep, params, plotsdir, itc)
        UserInterface.plot_training(params, session.report.iterations, plotsdir)
        UserInterface.plot_benchmark(params, session.report.benchmark, plotsdir)
        plot_benchmark_stats(params, Just4Fun.benchmark_stats, plotsdir)
        plot_benchmark_duel_stats(params, Just4Fun.benchmark_stats, plotsdir)
    end
end

function UserInterface.load_session_report(dir::String, niters)
    rep = UserInterface.SessionReport()
    for itc in 0:niters
        idir = UserInterface.iterdir(dir, itc)
        ifile = joinpath(idir, UserInterface.REPORT_FILE)
        bfile = joinpath(idir, UserInterface.BENCHMARK_FILE)
        bsfile = joinpath(idir, BENCHMARK_STATS_FILE)
        # Load the benchmark report
        open(bfile, "r") do io
            push!(rep.benchmark, JSON3.read(io, Report.Benchmark))
        end
        # Load the benchmark stats
        open(bsfile, "r") do io
            push!(Just4Fun.benchmark_stats, JSON3.read(io, BenchmarkStats))
        end
        # Load the iteration report
        if itc > 0
            open(ifile, "r") do io
            push!(rep.iterations, JSON3.read(io, Report.Iteration))
            end
        end
    end
    @assert UserInterface.valid_session_report(rep)
    return rep
end

function save_report_increment(session, bench, bench_stats, itrep, idir)
    open(joinpath(idir, UserInterface.BENCHMARK_FILE), "w") do io
        JSON3.pretty(io, JSON3.write(bench))
    end
    open(joinpath(idir, BENCHMARK_STATS_FILE), "w") do io
        JSON3.pretty(io, JSON3.write(bench_stats))
    end
    if session.env.itc > 0
        @assert !isnothing(itrep)
        open(joinpath(idir, UserInterface.REPORT_FILE), "w") do io
            JSON3.pretty(io, JSON3.write(itrep))
        end
    end
end

"""
print_stats(logger::Log.Logger, stats::Vector{BenchmarkStats})

Logs the overall stats
"""
function print_stats(logger::Log.Logger, stats::Vector{BenchmarkStats})
    
    evals = vcat(stats...) # all evals
    gamestates = reduce(vcat, map(e -> e.result, evals)) # all gamestates
    # per duel eval stats
    duel_evals = Dict{String, Vector{GameState}}()
    
    duels = unique(map(e -> e.legend, evals))
    for duel in duels
        d_gamestates = reduce(vcat, map(eval -> eval.result , filter(e -> e.legend == duel, evals)))
        duel_evals[duel] = d_gamestates
    end

    # overall stats
    n = length(gamestates)
    by_draw = length(filter(r -> r == end_by_draw, gamestates))
    by_pattern = length(filter(r -> r == end_by_pattern, gamestates))
    by_points = length(filter(r -> r == end_by_points, gamestates))
    by_max_field = length(filter(r -> r == end_by_max_field, gamestates))

    @assert isempty(filter(r -> r == in_progress, gamestates)) "All games must end"

    p_draw = UserInterface.percentage(by_draw, n)
    p_pattern = UserInterface.percentage(by_pattern, n)
    p_points = UserInterface.percentage(by_points, n)
    p_max_field = UserInterface.percentage(by_max_field, n)
    
    details = [
        "$by_pattern ($p_pattern%) by pattern",
        "$by_points ($p_points%) by points",
        "$by_max_field ($p_max_field%) by max field",
        "$by_draw ($p_draw%) by draw"
    ]
    details = join(details, ", ")
    msg = "Game end statistics: $details"
    Log.print(logger, msg)

    
    for (duel, gs) in duel_evals
        
        duel_n = length(gs)
        by_draw = length(filter(r -> r == end_by_draw, gs))
        by_pattern = length(filter(r -> r == end_by_pattern, gs))
        by_points = length(filter(r -> r == end_by_points, gs))
        by_max_field = length(filter(r -> r == end_by_max_field, gs))
    
        p_draw = UserInterface.percentage(by_draw, duel_n)
        p_pattern = UserInterface.percentage(by_pattern, duel_n)
        p_points = UserInterface.percentage(by_points, duel_n)
        p_max_field = UserInterface.percentage(by_max_field, duel_n)
        
        details = [
            "$by_pattern ($p_pattern%) by pattern",
            "$by_points ($p_points%) by points",
            "$by_max_field ($p_max_field%) by max field",
            "$by_draw ($p_draw%) by draw"
        ]
        details = join(details, ", ")
        msg = "$duel:\t$details"
        Log.print(logger, msg)
    end
end

"""
print_stats(logger::Log.Logger, stats::BenchmarkStats, itc)

Logs the stats for all duels in an iteration ()
"""
function print_stats(logger::Log.Logger, stats::BenchmarkStats, itc)
    results = reduce(vcat, map(e -> e.result, stats))
    n = length(results)
    by_draw = length(filter(r -> r == end_by_draw, results))
    by_pattern = length(filter(r -> r == end_by_pattern, results))
    by_points = length(filter(r -> r == end_by_points, results))
    by_max_field = length(filter(r -> r == end_by_max_field, results))

    @assert isempty(filter(r -> r == in_progress, results)) "All games must end"

    p_draw = UserInterface.percentage(by_draw, n)
    p_pattern = UserInterface.percentage(by_pattern, n)
    p_points = UserInterface.percentage(by_points, n)
    p_max_field = UserInterface.percentage(by_max_field, n)
    
    details = [
        "$by_pattern ($p_pattern%) by pattern",
        "$by_points ($p_points%) by points",
        "$by_max_field ($p_max_field%) by max field",
        "$by_draw ($p_draw%) by draw"
    ]
    details = join(details, ", ")
    msg = "Game end statistics: $details"
    Log.print(logger, msg)
end

"""
print_stats(logger::Log.Logger, stats::EvaluationStats, duel)

Logs the stats of a duel (a number of games between two specific agents)
"""
function print_stats(logger::Log.Logger, stats::EvaluationStats)
    n = length(stats.result)
    by_draw = length(filter(r -> r == end_by_draw, stats.result))
    by_pattern = length(filter(r -> r == end_by_pattern, stats.result))
    by_points = length(filter(r -> r == end_by_points, stats.result))
    by_max_field = length(filter(r -> r == end_by_max_field, stats.result))

    @assert isempty(filter(r -> r == in_progress, stats.result)) "All games must end"

    p_draw = UserInterface.percentage(by_draw, n)
    p_pattern = UserInterface.percentage(by_pattern, n)
    p_points = UserInterface.percentage(by_points, n)
    p_max_field = UserInterface.percentage(by_max_field, n)
    
    details = [
        "$by_pattern ($p_pattern%) by pattern",
        "$by_points ($p_points%) by points",
        "$by_max_field ($p_max_field%) by max field",
        "$by_draw ($p_draw%) by draw"
    ]
    details = join(details, ", ")
    msg = "Game end statistics: $details"
    Log.print(logger, msg)
end

"""
    explore(session::Session, [mcts_params, use_gpu])

Start an explorer session for the current environment.
"""
function explore(session::Session; args...)
  Log.section(session.logger, 1, "Starting interactive exploration")
  UserInterface.explore(session, AlphaZeroPlayer(session.env; args...), GI.init(session.env.gspec))
end