using StatsPlots
using AlphaZero: Benchmark
using DataFrames

function plot_benchmark_stats(params::Params, benchs_stats::Vector{BenchmarkStats}, dir::String)
    isempty(benchs_stats) && return
    nduels = length(benchs_stats[1])
    nduels >= 1 || return
    @assert all(length(b) == nduels for b in benchs_stats)
    isdir(dir) || mkpath(dir)
    iterations = Vector{Int64}()
    by_draw = Vector{Int64}()
    by_pattern = Vector{Int64}()
    by_points = Vector{Int64}()
    by_max_field = Vector{Int64}()

    # TODO: what do i want to get?
    #agent_action_redraw = []
    #agent_action_regular = []
    #
    #action_redraw = []
    #action_regular = []
    
    for (iter_index, iteration_bench_stats::BenchmarkStats) in enumerate(benchs_stats)

        push!(iterations, iter_index - 1)

        # add element for the iteration
        push!(by_draw, 0)
        push!(by_pattern, 0)
        push!(by_points, 0)
        push!(by_max_field, 0)

        #push!(agent_action_redraw, 0)
        #push!(agent_action_regular, 0)

        #push!(action_redraw, 0)
        #push!(action_regular, 0)
        #
        #redraws_per_duel = 0
        #regulars_per_duel = 0

        for (duel_index, duel_bench_stats::EvaluationStats) in enumerate(iteration_bench_stats)
            # duel_bench contains all the games of a duel
            # TODO: make per duel stats

            by_draw[iter_index]         += length(filter(r -> r == end_by_draw, duel_bench_stats.result))
            by_pattern[iter_index]      += length(filter(r -> r == end_by_pattern, duel_bench_stats.result))
            by_points[iter_index]       += length(filter(r -> r == end_by_points, duel_bench_stats.result))
            by_max_field[iter_index]    += length(filter(r -> r == end_by_max_field, duel_bench_stats.result))

            @assert isempty(filter(r -> r == in_progress, duel_bench_stats.result)) "All games must end"
            
            #agent_redraws_per_duel = sum(duel_bench_stats.action_redraw[1])
            #agent_regulars_per_duel = sum(duel_bench_stats.action_regular[1])
            #
            #push!(agent_action_redraw, agent_redraws_per_duel)
            #push!(agent_action_regular, agent_regulars_per_duel)
            ## TODO: where to add this?
            #redraws_per_duel += agent_redraws_per_duel + sum(duel_bench_stats.action_redraw[2])
            #regulars_per_duel += agent_regulars_per_duel + sum(duel_bench_stats.action_regular[2])
        end
        # TODO: what does this?
        #push!(per_iteration_action_redraw, sum(per_iteration_per_player_action_redraw[index]))
        #push!(per_iteration_action_regular, sum(per_iteration_per_player_action_regular[index]))
    end
  
    catnam = ["End in draw", "End by pattern", "End by most points", "End by highest field"]
    ctg = repeat(catnam, inner = length(iterations))
    nam = repeat(iterations, outer = length(catnam))
    
    gb = groupedbar(nam, reduce(hcat, [by_draw, by_pattern, by_points, by_max_field]),
        group = ctg,
        xlabel = "Iterations", ylabel = "Number of games", title = "Number of games by end reason and iteration",
        bar_position = :stack, bar_width=0.7, lw = 0, framestyle = :box, legend = :bottomright
    )
    
    # to save as pic
    Plots.savefig(gb, joinpath(dir, "benchmark_game_ends.svg"))
    
    # to display (gtk+) window
    # display(gb)
end


struct DuelStats 
    by_draw::Int64
    by_pattern::Int64
    by_points::Int64
    by_max_field::Int64
end

function plot_benchmark_duel_stats(params::Params, benchs_stats::Vector{BenchmarkStats}, dir::String)
    isempty(benchs_stats) && return
    nduels = length(benchs_stats[1])
    nduels >= 1 || return
    @assert all(length(b) == nduels for b in benchs_stats)
    isdir(dir) || mkpath(dir)

    first_iter = first(benchmark_stats)
    duels = unique(map(e -> e.legend, first_iter))
    
    for duel in duels
        
        # prepare graph for each duel
        iterations = Vector{Int64}()
        by_draw = Vector{Int64}()
        by_pattern = Vector{Int64}()
        by_points = Vector{Int64}()
        by_max_field = Vector{Int64}()

        for (iter_index, benchs::BenchmarkStats) in enumerate(benchs_stats)

            push!(iterations, iter_index - 1)

            duel_benchs = filter(b -> b.legend == duel, benchs)
            duel_bench_stats = only(duel_benchs)
            # add element for the iteration
            push!(by_draw, length(filter(r -> r == end_by_draw, duel_bench_stats.result)))
            push!(by_pattern, length(filter(r -> r == end_by_pattern, duel_bench_stats.result)))
            push!(by_points, length(filter(r -> r == end_by_points, duel_bench_stats.result)))
            push!(by_max_field, length(filter(r -> r == end_by_max_field, duel_bench_stats.result)))

            @assert isempty(filter(r -> r == in_progress, duel_bench_stats.result)) "All games must end"
        end

        catnam = ["End in draw", "End by pattern", "End by most points", "End by highest field"]
        ctg = repeat(catnam, inner = length(iterations))
        nam = repeat(iterations, outer = length(catnam))
        
        gb = groupedbar(nam, reduce(hcat, [by_draw, by_pattern, by_points, by_max_field]),
            group = ctg,
            xlabel = "Iterations", ylabel = "Number of games", title = "Number of games by end reason and iteration for $duel",
            bar_position = :stack, bar_width=0.7, lw = 0, framestyle = :box, legend = :bottomright
        )
        
        # to save as pic
        Plots.savefig(gb, joinpath(dir, "benchmark_game_ends_$(replace(duel, r"\s" => s"-", r"/" => s"vs")).svg"))
        
        # to display (gtk+) window
        # display(gb)
    end
end
