using Coverage
using CoverageTools: MallocInfo
using Printf
using Formatting: fmt, format
using JSON3
using Just4Fun
using AlphaZero
using AlphaZero: UI, GI, Benchmark
using AlphaZero.UserInterface: Explorer, StateActionStats
using AlphaZero.Log: Logger
using Base: @kwdef

"""
execute(operations::Vector, experiment::Module, benchmark::AbstractVector{Benchmark.Duel}, mode::String, mode_param::String)

Performs an operation using an experiment in a mode.
Operations:
- train
- play
- explore
"""
function execute(operations::Vector, experiment::Module, benchmark::AbstractVector{Benchmark.Duel}, mode::String, mode_param::String)
    @info "Operations: [$(join(map(repr, operations)))]"
    @info ""
    session = nothing
    for operation in operations
        @info "Operation: $(repr(operation))"
        @info ""
        experiment_inst = Experiment(
            session_name(experiment, experiment.gspec),
            experiment.gspec,
            experiment.params,
            experiment.network,
            experiment.netparams,
            benchmark
        )
        session = execute_op_in(operation, experiment_inst, mode, mode_param)
    end
    return session
end

target()::String     = length(ARGS) > 0 ? ARGS[1] : "all"
mode()::String       = length(ARGS) > 1 ? ARGS[2] : "original"
mode_param()::String = length(ARGS) > 2 ? ARGS[3] : mode() == "profile" ? "text" : ""

"""
Target that work with Just4Fun.run():
    * train
    * play
    * explore
    * benchmark

Targets that work with Just4Fun.run_test():
    * all
    * framework
    * unit
    * integration
    * custom
    * dummy-run
    * grad-updates
"""
function set_target(target::String)
    if length(ARGS) < 1
        push!(ARGS, target)
    else
        ARGS[1] = target
    end
end

"""
Modes:
    * original
    * benchmark
    * time-allocated
    * profile
"""
function set_mode(mode::String)
    @assert length(ARGS) > 0 "Please set a target first!"
    if length(ARGS) < 2
        push!(ARGS, mode)
    else
        ARGS[2] = mode
    end
end

"""
Modes params:
    * interactive
    * svg
    * html
    * text
"""
function set_mode_param(mode_param::String)
    @assert length(ARGS) > 1 "Please set a target and mode first!"
    if length(ARGS) < 3
        push!(ARGS, mode_param)
    else
        ARGS[3] = mode_param
    end
end

unit_tests(x)   = include("./test/unit/main.jl")
e2e_tests(x)    = include("./test/e2e/main.jl")

function unit_and_e2e_tests(a)
    unit_tests(a)
    e2e_tests(a)
end

"""

target_opperation()

Retruns the functions that executes the target.
"""
function target_opperation()

    opcode = target()
    operations = Vector()

    # main run modes
    if opcode == "train"
        push!(operations, train)
    elseif opcode == "play"
        push!(operations, play)
    elseif opcode == "explore"
        push!(operations, explore)
    elseif opcode == "benchmark"
        push!(operations, run_benchmark)
        
    end
    
    # test run modes
    if opcode == "framework" || opcode == "all"
        push!(operations, Scripts.test_game)
    end

    if opcode == "unit" || opcode == "all"
        push!(operations, unit_tests)
    end
    
    if opcode == "integration" || opcode == "all"
        push!(operations, e2e_tests)
    end
    
    if opcode == "custom" || opcode == "all"
        push!(operations, unit_and_e2e_tests)
    end
    
    if opcode == "dummy-run" || opcode == "all"
        push!(operations, dummy_run)
    end
    
    if opcode == "grad-updates" || opcode == "all"
        push!(operations, test_grad_updates)
    end

    if isempty(operations)
        throw(AssertionError("Unhandled operation '$opcode'"))
    end

    return operations
end

gpu_available(gspec::AbstractGameSpec, net, net_params) = NetLib.Network.on_gpu(NetLib.Network.copy(net(gspec, net_params), on_gpu=true, test_mode=true))

"""
session_name(experiment_module::Module, gspec::AbstractGameSpec, use_gpu::Bool)

Cosntructs the session/experiment name
"""
function session_name(experiment_module::Module, gspec::AbstractGameSpec)
    module_name = nameof(experiment_module)
    nn_name = nameof(experiment_module.network)
    gs_name = nameof(typeof(gspec))

    proc_type = experiment_module.USE_GPU && gpu_available(gspec, experiment_module.network, experiment_module.netparams) ? "GPU" : "CPU"

    "$(gs_name)_$(nn_name)_$(module_name)_$(proc_type)"
end

function print_mode()
    @info "Target: $(target())"
    if !isnothing(mode_param())
        @info "Mode: $(mode()) ($(mode_param()))"
    else
        @info "Mode: $(mode())"
    end
    @info ""
end

function print_model(experiment::Module)
  network = experiment.network(experiment.gspec, experiment.netparams)
  JSON3.pretty(network)
end

function print_experiment(experiment)
    net_params = nameof(experiment)
    net = nameof(experiment.network)
    @info "Network: $net"
    @info "Parameters: $net_params"
    @info ""
    #print_model(experiment)
end


@kwdef mutable struct RawNetworkActionStats
    Pnet    :: Union{Float64, Nothing} = nothing # Prior probability as given by the oracle
    prePnet :: Union{Float64, Nothing} = nothing # Prior probability as given by the oracle before masking
end

"""
ExtendedStateActionStats

Contains not only the state action stats from StateActionStats but also the visit counts Nmcts and Nmem.
"""
@kwdef mutable struct ExtendedStateActionStats
    index :: Union{Int, Nothing} = nothing      # The index of the action as reported by the player, dummy actions don't have an index
    P     :: Union{Float64, Nothing} = nothing  # Probability given by `think`
    Pmem  :: Union{Float64, Nothing} = nothing  # Recorded π component in memory
    Pmcts :: Union{Float64, Nothing} = nothing  # Percentage of MCTS visits
    Qmcts :: Union{Float64, Nothing} = nothing
    UCT   :: Union{Float64, Nothing} = nothing
    Pnet  :: Union{Float64, Nothing} = nothing
    Qnet  :: Union{Float64, Nothing} = nothing
    Nmcts :: Union{Int, Nothing} = nothing
    Nmem  :: Union{Int, Nothing} = nothing
end

"""
ExtendedStateStats{Action}

Contains not only the action stats from StateStats but also the raw network stats.
"""
mutable struct ExtendedStateStats{Action}
    Nmcts :: Union{Int, Nothing}
    Nmem  :: Union{Int, Nothing}
    Vmem  :: Union{Float64, Nothing}
    Vnet  :: Union{Float64, Nothing}
    actions :: Vector{Tuple{Action, ExtendedStateActionStats, RawNetworkActionStats}}
    function ExtendedStateStats(actions)
        actions_stats = [(a, ExtendedStateActionStats(), RawNetworkActionStats()) for a in actions]
        new{eltype(actions)}(nothing, nothing, nothing, nothing, actions_stats)
    end
end

for T in [
    RawNetworkActionStats,
    ExtendedStateStats,
    ExtendedStateActionStats,
    StateActionStats
]
    @eval JSON3.StructType(::Type{<:$T}) = JSON3.Struct()
end


is_dummy_action(a) = isnothing(a.cards)
dummy_actions(as) = filter(is_dummy_action, as)
non_dummy_actions(as) = filter(!is_dummy_action, as)

"""
    save_state_statistics(gspec::Just4FunSpec, stats::ExtendedStateStats)

Stores the stats for an explored state in a file.

If the command was interactive, it is stored to a file with the current
timestamp.
If the command was file, it is stored to a file with the name of the input and
the current timestamp.
"""
function save_state_statistics(gspec::Just4FunSpec, stats::ExtendedStateStats)
    prob   = Log.ColType(nothing, x -> fmt(".1f", 100 * x) * "%")
    val    = Log.ColType(nothing, x -> fmt("+.2f", x))
    bigint = Log.ColType(nothing, n -> format(ceil(Int, n), commas=true))
    alabel = Log.ColType(nothing, identity)
    btable = Log.Table([
        ("Nmcts", bigint, r -> r.Nmcts),
        ("Nmem",  bigint, r -> r.Nmem),
        ("Vmem",  val,    r -> r.Vmem),
        ("Vnet",  val,    r -> r.Vnet)],
        header_style=Log.BOLD)
    atable = Log.Table([
        ("action index",    bigint, r -> r[2].index),
        ("field value",     bigint, r -> r[1].value),
        ("action",          alabel, r -> GI.action_string(gspec, r[1])),
        ("",                prob,   r -> r[2].P),
        ("Pmcts",           prob,   r -> r[2].Pmcts),
        ("Pnet",            prob,   r -> r[2].Pnet),
        ("UCT",             val,    r -> r[2].UCT),
        ("Pmem",            prob,   r -> r[2].Pmem),
        ("Qmcts",           val,    r -> r[2].Qmcts),
        ("Qnet",            val,    r -> r[2].Qnet),
        ("Nmcts",           bigint, r -> r[2].Nmcts),
        ("Nmem",            bigint, r -> r[2].Nmem)],
        header_style=Log.BOLD)
    ntable = Log.Table([
        ("action index",    bigint, r -> r[2].index),
        ("field value",     bigint, r -> r[1].value),
        ("Pnet",            prob,   r -> r[3].Pnet),
        ("prePnet",         prob,   r -> r[3].prePnet)],
        header_style=Log.BOLD)
    f = open("state_statistics.txt", "w")
    logger = Logger(devnull, logfile=f)

    # Sort the actions from best to worst for btable and atable
    sort!(stats.actions, rev=true, by=t -> !is_dummy_action(t[1]) ? t[2].P : -1.0)

    if !all(isnothing, [stats.Nmcts, stats.Nmem, stats.Vmem, stats.Vnet])
        Log.table(logger, btable, [stats])
        Log.sep(logger)
    end
    Log.table(logger, atable, filter(t -> !is_dummy_action(t[1]), stats.actions))
    Log.sep(logger)
    
    # Sort the actions by (field_)value for stable order for better comparability
    sort!(stats.actions, rev=true, by=t -> t[1].value)

    Log.table(logger, ntable, stats.actions)
    Log.sep(logger)

    JSON3.write(f, stats)
    close(f)
end

function set_seed!(seed)
    Random.seed!(seed)
    @info ""
    @info "Initializied Radnom with seed $seed"
    @info ""
end

# matches julia coverage files with and without the PID
ismemfile(filename) = occursin(r"\.jl\.?[0-9]*\.mem$", filename)
# matches a coverage file for the given sourcefile. They can be full paths
# with directories, but the directories must match
function ismemfile(filename, sourcefile)
    startswith(filename, sourcefile) || return false
    occursin(r"\.jl\.?[0-9]*\.mem$", filename)
end

"""
    clean_folder(folder::String)
Cleans up all the `.cov` and `.mem` files in the given directory and subdirectories.
Unlike `process_folder` this does not include a default value
for the root folder, requiring the calling code to be more explicit about
which files will be deleted.
"""
function custom_clean_folder(folder::String)
    files = readdir(folder)
    for file in files
        fullfile = joinpath(folder, file)
        if isfile(fullfile) && (Coverage.iscovfile(file) || ismemfile(file))
            # we have ourselves a coverage file. eliminate it
            @info "Removing $fullfile"
            rm(fullfile)
        elseif isdir(fullfile)
            custom_clean_folder(fullfile)
        end
    end
    nothing
end


"""
    coverage_fmt(coverage::Union{FileCoverage,Vector{FileCoverage}})::String

Returns the coverage as formated string.
"""
function coverage_fmt(coverage::Union{FileCoverage,Vector{FileCoverage}})::String
    covered_lines, total_lines = get_summary(coverage)
    line_coverage_pct = 100 * covered_lines / total_lines
    "$(@sprintf("%.2f", line_coverage_pct))%"
end

function get_line_from(line_number::Int64, file_path::String)
    io = open(file_path)
    
    for _ = 1:line_number-1
        readline(io)
    end
    
    line = readline(io)

    close(io)
    
    return line != "" ? line : "Line not found."
end

function print_allocation(malloc::MallocInfo)
    line = get_line_from(malloc.linenumber, malloc.filename)
    @info "$(malloc.bytes) bytes: $line"
end