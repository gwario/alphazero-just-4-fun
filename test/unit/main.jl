using Test


@testset verbose = true "Just4Fun Unit Tests" begin

    @testset verbose = true "Game" begin
        @testset verbose = true "Rules tests" begin
            include("./game/rules.jl")
        end
        @testset verbose = true "GI tests" begin
            include("./game/game.jl")
        end
        @testset verbose = true "Utils tests" begin
            include("./game/utils.jl")
        end
    end
    
    @testset verbose = true "Players" begin
        @testset verbose = true "Minimax tests" begin
            include("./player/minimax.jl")
        end
    end

    @testset verbose = true "Neural Network Tests" begin
        @testset verbose = true "Flux networks tests" begin
            include("../../src/networks/flux/common.jl")
            include("./networks/flux/common.jl")
        end
    end

    @testset verbose = true "Network Inputs Tests" begin
        @testset verbose = true "Cards Only" begin
            @testset verbose = true "All cards ordered" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ao.jl")
                include("./networks/inputs/ao.jl")
            end
        end

        @testset verbose = true "Cards and Fields" begin
            @testset verbose = true "Majority and Domination for current player and field values and all cards ordered" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ma-do-fv_ao.jl")
                include("./networks/inputs/ma-do-fv_ao.jl") 
            end

            @testset verbose = true "Majority and Domination for current player and all cards ordered" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ma-do_ao.jl")
                include("./networks/inputs/ma-do_ao.jl") 
            end

            @testset verbose = true "Majority and Domination for both players, player layer and field values and all cards ordered" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ma-do-pl-fv_ao.jl")
                include("./networks/inputs/ma-do-pl-fv_ao.jl") 
            end
        end
        
        @testset verbose = true "Fields Only" begin
            @testset verbose = true "Majority for both players and the player layer" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ma-pl.jl")
                include("./networks/inputs/ma-pl.jl")
            end

            @testset verbose = true "The majority and the dominance for both players and the player layer" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ma-do-pl.jl")
                include("./networks/inputs/ma-do-pl.jl")
            end

            @testset verbose = true "The stones for the current player and the field values" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/st-fv.jl")
                include("./networks/inputs/st-fv.jl")
            end

            @testset verbose = true "The stones for both players, the field values and the player layer" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/st-fv-pl.jl")
                include("./networks/inputs/st-fv-pl.jl")
            end

            @testset verbose = true "The stones, the majority and the dominance for both players, the field values and the player layer" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/st-ma-do-fv-pl.jl")
                include("./networks/inputs/st-ma-do-fv-pl.jl")
            end

            @testset verbose = true "The stones, the majority and the dominance for the current player and the field values" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/st-ma-do-fv.jl")
                include("./networks/inputs/st-ma-do-fv.jl")
            end

            @testset verbose = true "Majority and Domination for current player" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ma-do.jl")
                include("./networks/inputs/ma-do.jl")
            end

            @testset verbose = true "Majority and Domination for current player and field values" begin
                include("../../src/networks/flux/common.jl")
                include("../../src/networks/inputs/ma-do-fv.jl")
                include("./networks/inputs/ma-do-fv.jl") 
            end
        end
    end
end
