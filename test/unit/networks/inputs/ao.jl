@testset "GI.vectorize_state" begin
    @testset "correctly sized array of Float32 (yellow)" begin
        stack = Stack{Just4Fun.CardValue}()
        state = (
            stack=stack, used_cards=Cards(),
            player_cards=SMatrix{4, 2}([
                0x01 0x01 ;
                0x02 0x02 ;
                0x03 0x03 ;
                0x04 0x04
            ]),
            field_stones=SArray{Tuple{6, 6, 2}, Stones}([
                # p1
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;;;
                # p2
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00;
                0x00 0x00 0x00 0x00 0x00 0x00
            ]),
            player_stones=SVector{2, Stones}(repeat([Stones(20)], 2)),
            curplayer=Player(Just4Fun.YELLOW)
        )
      
        vector = GI.vectorize_state(Just4FunSpec(), state)
      
        @test vector isa Array{Float32}
        # p cards
        @test size(vector) == (55,)
    end
  
    @testset "contains cards vector (red)" begin
      
        stack = Stack{Just4Fun.CardValue}()
        state = (
          stack=stack, used_cards=Cards(),
          player_cards=SMatrix{4, 2}([
            0x05 0x01 ;
            0x06 0x02 ;
            0x07 0x03 ;
            0x08 0x04
          ]),
          field_stones=SArray{Tuple{6, 6, 2}, Stones}([
            # p1
            0x02 0x00 0x00 0x00 0x00 0x01;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;;;
            # p2
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x01 0x00 0x00 0x00 0x00 0x02
          ]),
          player_stones=SVector{2, Stones}(repeat([Stones(20)], 2)),
          curplayer=Player(Just4Fun.RED)
        )
        
        vector = GI.vectorize_state(Just4FunSpec(), state)
        
        @test vector isa Array{Float32}
        # cards
        @test vector == Float32[
            1.0, 0.0, 0.0, 0.0, # 1s
            1.0, 0.0, 0.0, 0.0, # 2s
            1.0, 0.0, 0.0, 0.0, # 3s
            1.0, 0.0, 0.0, 0.0, # 4s
            0.0, 0.0, 0.0, 0.0, # 5s
            0.0, 0.0, 0.0, 0.0, # 6s
            0.0, 0.0, 0.0, 0.0, # 7s
            0.0, 0.0, 0.0, 0.0, # 8s
            0.0, 0.0, 0.0, 0.0, # 9s
            0.0, 0.0, 0.0, 0.0, # 10s
            0.0, 0.0, 0.0, 0.0, # 11s
            0.0, 0.0, 0.0, 0.0, # 12s
            0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, #13-19
        ]
    end

    @testset "contains cards vector (yellow)" begin
      
        stack = Stack{Just4Fun.CardValue}()
        state = (
          stack=stack, used_cards=Cards(),
          player_cards=SMatrix{4, 2}([
            0x05 0x01 ;
            0x06 0x02 ;
            0x07 0x03 ;
            0x08 0x04
          ]),
          field_stones=SArray{Tuple{6, 6, 2}, Stones}([
            # p1
            0x02 0x00 0x00 0x00 0x00 0x01;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;;;
            # p2
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x00 0x00 0x00 0x00 0x00 0x00;
            0x01 0x00 0x00 0x00 0x00 0x02
          ]),
          player_stones=SVector{2, Stones}(repeat([Stones(20)], 2)),
          curplayer=Player(Just4Fun.YELLOW)
        )
        
        vector = GI.vectorize_state(Just4FunSpec(), state)
        
        @test vector isa Array{Float32}
        # cards
        @test vector == Float32[
            0.0, 0.0, 0.0, 0.0, # 1s
            0.0, 0.0, 0.0, 0.0, # 2s
            0.0, 0.0, 0.0, 0.0, # 3s
            0.0, 0.0, 0.0, 0.0, # 4s
            1.0, 0.0, 0.0, 0.0, # 5s
            1.0, 0.0, 0.0, 0.0, # 6s
            1.0, 0.0, 0.0, 0.0, # 7s
            1.0, 0.0, 0.0, 0.0, # 8s
            0.0, 0.0, 0.0, 0.0, # 9s
            0.0, 0.0, 0.0, 0.0, # 10s
            0.0, 0.0, 0.0, 0.0, # 11s
            0.0, 0.0, 0.0, 0.0, # 12s
            0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, #13-19
        ]
    end
end