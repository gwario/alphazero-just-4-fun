using Test

using Just4Fun

@testset "CARD_VECTOR_ORDER complete (default)" begin
    @test issetequal(CARD_VECTOR_ORDER, Just4Fun.JUST4FUN_DEFAULT_DECK)
    @test issetequal(CARD_VECTOR_ORDER, convert(Array{Int64}, Just4Fun.DECK))
end


@testset "NN_OUTPUT_FIELD_VALUE_MAPPING complete (default)" begin
    @test all(map(fv -> fv in Just4Fun.JUST4FUN_DEFAULT_ORIGINAL_DIST, NN_OUTPUT_FIELD_VALUE_MAPPING))
    @test length(Just4Fun.JUST4FUN_DEFAULT_ORIGINAL_DIST) == length(NN_OUTPUT_FIELD_VALUE_MAPPING)
    
    @test all(map(fv -> fv in Just4Fun.FIELD_VALUES, NN_OUTPUT_FIELD_VALUE_MAPPING))
    @test length(Just4Fun.FIELD_VALUES) == length(NN_OUTPUT_FIELD_VALUE_MAPPING)
end


@testset "board_position_mask(g::Just4FunEnv)" begin

    state = (
      stack=Stack{Just4Fun.CardValue}(), used_cards=Cards(),
      player_cards=SMatrix{4, 2}([
        0x01 0x01 ;
        0x02 0x02 ;
        0x03 0x03 ;
        0x04 0x04
      ]),
      field_stones = SArray{Tuple{6, 6, 2}, Stones}([
        # p1
        0x00 0x00 0x00 0x00 0x00 0x01;
        0x00 0x00 0x00 0x00 0x01 0x00;
        0x00 0x00 0x00 0x01 0x00 0x00;
        0x00 0x00 0x01 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;;;
        # p2
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00
      ]),
      player_stones=SVector{2, Stones}(repeat([Stones(0)], 2)),
      curplayer=Player(Just4Fun.YELLOW),
      state=in_progress,
      action_indices=[]
    )
    game = GI.init(Just4FunSpec(), state)
    av_actions = GI.available_actions(game)
    @show av_actions
    # av_actions = NamedTuple{(:cards, :value), Tuple{Vector{UInt8}, UInt8}}[
    #    (cards = [0x01], value = 0x01), (cards = [0x02], value = 0x02), (cards = [0x03], value = 0x03), (cards = [0x04], value = 0x04),
    #    (cards = [0x01, 0x02], value = 0x03), (cards = [0x01, 0x03], value = 0x04), (cards = [0x01, 0x04], value = 0x05),
    #    (cards = [0x02, 0x03], value = 0x05), (cards = [0x02, 0x04], value = 0x06), (cards = [0x03, 0x04], value = 0x07),
    #    (cards = [0x01, 0x02, 0x03], value = 0x06), (cards = [0x01, 0x02, 0x04], value = 0x07),
    #    (cards = [0x01, 0x03, 0x04], value = 0x08), (cards = [0x02, 0x03, 0x04], value = 0x09),
    #    (cards = [0x01, 0x02, 0x03, 0x04], value = 0x0a)
    #]
    #default field value distribution column-wise as array
    #  1, 33,  6, 15, 22, 28, 14, 11, 27, 32, 10,  7, 30,  9, 31,  5, 18, 23,
    # 24, 16, 20, 29, 36,  4, 19, 35,  3, 17, 25, 13,  8, 21, 12, 26,  2, 34
    #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
    # 0x01, 0x21, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
    # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04,
    # 0x13, 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
    #]
    @test board_position_mask(av_actions) == Bool[
        # 0x01, 0x21, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
             1,    0,    1,    0,    0,    0,    0,    0,    0,    0,    1,    1,
        # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04,
             0,    1,    0,    1,    0,    0,    0,    0,    0,    0,    0,    1,
        # 0x13, 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
             0,    0,    1,    0,    0,    0,    1,    0,    0,    0,    1,    0
    ]
end


@testset "board_position_mask(gspec, state) (default)" begin

    state = (
      stack=Stack{Just4Fun.CardValue}(), used_cards=Cards(),
      player_cards=SMatrix{4, 2}([
        0x01 0x01 ;
        0x02 0x02 ;
        0x03 0x03 ;
        0x04 0x04
      ]),
      field_stones = SArray{Tuple{6, 6, 2}, Stones}([
        # p1
        0x00 0x00 0x00 0x00 0x00 0x01;
        0x00 0x00 0x00 0x00 0x01 0x00;
        0x00 0x00 0x00 0x01 0x00 0x00;
        0x00 0x00 0x01 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;;;
        # p2
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00;
        0x00 0x00 0x00 0x00 0x00 0x00
      ]),
      player_stones=SVector{2, Stones}(repeat([Stones(0)], 2)),
      curplayer=Player(Just4Fun.YELLOW),
      state=in_progress,
      action_indices=[]
    )
    #@show GI.available_actions(GI.init(Just4FunSpec(), state))
    # GI.available_actions(GI.init(Just4FunSpec(), state)) = NamedTuple{(:cards, :value), Tuple{Vector{UInt8}, UInt8}}[
    #    (cards = [0x01], value = 0x01), (cards = [0x02], value = 0x02), (cards = [0x03], value = 0x03), (cards = [0x04], value = 0x04),
    #    (cards = [0x01, 0x02], value = 0x03), (cards = [0x01, 0x03], value = 0x04), (cards = [0x01, 0x04], value = 0x05),
    #    (cards = [0x02, 0x03], value = 0x05), (cards = [0x02, 0x04], value = 0x06), (cards = [0x03, 0x04], value = 0x07),
    #    (cards = [0x01, 0x02, 0x03], value = 0x06), (cards = [0x01, 0x02, 0x04], value = 0x07),
    #    (cards = [0x01, 0x03, 0x04], value = 0x08), (cards = [0x02, 0x03, 0x04], value = 0x09),
    #    (cards = [0x01, 0x02, 0x03, 0x04], value = 0x0a)
    #]
    #default field value distribution column-wise as array
    #  1, 33,  6, 15, 22, 28, 14, 11, 27, 32, 10,  7, 30,  9, 31,  5, 18, 23,
    # 24, 16, 20, 29, 36,  4, 19, 35,  3, 17, 25, 13,  8, 21, 12, 26,  2, 34
    #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
    # 0x01, 0x21, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
    # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04,
    # 0x13, 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
    #]
    @test board_position_mask(Just4FunSpec(), state) == Bool[
        # 0x01, 0x21, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
             1,    0,    1,    0,    0,    0,    0,    0,    0,    0,    1,    1,
        # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04,
             0,    1,    0,    1,    0,    0,    0,    0,    0,    0,    0,    1,
        # 0x13, 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
             0,    0,    1,    0,    0,    0,    1,    0,    0,    0,    1,    0
    ]
end

@testset "board_π(game::AbstractGameEnv, π)" begin
  @testset "1 yellow" begin
    stack = Stack{Just4Fun.CardValue}()

    push!(stack, Just4Fun.CardValue(7))
    push!(stack, Just4Fun.CardValue(5))
    push!(stack, Just4Fun.CardValue(6))
    push!(stack, Just4Fun.CardValue(3))
    push!(stack, Just4Fun.CardValue(4))
    push!(stack, Just4Fun.CardValue(2))
    # ----------------------------------------
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(4)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(6)) # p1
    # p1: 2 2 4 6
    # p2: 1 1 1 1
    spec = Just4Fun.Just4FunSpec(stack)
    game = GI.init(spec)
    @test game.curplayer == Just4Fun.YELLOW
    @test length(GI.available_actions(game)) == 11
    # 1 (cards = [0x02], value = 0x02)
    # 2 (cards = [0x04], value = 0x04)
    # 3 (cards = [0x06], value = 0x06)
    # 4 (cards = [0x02, 0x04], value = 0x06)
    # 5 (cards = [0x02, 0x06], value = 0x08)
    # 6 (cards = [0x02, 0x02], value = 0x04)
    # 7 (cards = [0x04, 0x06], value = 0x0a)
    # 8 (cards = [0x02, 0x04, 0x06], value = 0x0c)
    # 9 (cards = [0x02, 0x02, 0x04], value = 0x08)
    # 10 (cards = [0x02, 0x02, 0x06], value = 0x0a)
    # 11 (cards = [0x02, 0x02, 0x04, 0x06], value = 0x0e)

    #default field value distribution column-wise as array
    #  1, 33,  6, 15, 22, 28, 14, 11, 27, 32, 10,  7, 30,  9, 31,  5, 18, 23,
    # 24, 16, 20, 29, 36,  4, 19, 35,  3, 17, 25, 13,  8, 21, 12, 26,  2, 34
    #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
    # 1, 33, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
    # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04, 0x13,
    # 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
    #]
    #    2    4    6    6    8    4    10    12    8    10    14
    π = [2.0, 4.0, 6.0, 6.0, 8.0, 4.0, 10.0, 12.0, 8.0, 10.0, 14.0]
    @test Just4Fun.board_π(game, π) == [6.0, 14.0, 10.0, 4.0, 8.0, 12.0, 2.0]
  end


  @testset "1 red" begin
    stack = Stack{Just4Fun.CardValue}()

    push!(stack, Just4Fun.CardValue(7))
    push!(stack, Just4Fun.CardValue(5))
    push!(stack, Just4Fun.CardValue(6))
    push!(stack, Just4Fun.CardValue(3))
    push!(stack, Just4Fun.CardValue(4))
    push!(stack, Just4Fun.CardValue(2))
    # ----------------------------------------
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(4)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(6)) # p1
    # p1: 2 2 4 6
    # p2: 1 1 1 1
    spec = Just4Fun.Just4FunSpec(stack)
    game = GI.init(spec)
    game.curplayer = Just4Fun.RED
    @test length(GI.available_actions(game)) == 4
    # 1 (cards = [0x01], value = 0x01)
    # 2 (cards = [0x01, 0x01], value = 0x02)
    # 3 (cards = [0x01, 0x01, 0x01], value = 0x03)
    # 4 (cards = [0x01, 0x01, 0x01, 0x01], value = 0x04)

    #default field value distribution column-wise as array
    #  1, 33,  6, 15, 22, 28, 14, 11, 27, 32, 10,  7, 30,  9, 31,  5, 18, 23,
    # 24, 16, 20, 29, 36,  4, 19, 35,  3, 17, 25, 13,  8, 21, 12, 26,  2, 34
    #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
    # 1, 33, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
    # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04, 0x13,
    # 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
    #]

    π = [1.0, 4.0, 3.0, 8.0]
    @test Just4Fun.board_π(game, π) == [1.0, 8.0, 3.0, 4.0]
  end

  @testset "2 yellow" begin
    stack = Stack{Just4Fun.CardValue}()

    push!(stack, Just4Fun.CardValue(7))
    push!(stack, Just4Fun.CardValue(5))
    push!(stack, Just4Fun.CardValue(6))
    push!(stack, Just4Fun.CardValue(3))
    push!(stack, Just4Fun.CardValue(4))
    push!(stack, Just4Fun.CardValue(2))
    # ----------------------------------------
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(3)) # p2
    push!(stack, Just4Fun.CardValue(4)) # p1
    push!(stack, Just4Fun.CardValue(5)) # p2
    push!(stack, Just4Fun.CardValue(6)) # p1
    push!(stack, Just4Fun.CardValue(7)) # p2
    push!(stack, Just4Fun.CardValue(8)) # p1
    # p1: 2 4 6 8
    # p2: 1 3 5 7
    spec = Just4Fun.Just4FunSpec(stack)
    game = GI.init(spec)
    @test game.curplayer == Just4Fun.YELLOW
    @test length(GI.available_actions(game)) == 15
    #  1 (cards = [0x02], value = 0x02),
    #  2 (cards = [0x04], value = 0x04),
    #  3 (cards = [0x06], value = 0x06),
    #  4 (cards = [0x08], value = 0x08),
    #  5 (cards = [0x02, 0x04], value = 0x06),
    #  6 (cards = [0x02, 0x06], value = 0x08),
    #  7 (cards = [0x02, 0x08], value = 0x0a),
    #  8 (cards = [0x04, 0x06], value = 0x0a),
    #  9 (cards = [0x04, 0x08], value = 0x0c),
    # 10 (cards = [0x06, 0x08], value = 0x0e),
    # 11 (cards = [0x02, 0x04, 0x06], value = 0x0c),
    # 12 (cards = [0x02, 0x04, 0x08], value = 0x0e),
    # 13 (cards = [0x02, 0x06, 0x08], value = 0x10),
    # 14 (cards = [0x04, 0x06, 0x08], value = 0x12),
    # 15 (cards = [0x02, 0x04, 0x06, 0x08], value = 0x14)


    #default field value distribution column-wise as array
    #  1, 33,  6, 15, 22, 28, 14, 11, 27, 32, 10,  7, 30,  9, 31,  5, 18, 23,
    # 24, 16, 20, 29, 36,  4, 19, 35,  3, 17, 25, 13,  8, 21, 12, 26,  2, 34
    #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
    # 1, 33, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
    # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04, 0x13,
    # 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
    #]
    #    2    4    6    8    6    8    10    10    12    14    12    14    16    18    20
    π = [2.0, 4.0, 6.0, 8.0, 4.0, 8.0, 10.0, 10.0, 12.0, 14.0, 12.0, 14.0, 16.0, 18.0, 20.0]
    #                                   6    14    10    18    16    20    4    8    12    2
    @test Just4Fun.board_π(game, π) == [6.0, 14.0, 10.0, 18.0, 16.0, 20.0, 4.0, 8.0, 12.0, 2.0]
  end

  @testset "2 red" begin
    stack = Stack{Just4Fun.CardValue}()

    push!(stack, Just4Fun.CardValue(7))
    push!(stack, Just4Fun.CardValue(5))
    push!(stack, Just4Fun.CardValue(6))
    push!(stack, Just4Fun.CardValue(3))
    push!(stack, Just4Fun.CardValue(4))
    push!(stack, Just4Fun.CardValue(2))
    # ----------------------------------------
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(3)) # p2
    push!(stack, Just4Fun.CardValue(4)) # p1
    push!(stack, Just4Fun.CardValue(5)) # p2
    push!(stack, Just4Fun.CardValue(6)) # p1
    push!(stack, Just4Fun.CardValue(7)) # p2
    push!(stack, Just4Fun.CardValue(8)) # p1
    # p1: 2 4 6 8
    # p2: 1 3 5 7
    spec = Just4Fun.Just4FunSpec(stack)
    game = GI.init(spec)
    game.curplayer = Just4Fun.RED
    @test length(GI.available_actions(game)) == 15
    #  1 (cards = [0x01], value = 0x01),
    #  2 (cards = [0x03], value = 0x03),
    #  3 (cards = [0x05], value = 0x05),
    #  4 (cards = [0x07], value = 0x07),
    #  5 (cards = [0x01, 0x03], value = 0x04),
    #  6 (cards = [0x01, 0x05], value = 0x06),
    #  7 (cards = [0x01, 0x07], value = 0x08),
    #  8 (cards = [0x03, 0x05], value = 0x08),
    #  9 (cards = [0x03, 0x07], value = 0x0a),
    # 10 (cards = [0x05, 0x07], value = 0x0c),
    # 11 (cards = [0x01, 0x03, 0x05], value = 0x09),
    # 12 (cards = [0x01, 0x03, 0x07], value = 0x0b),
    # 13 (cards = [0x01, 0x05, 0x07], value = 0x0d),
    # 14 (cards = [0x03, 0x05, 0x07], value = 0x0f),
    # 15 (cards = [0x01, 0x03, 0x05, 0x07], value = 0x10)
    
    #default field value distribution column-wise as array
    #  1, 33,  6, 15, 22, 28, 14, 11, 27, 32, 10,  7, 30,  9, 31,  5, 18, 23,
    # 24, 16, 20, 29, 36,  4, 19, 35,  3, 17, 25, 13,  8, 21, 12, 26,  2, 34
    #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
    # 1, 33, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
    # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04, 0x13,
    # 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
    #]
    #    1    3    5    7    4    6    8    8    10    12    9    11    13    15    16
    π = [1.0, 3.0, 5.0, 7.0, 4.0, 6.0, 8.0, 8.0, 10.0, 12.0, 9.0, 11.0, 13.0, 15.0, 16.0]
    #                                   1    6    15    11    10    7    9    5    16    4    3    13    8    12
    @test Just4Fun.board_π(game, π) == [1.0, 6.0, 15.0, 11.0, 10.0, 7.0, 9.0, 5.0, 16.0, 4.0, 3.0, 13.0, 8.0, 12.0]
  end
end

@testset "cards_actions_indices(available_actions::Vector{CardsAction}) (default)" begin

  #default field value distribution column-wise as array
  #  1, 33,  6, 15, 22, 28,
  # 14, 11, 27, 32, 10,  7,
  # 30,  9, 31,  5, 18, 23,
  # 24, 16, 20, 29, 36,  4,
  # 19, 35,  3, 17, 25, 13,
  #  8, 21, 12, 26,  2, 34
  #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
  #    1, 33, 0x06, 0x0f, 0x16, 0x1c,
  #    0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
  #    0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17,
  #    0x18, 0x10, 0x14, 0x1d, 0x24, 0x04,
  #    0x13, 0x23, 0x03, 0x11, 0x19, 0x0d,
  #    0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
  #]
  @test cards_actions_indices([(cards=CardValue[], value=CardValue(0))])  == []
  @test cards_actions_indices([(cards=[CardValue(1), CardValue(2), CardValue(3), CardValue(4)], value=CardValue(10))]) == [11]
  @test cards_actions_indices([(cards=[CardValue(2), CardValue(3), CardValue(4)], value=CardValue(9))])  == [14]
  @test cards_actions_indices([(cards=[CardValue(4), CardValue(12), CardValue(18)], value=CardValue(34))]) == [36]
end

@testset "[UNUSED] unique_value_π(game::AbstractGameEnv, π)" begin
    stack = Stack{Just4Fun.CardValue}()
  
    push!(stack, Just4Fun.CardValue(7))
    push!(stack, Just4Fun.CardValue(5))
    push!(stack, Just4Fun.CardValue(6))
    push!(stack, Just4Fun.CardValue(3))
    push!(stack, Just4Fun.CardValue(4))
    push!(stack, Just4Fun.CardValue(2))
    # ----------------------------------------
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(2)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(4)) # p1
    push!(stack, Just4Fun.CardValue(1)) # p2
    push!(stack, Just4Fun.CardValue(6)) # p1
    # p1: 2 2 4 6
    # p2: 1 1 1 1
    spec = Just4Fun.Just4FunSpec(stack)
    game = GI.init(spec)
    @test game.curplayer == Just4Fun.YELLOW
    @test length(GI.available_actions(game)) == 11
    # 1 (cards = [0x02], value = 0x02)
    # 2 (cards = [0x04], value = 0x04)
    # 3 (cards = [0x06], value = 0x06)
    # 4 (cards = [0x02, 0x04], value = 0x06)
    # 5 (cards = [0x02, 0x06], value = 0x08)
    # 6 (cards = [0x02, 0x02], value = 0x04)
    # 7 (cards = [0x04, 0x06], value = 0x0a)
    # 8 (cards = [0x02, 0x04, 0x06], value = 0x0c)
    # 9 (cards = [0x02, 0x02, 0x04], value = 0x08)
    # 10 (cards = [0x02, 0x02, 0x06], value = 0x0a)
    # 11 (cards = [0x02, 0x02, 0x04, 0x06], value = 0x0e)

    #default field value distribution column-wise as array
    #  1, 33,  6, 15, 22, 28, 14, 11, 27, 32, 10,  7, 30,  9, 31,  5, 18, 23,
    # 24, 16, 20, 29, 36,  4, 19, 35,  3, 17, 25, 13,  8, 21, 12, 26,  2, 34
    #NN_OUTPUT_FIELD_VALUE_MAPPING = UInt8[
    # 1, 33, 0x06, 0x0f, 0x16, 0x1c, 0x0e, 0x0b, 0x1b, 0x20, 0x0a, 0x07,
    # 0x1e, 0x09, 0x1f, 0x05, 0x12, 0x17, 0x18, 0x10, 0x14, 0x1d, 0x24, 0x04, 0x13,
    # 0x23, 0x03, 0x11, 0x19, 0x0d, 0x08, 0x15, 0x0c, 0x1a, 0x02, 0x22
    #]

    π = [2.0, 4.0, 6.0, 6.0, 8.0, 4.0, 10.0, 12.0, 8.0, 10.0, 14.0]
    #@show Just4Fun.unique_value_π(game, π) # Float32[1.0, 2.0, 3.0, 5.0, 7.0, 8.0, 11.0]
    #@test Just4Fun.unique_value_π(game, π) == [6.0, 14.0, 10.0, 4.0, 8.0, 12.0, 2.0]
    @test_throws AssertionError Just4Fun.unique_value_π(game, π)
end