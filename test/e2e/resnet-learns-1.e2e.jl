using Test

using Just4Fun
using DataStructures

@testset "After training for a certain move, the move gets high probability" begin
    @info "Running scenario $(@__FILE__)..."

    include("../../src/networks/inputs/ma-do.jl")
    
    gspec = Just4FunSpec()
    network = ResNet
    netparams = NetLib.ResNetHP(
      num_filters=2,
      num_blocks=1,
      conv_kernel_size=(3, 3),
      num_policy_head_filters=2,
      num_value_head_filters=1
    )

    self_play_params = SelfPlayParams(
      sim=SimParams(
        num_games=600,
        num_workers=64,
        batch_size=64,
        use_gpu=true
      ),
      mcts=MctsParams(
        num_iters_per_turn=10,
        cpuct=1.5,
        prior_temperature=1.0, temperature=PLSchedule([0, 20, 30], [1.0, 1.0, 0.3]),
        dirichlet_noise_ϵ=0.25, dirichlet_noise_α=1.0
      )
    )
    
    learning_params = LearningParams(
      use_gpu=true,
      samples_weighing_policy=LOG_WEIGHT,
      batch_size=64, loss_computation_batch_size=64,
      optimiser=Network.Adam(lr=2e-3),
      l2_regularization=1e-4,
      min_checkpoints_per_epoch=1, max_batches_per_checkpoint=500, num_checkpoints=1
    )

    params = Params(
      arena=nothing, self_play=self_play_params, learning=learning_params,
      num_iters=100, ternary_rewards=true, mem_buffer_size=PLSchedule(
        [0, 15], [400_000, 1_000_000]
      )
    )
    
    benchmark_sim = SimParams(
      self_play_params.sim;
      num_games=1000,
      num_workers=64,
      batch_size=64
    )
    e = Experiment(@__FILE__, gspec, params, network, netparams, [Benchmark.Duel(Benchmark.RandomPlayer(), Benchmark.RandomPlayer(), benchmark_sim)])
    sess = Session(e; autosave=false, dir="../../sessions/$(e.name)")

    player = AlphaZeroPlayer(sess.env; mcts_params=params.self_play.mcts)
    
    # 1. load state
    lines = reverse(filter(not_comment, chomp.(readlines("./test/states/winning-preventing-action/pattern_c-1-1_p-1_simple.txt"))))
    state = _read_state!(lines)
    
    game = GI.init(sess.env.gspec, state)

    exp = Explorer(player, game)
    
    GI.render(exp.game)

    target_action_value = 15

    # 2. check the 'right' move has 'low' predicted probability
    stats = UI.state_statistics(exp.game, exp.player, exp.turn, exp.memory)
    UI.print_state_statistics(GI.spec(exp.game), stats)

    action_stats_index = findfirst(tuple -> tuple[1].value == UInt8(target_action_value), stats.actions)

    @test !isnothing(action_stats_index)

    target_action, action_stats, network_stats = stats.actions[action_stats_index]

    @test action_stats.Pnet < 0.1

    # 3. load sample in memory
    empty!(sess.env.memory)
    lines = reverse(filter(not_comment, chomp.(readlines("./test/samples/pattern_c-1-1_p-1_simple_action-win.txt"))))
    initial_state = _read_state!(lines)
    trace = Trace(initial_state)
    while !isempty(lines)
        push!(trace,
          _read_policy!(lines),
          _read_reward!(lines),
          _read_state!(lines)
        )
    end
    AlphaZero.push_trace!(sess.env.memory, trace, exp.player.mcts.gamma)

    @test length(sess.env.memory) == 1

    # 4. train for 100 iterations on memory
    for _ in 1:100
      AlphaZero.learning_step!(sess.env, sess)
    end
    @assert exp.player.mcts.oracle != sess.env.bestnn "Training did not change the network!"
    exp.player.mcts.oracle = sess.env.bestnn
    
    # 5. check the 'right' move has 'high' predicted probability now
    stats = UI.state_statistics(exp.game, exp.player, exp.turn, exp.memory)
    UI.print_state_statistics(GI.spec(exp.game), stats)

    action_stats_index = findfirst(tuple -> tuple[1].value == UInt8(target_action_value), stats.actions)

    @test !isnothing(action_stats_index)

    target_action, action_stats, network_stats = stats.actions[action_stats_index]

    @test action_stats.P > 0.95
    @test action_stats.Pnet > 0.90
    
    GI.render(exp.game)
end