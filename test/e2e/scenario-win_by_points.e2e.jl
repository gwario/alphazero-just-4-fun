using Test

using Just4Fun
using DataStructures

@testset "Win by points with scripted players" begin
    @info "Running scenario $(@__FILE__)..."

    #GI.render(GI.init(Just4FunSpec()))

    cv(v::Int)::CardValue = CardValue(v)

    lifo_stack = Stack{CardValue}()
    # supply a order of actions for each player to the game
    player1_actions = Queue{CardsAction}()
    player2_actions = Queue{CardsAction}()

    # -- Initial deal below --------
    push!(lifo_stack, cv(3)) # p1
    push!(lifo_stack, cv(5)) # p2
    push!(lifo_stack, cv(8)) # p1
    push!(lifo_stack, cv(9)) # p2
    push!(lifo_stack, cv(16))# p1
    push!(lifo_stack, cv(9)) # p2
    push!(lifo_stack, cv(4)) # p1
    push!(lifo_stack, cv(2)) # p2
    
    # p1 = [  3  8 16  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(8)], value=11)))
    # p1 = [       16  4 ]
    push!(lifo_stack, cv(7)) # p1 = [  7    16  4 ]
    push!(lifo_stack, cv(12))# p1 = [  7 12 16  4 ]
    
    # p2 = [  5  9  9  2 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(9), cv(9)],  value=23)))
    # p2 = [           2 ]
    push!(lifo_stack, cv(3)) # p2 = [  3        2 ]
    push!(lifo_stack, cv(11))# p2 = [  3 11     2 ]
    push!(lifo_stack, cv(17))# p2 = [  3 11 17  2 ]
    
    # 1 stone(s) on the board

    # p1 = [  7 12 16  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(16)], value=16)))
    # p1 = [  7 12     4 ]
    push!(lifo_stack, cv(12))# p1 = [  7 12 12  4 ]

    # p2 = [  3 11 17  2 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(3), cv(11)], value=16)))
    # p2 = [       17    ]
    push!(lifo_stack, cv(2))# p2 = [  2    17    ]
    push!(lifo_stack, cv(3))# p2 = [  2  3 17    ]
    push!(lifo_stack, cv(6))# p2 = [  2  3 17  6 ]

    # 2 stone(s) on the board

    # p1 = [  7 12 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(4), cv(12)], value=16)))
    # p1 = [  7    12    ]
    push!(lifo_stack, cv(7)) # p1 = [  7  7 12    ]
    push!(lifo_stack, cv(14))# p1 = [  7  7 12 14 ]
    
    # p2 = [  2  3 17  6 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(17)], value=17)))
    # p2 = [  2  3     6 ]
    push!(lifo_stack, cv(4))# p2 = [  2  3  4  6 ]

    # 3 stone(s) on the board

    # p1 = [  7  7 12 14 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(14)], value=21)))
    # p1 = [     7 12    ]
    push!(lifo_stack, cv(6)) # p1 = [  6  7 12    ]
    push!(lifo_stack, cv(9)) # p1 = [  6  7 12  9 ]
    
    # p2 = [  2  3  4  6 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(3), cv(6)], value=11)))
    # p2 = [        4    ]
    push!(lifo_stack, cv(2)) # p2 = [  2     4     ]
    push!(lifo_stack, cv(7)) # p2 = [  2  7  4     ]
    push!(lifo_stack, cv(10))# p2 = [  2  7  4  10 ]

    # 4 stone(s) on the board

    # p1 = [  6  7 12  9 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(6)], value=6)))
    # p1 = [     7 12  9 ]
    push!(lifo_stack, cv(4)) # p1 = [  4  7 12  9 ]

    # p2 = [  2  7  4  10 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(4)], value=6)))
    # p2 = [     7     10 ]
    push!(lifo_stack, cv(5)) # p2 = [  5  7    10 ]
    push!(lifo_stack, cv(8)) # p2 = [  5  7  8 10 ]

    # 5 stone(s) on the board

    # p1 = [  4  7 12  9 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(9)], value=16)))
    # p1 = [  4    12    ]
    push!(lifo_stack, cv(2)) # p1 = [  4  2 12    ]
    push!(lifo_stack, cv(4)) # p1 = [  4  2 12  4 ]

    # p2 = [  5  7  8 10 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(7), cv(10)], value=17)))
    # p2 = [  5     8    ]
    push!(lifo_stack, cv(8)) # p2 = [  5  8  8    ]
    push!(lifo_stack, cv(11))# p2 = [  5  8  8 11 ]

    # 6 stone(s) on the board

    # p1 = [  4  2 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(2), cv(4)], value=6)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(6)) # p1 = [  6    12  4 ]
    push!(lifo_stack, cv(5)) # p1 = [  6  5 12  4 ]
    
    # p2 = [  5  8  8 11 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(8), cv(8)], value=21)))
    # p2 = [          11 ]
    push!(lifo_stack, cv(12))# p2 = [ 12       11 ]
    push!(lifo_stack, cv(1)) # p2 = [ 12  1    11 ]
    push!(lifo_stack, cv(6)) # p2 = [ 12  1  6 11 ]

    # 7 stone(s) on the board

    # p1 = [  6  5 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(5), cv(6)], value=11)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(19))# p1 = [ 19    12  4 ]
    push!(lifo_stack, cv(7)) # p1 = [ 19  7 12  4 ]

    # p2 = [ 12  1  6 11 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(11)], value=11)))
    # p2 = [ 12  1  6    ]
    push!(lifo_stack, cv(12))# p2 = [ 12  1  6 12 ]

    # 8 stone(s) on the board

    # p1 = [ 19  7 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(19)], value=26)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(1)) # p1 = [  1    12  4 ]
    push!(lifo_stack, cv(10))# p1 = [  1 10 12  4 ]

    # p2 = [ 12  1  6 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(6), cv(12)], value=19)))
    # p2 = [          12 ]
    push!(lifo_stack, cv(1)) # p2 = [  1       12 ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1    12 ]
    push!(lifo_stack, cv(4)) # p2 = [  1  1  4 12 ]

    # 9 stone(s) on the board

    # p1 = [  1 10 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(10)], value=11)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(11))# p1 = [ 11    12  4 ]
    push!(lifo_stack, cv(1)) # p1 = [ 11  1 12  4 ]

    # p2 = [  1  1  4 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(12)], value=12)))
    # p2 = [  1  1  4    ]
    push!(lifo_stack, cv(12)) # p2 = [  1  1  4 12 ]

    # 10 stones each placed - p1/yellow: 59, p2/red: 71 

    # p1 = [ 11  1 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(4), cv(11)], value=15)))
    # p1 = [     1 12    ]
    push!(lifo_stack, cv(15))# p1 = [ 15  1 12    ]
    push!(lifo_stack, cv(1)) # p1 = [ 15  1 12  1 ]

    # p2 = [  1  1  4 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(12)], value=12)))
    # p2 = [  1  1  4    ]
    push!(lifo_stack, cv(17)) # p2 = [  1  1  4 17 ]

    # 11 stones each placed

    # p1 = [ 15  1 12  1 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(1), cv(12)], value=14)))
    # p1 = [ 15          ]
    push!(lifo_stack, cv(1) )# p1 = [ 15  1       ]
    push!(lifo_stack, cv(2)) # p1 = [ 15  1  2    ]
    push!(lifo_stack, cv(8)) # p1 = [ 15  1  2  8 ]

    # p2 = [  1  1  4 17 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(17)], value=18)))
    # p2 = [     1  4    ]
    push!(lifo_stack, cv(17)) # p2 = [ 17  1  4    ]
    push!(lifo_stack, cv(12)) # p2 = [ 17  1  4 12 ]

    # 12 stones each placed

    # p1 = [ 15  1  2  8 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(2)], value=2)))
    # p1 = [ 15  1     8 ]
    push!(lifo_stack, cv(11)) # p1 = [ 15  1 11  8 ]

    # p2 = [ 17  1  4 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(4), cv(12), cv(17)], value=33)))
    # p2 = [     1       ]
    push!(lifo_stack, cv(11))# p2 = [ 11  1       ]
    push!(lifo_stack, cv(2)) # p2 = [ 11  1  2    ]
    push!(lifo_stack, cv(8)) # p2 = [ 11  1  2  8 ]

    # 13 stones each placed

    # p1 = [ 15  1 11  8 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(8)], value=8)))
    # p1 = [ 15  1 11    ]
    push!(lifo_stack, cv(8)) # p1 = [ 15  1 11  8 ]

    # p2 = [ 11  1  2  8 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(8)], value=8)))
    # p2 = [ 11  1  2    ]
    push!(lifo_stack, cv(8)) # p2 = [ 11  1  2  8 ]

    # 14 stones each placed

    # p1 = [ 15  1 11  8 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(8)], value=9)))
    # p1 = [ 15    11    ]
    push!(lifo_stack, cv(6)) # p1 = [ 15  6 11    ]
    push!(lifo_stack, cv(3)) # p1 = [ 15  6 11  3 ]
    
    # p2 = [ 11  1  2  8 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(8)], value=9)))
    # p2 = [ 11     2    ]
    push!(lifo_stack, cv(19))# p2 = [ 11 19  2    ]
    push!(lifo_stack, cv(13))# p2 = [ 11 19  2 13 ]

    # 15 stones each placed

    # p1 = [ 15  6 11  3 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(6), cv(11), cv(15)], value=32)))
    # p1 = [           3 ]
    push!(lifo_stack, cv(1)) # p1 = [  1        3 ]
    push!(lifo_stack, cv(7)) # p1 = [  1  7     3 ]
    push!(lifo_stack, cv(11))# p1 = [  1  7 11  3 ]

    # p2 = [ 11 19  2 13 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(11), cv(19)], value=32)))
    # p2 = [          13 ]
    push!(lifo_stack, cv(4)) # p2 = [  4       13 ]
    push!(lifo_stack, cv(9)) # p2 = [  4  9    13 ]
    push!(lifo_stack, cv(12))# p2 = [  4  9 12 13 ]

    # 16 stones each placed

    # p1 = [  1  7 11  3 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(3)], value=4)))
    # p1 = [     7 11    ]
    push!(lifo_stack, cv(13))# p1 = [ 13  7 11    ]
    push!(lifo_stack, cv(2)) # p1 = [ 13  7 11  2 ]

    # p2 = [  4  9 12 13 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(4)], value=4)))
    # p2 = [     9 12 13 ]
    push!(lifo_stack, cv(2)) # p2 = [  2  9 12 13 ]
    
    # 17 stones each placed

    # p1 = [ 13  7 11  2 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(13)], value=13)))
    # p1 = [     7 11  2 ]
    push!(lifo_stack, cv(12))# p1 = [ 12  7 11  2 ]

    # p2 = [  2  9 12 13 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(13)], value=13)))
    # p2 = [  2  9 12    ]
    push!(lifo_stack, cv(9)) # p2 = [  2  9 12  9 ]
    
    # 18 stones each placed

    # p1 = [ 12  7 11  2 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(2)], value=2)))
    # p1 = [ 12  7 11    ]
    push!(lifo_stack, cv(3))# p1 = [ 12  7 11  3 ]

    # p2 = [  2  9 12  9 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(9)], value=11)))
    # p2 = [       12  9 ]
    push!(lifo_stack, cv(4)) # p2 = [  4    12  9 ]
    push!(lifo_stack, cv(5)) # p2 = [  4  5 12  9 ]
    
    # 19 stones each placed

    # p1 = [ 12  7 11  3 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(11), cv(12)], value=26)))
    # p1 = [     7       ]
    push!(lifo_stack, cv(1))# p1 = [  1  7       ]
    push!(lifo_stack, cv(2))# p1 = [  1  7  2    ]
    push!(lifo_stack, cv(3))# p1 = [  1  7  2  3 ]

    # p2 = [  4  5 12  9 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(4), cv(5), cv(9), cv(12)], value=30)))
    # p2 = [             ]
    push!(lifo_stack, cv(1)) # p2 = [  1          ]
    push!(lifo_stack, cv(2)) # p2 = [  1  2       ]
    push!(lifo_stack, cv(3)) # p2 = [  1  2  3    ]
    push!(lifo_stack, cv(4)) # p2 = [  1  2  3  4 ]

    # 20 stones each placed

    # end
    # p1 = [  1  7  2  3 ]
    # p2 = [  1  2  3  4 ]

    # prepare stack data structure
    stack = Stack{CardValue}()
    for c in lifo_stack
        push!(stack, c)
    end

    spec = Just4FunSpec(stack)
    player1 = AlphaZero.ScriptedPlayer(player1_actions, nothing)
    player2 = AlphaZero.ScriptedPlayer(player2_actions, nothing)

    try
        Scripts.play_game(spec, TwoPlayers(player1, player2))
    catch error
        rethrow(error)
    finally
        GI.render(player1.game)
    end
    
    # p1 = [  1  7  2  3 ]
    # p2 = [  1  2  3  4 ]
    # check sate
    @test player1.game.player_cards == @SMatrix [
        cv(1) cv(1) ;
        cv(7) cv(2) ;
        cv(2) cv(3) ;
        cv(3) cv(4)
    ]
    # YELLOW: 80
    # RED: 152
    #@test game.field_stones TODO:
    @test player1.game.player_stones == SVector{2,Stones}(0, 0)
    @test length(player1.game.stack) == 0
    @test player1.game.curplayer == Just4Fun.Player(Just4Fun.RED) 
    @test player1.game.winner == Just4Fun.Player(Just4Fun.RED)
    @test player1.game.state == end_by_points
    
    # check api
    @test GI.game_terminated(player1.game) == true
    @test GI.game_terminated(player2.game) == true
    @test GI.white_reward(player1.game) == -1.0 # first player / yellow (just 4 fun) / white (framework name)
    @test GI.white_reward(player2.game) == -1.0
    @test length(player1_actions) == 0
    @test length(player2_actions) == 0
end