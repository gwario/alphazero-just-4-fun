using Test


using Just4Fun
using DataStructures

@testset "Win by highest position with scripted players" begin
    @info "Running scenario $(@__FILE__)..."

    #GI.render(GI.init(Just4FunSpec()))

    cv(v::Int)::CardValue = CardValue(v)

    lifo_stack = Stack{CardValue}()
    # supply a order of actions for each player to the game
    player1_actions = Queue{CardsAction}()
    player2_actions = Queue{CardsAction}()

    # -- Initial deal below --------
    push!(lifo_stack, cv(3)) # p1
    push!(lifo_stack, cv(3)) # p2
    push!(lifo_stack, cv(8)) # p1
    push!(lifo_stack, cv(8)) # p2
    push!(lifo_stack, cv(16))# p1
    push!(lifo_stack, cv(16))# p2
    push!(lifo_stack, cv(4)) # p1
    push!(lifo_stack, cv(8)) # p2
    
    # p1 = [  3  8 16  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(8)], value=11)))
    # p1 = [       16  4 ]
    push!(lifo_stack, cv(7)) # p1 = [  7    16  4 ]
    push!(lifo_stack, cv(12))# p1 = [  7 12 16  4 ]
    
    # p2 = [  3  8 16  8 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(3), cv(8), cv(8), cv(16)], value=35)))
    # p2 = [             ]
    push!(lifo_stack, cv(4)) # p2 = [  4          ]
    push!(lifo_stack, cv(12))# p2 = [  4 12       ]
    push!(lifo_stack, cv(13))# p2 = [  4 12 13    ]
    push!(lifo_stack, cv(5)) # p2 = [  4 12 13  5 ]
    
    # 2 stone on the board

    # p1 = [  7 12 16  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(16)], value=16)))
    # p1 = [  7 12     4 ]
    push!(lifo_stack, cv(12))# p1 = [  7 12 12  4 ]

    # p2 = [  4 12 13  5 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(4)], value=4)))
    # p2 = [    12 13  5 ]
    push!(lifo_stack, cv(1)) # p2 = [  1 12 13  5 ]
    
    # 4 stone on the board

    # p1 = [  7 12 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(4), cv(12)], value=16)))
    # p1 = [  7    12    ]
    push!(lifo_stack, cv(7)) # p1 = [  7  7 12    ]
    push!(lifo_stack, cv(14))# p1 = [  7  7 12 14 ]

    # p2 = [  1 12 13  5 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(12)], value=12)))
    # p2 = [  1    13  5 ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1 13  5 ]
    
    # 6 stone on the board

    # p1 = [  7  7 12 14 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(14)], value=21)))
    # p1 = [     7 12    ]
    push!(lifo_stack, cv(6)) # p1 = [  6  7 12    ]
    push!(lifo_stack, cv(9)) # p1 = [  6  7 12  9 ]

    # p2 = [  1  1 13  5 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(13)], value=13)))
    # p2 = [  1  1     5 ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1  1  5 ]
    
    # 8 stone on the board

    # p1 = [  6  7 12  9 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(6)], value=6)))
    # p1 = [     7 12  9 ]
    push!(lifo_stack, cv(4)) # p1 = [  4  7 12  9 ]

    # p2 = [  1  1  1  5 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(5)], value=5)))
    # p2 = [  1  1  1  5 ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1  1  1 ]

    # 10 stone on the board

    # p1 = [  4  7 12  9 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(4), cv(7), cv(9), cv(12)], value=32)))
    # p1 = [             ]
    push!(lifo_stack, cv(7)) # p1 = [  7          ]
    push!(lifo_stack, cv(3)) # p1 = [  7  3       ]
    push!(lifo_stack, cv(4)) # p1 = [  7  3  4    ]
    push!(lifo_stack, cv(13))# p1 = [  7  3  4 13 ]

    # p2 = [  1  1  1  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(1), cv(1)], value=3)))
    # p2 = [           1 ]
    push!(lifo_stack, cv(17)) # p2 = [ 17        1 ]
    push!(lifo_stack, cv(11)) # p2 = [ 17 11     1 ]
    push!(lifo_stack, cv(9))  # p2 = [ 17 11  9  1 ]

    # 12 stone on the board

    # p1 = [  7  3  4 13 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(4), cv(7)], value=14)))
    # p1 = [          13 ]
    push!(lifo_stack, cv(12))# p1 = [ 12       13 ]
    push!(lifo_stack, cv(7)) # p1 = [ 12  7    13 ]
    push!(lifo_stack, cv(15))# p1 = [ 12  7 15 13 ]

    # p2 = [ 17 11  9  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(11), cv(17)], value=29)))
    # p2 = [        9    ]
    push!(lifo_stack, cv(18)) # p2 = [ 18     9    ]
    push!(lifo_stack, cv(7))  # p2 = [ 18  7  9    ]
    push!(lifo_stack, cv(17)) # p2 = [ 18  7  9 17 ]

    # 14 stone on the board

    # p1 = [ 12  7 15 13 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(12), cv(13)], value=32)))
    # p1 = [       15    ]
    push!(lifo_stack, cv(3)) # p1 = [  3    15    ]
    push!(lifo_stack, cv(1)) # p1 = [  3  1 15    ]
    push!(lifo_stack, cv(1)) # p1 = [  3  1 15  1 ]

    # p2 = [ 18  7  9 17 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(9), cv(18)], value=27)))
    # p2 = [     7    17 ]
    push!(lifo_stack, cv(3)) # p2 = [  3  7    17 ]
    push!(lifo_stack, cv(5)) # p2 = [  3  7  5 17 ]
    
    # 16 stone on the board

    # p1 = [  3  1 15  1 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(1)], value=2)))
    # p1 = [  3    15    ]
    push!(lifo_stack, cv(14))# p1 = [  3 14 15    ]
    push!(lifo_stack, cv(3)) # p1 = [  3 14 15  3 ]

    # p2 = [  3  7  5 17 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(7), cv(17)], value=24)))
    # p2 = [  3     5    ]
    push!(lifo_stack, cv(18))# p2 = [  3 18  5    ]
    push!(lifo_stack, cv(8)) # p2 = [  3 18  5  8 ]
    
    # 18 stone on the board

    # p1 = [  3 14 15  3 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(14), cv(15)], value=29)))
    # p1 = [  3        3 ]
    push!(lifo_stack, cv(7))# p1 = [  3  7     3 ]
    push!(lifo_stack, cv(7))# p1 = [  3  7  7  3 ]

    # p2 = [  3 18  5  8 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(3), cv(8), cv(18)], value=29)))
    # p2 = [        5    ]
    push!(lifo_stack, cv(18)) # p2 = [ 18     5    ]
    push!(lifo_stack, cv(6))  # p2 = [ 18  6  5    ]
    push!(lifo_stack, cv(12)) # p2 = [ 18  6  5 12 ]

    # 20 stone on the board

    # p1 = [  3  7  7  3 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(3)], value=6)))
    # p1 = [     7  7    ]
    push!(lifo_stack, cv(14))# p1 = [ 14  7  7    ]
    push!(lifo_stack, cv(7)) # p1 = [ 14  7  7  7 ]

    # p2 = [ 18  6  5 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(6), cv(18)], value=29)))
    # p2 = [          12 ]
    push!(lifo_stack, cv(3)) # p2 = [  3       12 ]
    push!(lifo_stack, cv(6)) # p2 = [  3  6    12 ]
    push!(lifo_stack, cv(7)) # p2 = [  3  6  7 12 ]

    # 22 stone on the board

    # p1 = [ 14  7  7  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(7)], value=14)))
    # p1 = [ 14        7 ]
    push!(lifo_stack, cv(4)) # p1 = [ 14  4     7 ]
    push!(lifo_stack, cv(3)) # p1 = [ 14  4  3  7 ]

    # p2 = [  3  6  7 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(12)], value=12)))
    # p2 = [  3  6  7   ]
    push!(lifo_stack, cv(6)) # p2 = [  3  6  7  6 ]

    # 24 stone on the board

    # p1 = [ 14  4  3  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(4), cv(14)], value=21)))
    # p1 = [           7 ]
    push!(lifo_stack, cv(12)) # p1 = [ 12        7 ]
    push!(lifo_stack, cv(16)) # p1 = [ 12 16     7 ]
    push!(lifo_stack, cv(1))  # p1 = [ 12 16  1  7 ]

    # p2 = [  3  6  7  6 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(6), cv(7)], value=13)))
    # p2 = [  3  6       ]
    push!(lifo_stack, cv(14)) # p2 = [  3  6 14    ]
    push!(lifo_stack, cv(2))  # p2 = [  3  6 14  2 ]

    # 26 stone on the board

    # p1 = [ 12 16  1  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(12), cv(16)], value=28)))
    # p1 = [        1  7 ]
    push!(lifo_stack, cv(10)) # p1 = [ 10  7  1  7 ]
    push!(lifo_stack, cv(7))  # p1 = [ 10  7  1  7 ]

    # p2 = [  3  6 14  2 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(3), cv(6), cv(14)], value=25)))
    # p2 = [             ]
    push!(lifo_stack, cv(1)) # p2 = [  1          ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1       ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1   1   ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1  1  1 ]

    # 28 stone on the board

    # p1 = [ 10  7  1  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(10)], value=17)))
    # p1 = [        1  7 ]
    push!(lifo_stack, cv(2))  # p1 = [  2     1  7 ]
    push!(lifo_stack, cv(16)) # p1 = [  2 16  1  7 ]

    # p2 = [  1  1  1  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(1), cv(1), cv(1)], value=4)))
    # p2 = [             ]
    push!(lifo_stack, cv(1)) # p2 = [  1          ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1       ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1  1    ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1  1  1 ]

    # 30 stone on the board

    # p1 = [  2 16  1  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(2)], value=2)))
    # p1 = [    16  1  7 ]
    push!(lifo_stack, cv(5)) # p1 = [  5 16  1  7 ]

    # p2 = [  1  1  1  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(1), cv(1)], value=3)))
    # p2 = [           1 ]
    push!(lifo_stack, cv(3)) # p2 = [  3          ]
    push!(lifo_stack, cv(2)) # p2 = [  3  2       ]
    push!(lifo_stack, cv(19))# p2 = [  3  2 19  1 ]

    # 32 stone on the board

    # p1 = [  5 16  1  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(5), cv(7), cv(16)], value=28)))
    # p1 = [        1     ]
    push!(lifo_stack, cv(15)) # p1 = [ 15 14  1    ]
    push!(lifo_stack, cv(14)) # p1 = [ 15 14  1    ]
    push!(lifo_stack, cv(7))  # p1 = [ 15 14  1  7 ]

    # p2 = [  3  2 19  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(3)], value=5)))
    # p2 = [       19  1 ]
    push!(lifo_stack, cv(5)) # p2 = [  5    19  1 ]
    push!(lifo_stack, cv(2)) # p2 = [  5  2 19  1 ]
    
    # 34 stone on the board

    # p1 = [ 15 14  1  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(14), cv(15)], value=30)))
    # p1 = [           7 ]
    push!(lifo_stack, cv(10)) # p1 = [ 10        7 ]
    push!(lifo_stack, cv(14)) # p1 = [ 10 14     7 ]
    push!(lifo_stack, cv(12)) # p1 = [ 10 14 12  7 ]

    # p2 = [  5  2 19  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(5)], value=7)))
    # p2 = [       19  1 ]
    push!(lifo_stack, cv(16)) # p2 = [ 16    19  1 ]
    push!(lifo_stack, cv(2))  # p2 = [ 16  2 19  1 ]
    
    # 36 stone on the board

    # p1 = [ 10 14 12  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(12), cv(14)], value=26)))
    # p1 = [ 10        7 ]
    push!(lifo_stack, cv(5)) # p1 = [ 10  5     7 ]
    push!(lifo_stack, cv(5)) # p1 = [ 10  5  5  7 ]

    # p2 = [ 16  2 19  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(19)], value=19)))
    # p2 = [ 16  2     1 ]
    push!(lifo_stack, cv(2)) # p2 = [ 16  2  2  1 ]
    
    # 38 stone on the board
    
    # p1 = [ 10  5  5  7 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(10)], value=17)))
    # p1 = [     5  5    ]
    push!(lifo_stack, cv(1)) # p1 = [  1  5  5    ]
    push!(lifo_stack, cv(1)) # p1 = [  1  5  5  1 ]

    # p2 = [ 16  2  2  1 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(2), cv(16)], value=19)))
    # p2 = [        2    ]
    push!(lifo_stack, cv(2)) # p2 = [  2     2    ]
    push!(lifo_stack, cv(2)) # p2 = [  2  2  2    ]
    push!(lifo_stack, cv(2)) # p2 = [  2  2  2  2 ]

    # 40 stone on the board

    # end
    # p1 = [  1  5  5  1 ]
    # p2 = [  2  2  2  2 ]

    # prepare stack data structure
    stack = Stack{CardValue}()
    for c in lifo_stack
        push!(stack, c)
    end

    spec = Just4FunSpec(stack)
    player1 = AlphaZero.ScriptedPlayer(player1_actions, nothing)
    player2 = AlphaZero.ScriptedPlayer(player2_actions, nothing)

    try
        Scripts.play_game(spec, TwoPlayers(player1, player2))
    catch error
        rethrow(error)
    finally
        GI.render(player1.game, debug=true)
    end

    # p1 = [  1  5  5  1 ]
    # p2 = [  2  2  2  2 ]
    # check sate
    @test player1.game.player_cards == @SMatrix [
        cv(1) cv(2) ;
        cv(5) cv(2) ;
        cv(5) cv(2) ;
        cv(1) cv(2)
    ]
    
    #@test game.field_stones TODO:
    @test player1.game.player_stones == SVector{2,Stones}(0, 0)
    @test length(player1.game.stack) == 0
    @test player1.game.curplayer == Just4Fun.Player(Just4Fun.RED)
    @test player1.game.winner == Just4Fun.Player(Just4Fun.RED) # highest 35 (vs 32 for yellow)
    @test player1.game.state == end_by_max_field
    
    # check api
    @test GI.game_terminated(player1.game) == true
    @test GI.game_terminated(player2.game) == true
    @test GI.white_reward(player1.game) == -1.0
    @test GI.white_reward(player2.game) == -1.0
    @test length(player1_actions) == 0
    @test length(player2_actions) == 0
end