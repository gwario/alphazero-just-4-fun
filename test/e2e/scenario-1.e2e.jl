using Test

using Just4Fun
using DataStructures

@testset "Simple scenario between two scripted players (1)" begin
    @info "Running scenario $(@__FILE__)..."

    #GI.render(GI.init(Just4FunSpec()))

    cv(v::Int)::CardValue = CardValue(v)

    lifo_stack = Stack{CardValue}()
    # supply a order of actions for each player to the game
    player1_actions = Queue{CardsAction}()
    player2_actions = Queue{CardsAction}()

    #  1 14 30 24 19  8
    # 33 11  9 16 35 21
    #  6 27 31 20  3 12
    # 15 32  5 29 17 26
    # 22 10 18 36 25  2
    # 28  7 23  4 13 34

    # -- Initial deal below --------
    push!(lifo_stack, cv(3)) # p1
    push!(lifo_stack, cv(5)) # p2
    push!(lifo_stack, cv(8)) # p1
    push!(lifo_stack, cv(9)) # p2
    push!(lifo_stack, cv(16))# p1
    push!(lifo_stack, cv(9)) # p2
    push!(lifo_stack, cv(4)) # p1
    push!(lifo_stack, cv(2)) # p2
    
    # p1 = [  3  8 16  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(8)], value=11)))
    # p1 = [       16  4 ]
    push!(lifo_stack, cv(7)) # p1 = [  7    16  4 ]
    push!(lifo_stack, cv(12))# p1 = [  7 12 16  4 ]
    
    # p2 = [  5  9  9  2 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(9), cv(9)], value=23)))
    # p2 = [           2 ]
    push!(lifo_stack, cv(3)) # p2 = [  3        2 ]
    push!(lifo_stack, cv(11))# p2 = [  3 11     2 ]
    push!(lifo_stack, cv(17))# p2 = [  3 11 17  2 ]
    
    # p1 = [  7 12 16  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(16)], value=16)))
    # p1 = [  7 12     4 ]
    push!(lifo_stack, cv(12))# p1 = [  7 12 12  4 ]

    # p2 = [  3 11 17  2 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(3), cv(11)], value=16)))
    # p2 = [       17    ]
    push!(lifo_stack, cv(2))# p2 = [  2    17    ]
    push!(lifo_stack, cv(3))# p2 = [  2  3 17    ]
    push!(lifo_stack, cv(6))# p2 = [  2  3 17  6 ]

    # p1 = [  7 12 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(4), cv(12)], value=16)))
    # p1 = [  7    12    ]
    push!(lifo_stack, cv(7)) # p1 = [  7  7 12    ]
    push!(lifo_stack, cv(14))# p1 = [  7  7 12 14 ]
    
    # p2 = [  2  3 17  6 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(17)], value=17)))
    # p2 = [  2  3     6 ]
    push!(lifo_stack, cv(4))# p2 = [  2  3  4  6 ]

    # p1 = [  7  7 12 14 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(14)], value=21)))
    # p1 = [     7 12    ]
    push!(lifo_stack, cv(6)) # p1 = [  6  7 12    ]
    push!(lifo_stack, cv(9)) # p1 = [  6  7 12  9 ]
    
    # p2 = [  2  3  4  6 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(3), cv(6)], value=11)))
    # p2 = [        4    ]
    push!(lifo_stack, cv(2)) # p2 = [  2     4     ]
    push!(lifo_stack, cv(7)) # p2 = [  2  7  4     ]
    push!(lifo_stack, cv(10))# p2 = [  2  7  4  10 ]

    # p1 = [  6  7 12  9 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(6)], value=6)))
    # p1 = [     7 12  9 ]
    push!(lifo_stack, cv(4)) # p1 = [  4  7 12  9 ]

    # p2 = [  2  7  4  10 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(4)], value=6)))
    # p2 = [     7     10 ]
    push!(lifo_stack, cv(5)) # p2 = [  5  7    10 ]
    push!(lifo_stack, cv(8)) # p2 = [  5  7  8 10 ]

    # p1 = [  4  7 12  9 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(9)], value=16)))
    # p1 = [  4    12    ]
    push!(lifo_stack, cv(2)) # p1 = [  4  2 12    ]
    push!(lifo_stack, cv(4)) # p1 = [  4  2 12  4 ]

    # p2 = [  5  7  8 10 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(7), cv(10)], value=17)))
    # p2 = [  5     8    ]
    push!(lifo_stack, cv(8)) # p2 = [  5  8  8    ]
    push!(lifo_stack, cv(11))# p2 = [  5  8  8 11 ]

    # p1 = [  4  2 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(2), cv(4)], value=6)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(6)) # p1 = [  6    12  4 ]
    push!(lifo_stack, cv(5)) # p1 = [  6  5 12  4 ]
    
    # p2 = [  5  8  8 11 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(8), cv(8)], value=21)))
    # p2 = [          11 ]
    push!(lifo_stack, cv(12))# p2 = [ 12       11 ]
    push!(lifo_stack, cv(1)) # p2 = [ 12  1    11 ]
    push!(lifo_stack, cv(6)) # p2 = [ 12  1  6 11 ]

    # p1 = [  6  5 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(5), cv(6)], value=11)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(19))# p1 = [ 19    12  4 ]
    push!(lifo_stack, cv(7)) # p1 = [ 19  7 12  4 ]

    # p2 = [ 12  1  6 11 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(11)], value=11)))
    # p2 = [ 12  1  6    ]
    push!(lifo_stack, cv(12))# p2 = [ 12  1  6 12 ]

    # p1 = [ 19  7 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(19)], value=26)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(1)) # p1 = [  1    12  4 ]
    push!(lifo_stack, cv(10))# p1 = [  1 10 12  4 ]

    # p2 = [ 12  1  6 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(6), cv(12)], value=19)))
    # p2 = [          12 ]
    push!(lifo_stack, cv(1)) # p2 = [  1       12 ]
    push!(lifo_stack, cv(1)) # p2 = [  1  1    12 ]
    push!(lifo_stack, cv(4)) # p2 = [  1  1  4 12 ]

    # p1 = [  1 10 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(10)], value=11)))
    # p1 = [       12  4 ]
    push!(lifo_stack, cv(11))# p1 = [ 11    12  4 ]
    push!(lifo_stack, cv(1)) # p1 = [ 11  1 12  4 ]

    # p2 = [  1  1  4 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(12)], value=12)))
    # p2 = [  1  1  4    ]
    push!(lifo_stack, cv(12)) # p2 = [  1  1  4 12 ]

    # p1 = [ 11  1 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(11)], value=11)))
    # p1 = [     1 12  4 ]
    push!(lifo_stack, cv(8))# p1 = [  8  1 12  4 ]

    # p2 = [  1  1  4 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1)], value=1)))
    # p2 = [     1  4 12 ]
    push!(lifo_stack, cv(19))# p2 = [ 19  1  4 12 ]

    # p1 = [  8  1 12  4 ]
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(8), cv(12)], value=21)))
    # p1 = [           4 ]
    push!(lifo_stack, cv(15))# p1 = [ 15        4 ]
    push!(lifo_stack, cv(2)) # p1 = [ 15  2     4 ]
    push!(lifo_stack, cv(14))# p1 = [ 15  2 14  4 ]

    # p2 = [ 19  1  4 12 ]
    enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(4), cv(12), cv(19)], value=36)))
    # p2 = [             ]
    push!(lifo_stack, cv(3))# p2 = [  3          ]
    push!(lifo_stack, cv(5))# p2 = [  3  5       ]
    push!(lifo_stack, cv(1))# p2 = [  3  5  1    ]
    push!(lifo_stack, cv(4))# p2 = [  3  5  1  4 ]
    

    # player 2 won, player 1 does not do any further move
    enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(8), cv(12)], value=21)))
    
    # p1 = [ 15  2 14  4 ]
    # p2 = [  3  5  1  4 ]

    # prepare stack data structure
    stack = Stack{CardValue}()
    for c in lifo_stack
      push!(stack, c)
    end

    spec = Just4FunSpec(stack)
    player1 = AlphaZero.ScriptedPlayer(player1_actions, nothing)
    player2 = AlphaZero.ScriptedPlayer(player2_actions, nothing)

    try
      Scripts.play_game(spec, TwoPlayers(player1, player2))
    catch error
      rethrow(error)
    finally
      GI.render(player1.game)
    end

    # p1 = [ 15  2 14  4 ]
    # p2 = [  3  5  1  4 ]
    # check sate
    @test player1.game.player_cards == @SMatrix [
      cv(15) cv(3) ;
      cv(2)  cv(5) ;
      cv(14) cv(1) ;
      cv(4)  cv(4)
    ]
    
    #@test game.field_stones TODO:
    @test player1.game.player_stones == SVector{2,Stones}(0x8, 0x8) # 23, p1 20-12, p2 20-12
    @test length(player1.game.stack) == 0
    @test player1.game.curplayer == Just4Fun.Player(Just4Fun.YELLOW)
    @test player1.game.winner == Just4Fun.Player(Just4Fun.RED) # 23 36 17 12 pattern
    @test player1.game.state == end_by_pattern
  
    # check api
    @test GI.game_terminated(player1.game) == true
    @test GI.game_terminated(player2.game) == true
    @test GI.white_reward(player1.game) == -1.0 # first player / yellow (just 4 fun) / white (framework name)
    @test GI.white_reward(player2.game) == -1.0
    @test length(player1_actions) == 1
    @test length(player2_actions) == 0
end