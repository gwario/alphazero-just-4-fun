using Test

using Just4Fun
using DataStructures

@testset "Redraw with scripted players" begin
    
    @info "Running scenario $(@__FILE__)..."

    @testset "Redraw - no end" begin
        #GI.render(GI.init(Just4FunSpec()))

        cv(v::Int)::CardValue = CardValue(v)

        lifo_stack = Stack{CardValue}()
        # supply a order of actions for each player to the game
        player1_actions = Queue{CardsAction}()
        player2_actions = Queue{CardsAction}()

        # deal 19 to p1 until all reachable fields are dominated, go on in order to force a redraw for p1
        # -- Initial deal below --------
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(1))  # p2
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(2))  # p2
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(3))  # p2
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(4))  # p2
        
        # p1 = [ 19 19 19 19 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(19)], value=19)))
        # p1 = [    19 19 19 ]
        push!(lifo_stack, cv(19)) # p1 = [ 19 19 19 19 ]
        
        # p2 = [  1  2  3  4 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1)],  value=1)))
        # p2 = [     2  3  4 ]
        push!(lifo_stack, cv(5)) # p2 = [  5  2  3  4 ]
        
        # p1 = [ 19 19 19 19 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(19)], value=19)))
        # p1 = [    19 19 19 ]
        push!(lifo_stack, cv(19))# p1 = [ 19 19 19 19 ]

        # p2 = [  5  2  3  4 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(2)], value=2)))
        # p2 = [  5     3  4 ]
        push!(lifo_stack, cv(6))# p2 = [  5  6  3  4 ]

        # p1 = [ 19 19 19 19 ]
        enqueue!(player1_actions, CardsAction((cards=[], value=0))) # redraw
        # p1 = [             ] 
        push!(lifo_stack, cv(1)) # p1 = [  1          ]
        push!(lifo_stack, cv(2)) # p1 = [  1  2       ]
        push!(lifo_stack, cv(3)) # p1 = [  1  2  3    ]
        push!(lifo_stack, cv(4)) # p1 = [  1  2  3  4 ]
        
        # end
        # p1 = [  1  2  3  4 ]
        # p2 = [  5  6  3  4 ]

        # prepare stack data structure
        stack = Stack{CardValue}()
        for c in lifo_stack
            push!(stack, c)
        end

        spec = Just4FunSpec(stack)
        player1 = AlphaZero.ScriptedPlayer(player1_actions, nothing)
        player2 = AlphaZero.ScriptedPlayer(player2_actions, nothing)

        try
            Scripts.play_game(spec, TwoPlayers(player1, player2))
        catch e
            if !(isa(e, ArgumentError) && e.msg == "Deque must be non-empty")
                rethrow(e)
            end
        finally
            GI.render(player1.game, debug=true)
        end

        # p1 = [  1  2  3  4 ]
        # p2 = [  5  6  3  4 ]
        # check sate
        @test player1.game.player_cards == @SMatrix [
            cv(1) cv(5) ;
            cv(2) cv(6) ;
            cv(3) cv(3) ;
            cv(4) cv(4)
        ]
        
        #@test game.field_stones TODO:
        @test player1.game.player_stones == SVector{2,Stones}(0x12, 0x12) # 18
        @test length(player1.game.stack) == 0
        @test player1.game.curplayer == Just4Fun.Player(Just4Fun.RED)
        @test player1.game.winner == Just4Fun.Player(0)
        @test player1.game.state == in_progress
        
        # check api
        @test GI.game_terminated(player1.game) == false
        @test GI.game_terminated(player2.game) == false
        @test GI.white_reward(player1.game) == 0.0
        @test GI.white_reward(player2.game) == 0.0
        @test length(player1_actions) == 0
        @test length(player2_actions) == 0
    end

    @testset "Redraw - player 1 - end with out of stones" begin
        #GI.render(GI.init(Just4FunSpec()))

        cv(v::Int)::CardValue = CardValue(v)

        lifo_stack = Stack{CardValue}()
        # supply a order of actions for each player to the game
        player1_actions = Queue{CardsAction}()
        player2_actions = Queue{CardsAction}()

        # deal 19 to p1 until all reachable fields are dominated, go on in order to force a redraw for p1
        # -- Initial deal below --------
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(1))  # p2
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(2))  # p2
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(3))  # p2
        push!(lifo_stack, cv(19)) # p1
        push!(lifo_stack, cv(4))  # p2
        
        # 40 stones left
        # p1 = [ 19 19 19 19 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(19)], value=19)))
        # p1 = [    19 19 19 ]
        push!(lifo_stack, cv(19)) # p1 = [ 19 19 19 19 ]
        # 39 stones left
        
        # p2 = [  1  2  3  4 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1)],  value=1)))
        # p2 = [     2  3  4 ]
        push!(lifo_stack, cv(5)) # p2 = [  5  2  3  4 ]
        # 38 stones left

        # p1 = [ 19 19 19 19 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(19)], value=19)))
        # p1 = [    19 19 19 ]
        push!(lifo_stack, cv(19))# p1 = [ 19 19 19 19 ]
        # 37 stones left

        # p2 = [  5  2  3  4 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(2)], value=2)))
        # p2 = [  5     3  4 ]
        push!(lifo_stack, cv(6))# p2 = [  5  6  3  4 ]
        # 36 stones left

        # p1 = [ 19 19 19 19 ]
        enqueue!(player1_actions, CardsAction((cards=[], value=0))) # redraw
        # p1 = [             ] 
        push!(lifo_stack, cv(1)) # p1 = [  1          ]
        push!(lifo_stack, cv(2)) # p1 = [  1  2       ]
        push!(lifo_stack, cv(3)) # p1 = [  1  2  3    ]
        push!(lifo_stack, cv(4)) # p1 = [  1  2  3  4 ]
        # 36 stones left - due to redraw
        
        # p2 = [  5  6  3  4 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(4), cv(6)], value=10)))
        # p2 = [  5     3    ]
        push!(lifo_stack, cv(1)) # p2 = [  5  1  3    ]
        push!(lifo_stack, cv(8)) # p2 = [  5  1  3  8 ]
        # 35 stones left

        # p1 = [  1  2  3  4 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(2), cv(3), cv(4)], value=9)))
        # p1 = [  1          ] 
        push!(lifo_stack, cv(9)) # p1 = [  1  9       ]
        push!(lifo_stack, cv(3)) # p1 = [  1  9  3    ]
        push!(lifo_stack, cv(7)) # p1 = [  1  9  3  7 ]
        # 34 stones left

        # p2 = [  5  1  3  8 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(8)], value=13)))
        # p2 = [     1  3    ]
        push!(lifo_stack, cv(4)) # p2 = [  4  1  3    ]
        push!(lifo_stack, cv(7)) # p2 = [  4  1  3  7 ]
        # 33 stones left

        # p1 = [  1  9  3  7 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(7), cv(9)], value=17)))
        # p1 = [        3    ]
        push!(lifo_stack, cv(13)) # p1 = [ 13     3    ]
        push!(lifo_stack, cv(1))  # p1 = [ 13  1  3    ]
        push!(lifo_stack, cv(2))  # p1 = [ 13  1  3  2 ]
        # 32 stones left

        # p2 = [  4  1  3  7 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(3), cv(4), cv(7)], value=15)))
        # p2 = [             ]
        push!(lifo_stack, cv(14))# p2 = [ 14          ]
        push!(lifo_stack, cv(12))# p2 = [ 14 12       ]
        push!(lifo_stack, cv(1)) # p2 = [ 14 12  1    ]
        push!(lifo_stack, cv(2)) # p2 = [ 14 12  1  2 ]
        # 31 stones left

        # p1 = [ 13  1  3  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(13)], value=14)))
        # p1 = [        3  2 ]
        push!(lifo_stack, cv(11)) # p1 = [ 11     3  2 ]
        push!(lifo_stack, cv(7))  # p1 = [ 11  7  3  2 ]
        # 30 stones left

        # p2 = [ 14 12  1  2 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(14)], value=15)))
        # p2 = [    12     2 ]
        push!(lifo_stack, cv(1)) # p2 = [  1 12     2 ]
        push!(lifo_stack, cv(10))# p2 = [  1 12 10  2 ]
        # 29 stones left

        # p1 = [ 11  7  3  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(2), cv(7)], value=9)))
        # p1 = [ 11     3    ]
        push!(lifo_stack, cv(6)) # p1 = [ 11  6  3    ]
        push!(lifo_stack, cv(18))# p1 = [ 11  6  3 18 ]
        # 28 stones left

        # p2 = [  1 12 10  2 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1)], value=1)))
        # p2 = [    12 10  2 ]
        push!(lifo_stack, cv(13)) # p2 = [ 13 12 10  2 ]
        # 27 stones left

        # p1 = [ 11  6  3 18 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(6), cv(11)], value=17)))
        # p1 = [        3 18 ]
        push!(lifo_stack, cv(17)) # p1 = [ 17     3 18 ]
        push!(lifo_stack, cv(7))  # p1 = [ 17  7  3 18 ]
        # 26 stones left
        
        # p2 = [ 13 12 10  2 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(10)], value=10)))
        # p2 = [ 13 12     2 ]
        push!(lifo_stack, cv(15)) # p2 = [ 13 12 15  2 ]
        # 25 stones left

        # p1 = [ 17  7  3 18 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(18)], value=21)))
        # p1 = [ 17  7       ]
        push!(lifo_stack, cv(9)) # p1 = [ 17  7  9    ]
        push!(lifo_stack, cv(13))# p1 = [ 17  7  9 13 ]
        # 24 stones left

        # p2 = [ 13 12 15  2 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(13)], value=13)))
        # p2 = [    12 15  2 ]
        push!(lifo_stack, cv(6)) # p2 = [  6 12 15  2 ]
        # 23 stones left

        # p1 = [ 17  7  9 13 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(9), cv(17)], value=26)))
        # p1 = [     7    13 ]
        push!(lifo_stack, cv(8)) # p1 = [  8  7    13 ]
        push!(lifo_stack, cv(7)) # p1 = [  8  7  7 13 ]
        # 22 stones left

        # p2 = [  6 12 15  2 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(2)], value=2)))
        # p2 = [  6 12 15    ]
        push!(lifo_stack, cv(16)) # p2 = [  6 12 15 16 ]
        # 21 stones left

        # p1 = [  8  7  7 13 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(8), cv(13)], value=28)))
        # p1 = [        7    ]
        push!(lifo_stack, cv(10)) # p1 = [ 10     7    ]
        push!(lifo_stack, cv(18)) # p1 = [ 10 18  7    ]
        push!(lifo_stack, cv(5))  # p1 = [ 10 18  7  5 ]
        # 20 stones left

        # p2 = [  6 12 15 16 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(6), cv(15)], value=21)))
        # p2 = [    12   16 ]
        push!(lifo_stack, cv(10)) # p2 = [ 10 12    16 ]
        push!(lifo_stack, cv(13)) # p2 = [ 10 12 13 16 ]
        # 19 stones left

        # p1 = [ 10 18  7  5 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(7)], value=7)))
        # p1 = [ 10 18     5 ]
        push!(lifo_stack, cv(19)) # p1 = [ 10 18 19  5 ]
        # 18 stones left

        # p2 = [ 10 12 13 16 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(10), cv(16)], value=26)))
        # p2 = [    12 13    ]
        push!(lifo_stack, cv(3)) # p2 = [  3 12 13    ]
        push!(lifo_stack, cv(7)) # p2 = [  3 12 13  7 ]
        # 17 stones left

        # p1 = [ 10 18 19  5 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(5), cv(10), cv(18)], value=33)))
        # p1 = [       19    ]
        push!(lifo_stack, cv(10)) # p1 = [ 10    19    ]
        push!(lifo_stack, cv(5))  # p1 = [ 10  5 19    ]
        push!(lifo_stack, cv(12)) # p1 = [ 10  5 19 12 ]
        # 16 stones left

        # p2 = [  3 12 13  7 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(3), cv(12), cv(13)], value=28)))
        # p2 = [           7 ]
        push!(lifo_stack, cv(18)) # p2 = [ 18        7 ]
        push!(lifo_stack, cv(15)) # p2 = [ 18 15     7 ]
        push!(lifo_stack, cv(19)) # p2 = [ 18 15 19  7 ]
        # 15 stones left

        # p1 = [ 10  5 19 12 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(5), cv(10), cv(19)], value=34)))
        # p1 = [          12 ]
        push!(lifo_stack, cv(7))  # p1 = [  7       12 ]
        push!(lifo_stack, cv(7))  # p1 = [  7  7    12 ]
        push!(lifo_stack, cv(16)) # p1 = [  7  7 16 12 ]
        # 14 stones left

        # p2 = [ 18 15 19  7 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(7)], value=7)))
        # p2 = [ 18 15 19    ]
        push!(lifo_stack, cv(14)) # p2 = [ 18 15 19 14 ]
        # 13 stones left

        # p1 = [  7  7 16 12 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(12)], value=12)))
        # p1 = [  7  7 16    ]
        push!(lifo_stack, cv(14))  # p1 = [  7  7 16 14 ]
        # 12 stones left

        # p2 = [ 18 15 19 14 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(15), cv(18)], value=33)))
        # p2 = [       19 14 ]
        push!(lifo_stack, cv(1))  # p2 = [  1    19 14 ]
        push!(lifo_stack, cv(12)) # p2 = [  1 12 19 14 ]
        # 11 stones left

        # p1 = [  7  7 16 14 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(7), cv(7)], value=14)))
        # p1 = [       16 14 ]
        push!(lifo_stack, cv(18))  # p1 = [ 18    16 14 ]
        push!(lifo_stack, cv(10))  # p1 = [ 18 10 16 14 ]
        # 10 stones left

        # p2 = [  1 12 19 14 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(14), cv(19)], value=34)))
        # p2 = [    12       ]
        push!(lifo_stack, cv(7)) # p2 = [  7 12       ]
        push!(lifo_stack, cv(5)) # p2 = [  7 12  5    ]
        push!(lifo_stack, cv(19))# p2 = [  7 12  5 19 ]
        # 9 stones left

        # p1 = [ 18 10 16 14 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(14), cv(16)], value=30)))
        # p1 = [ 18 10       ]
        push!(lifo_stack, cv(7))  # p1 = [ 18 10  7    ]
        push!(lifo_stack, cv(17)) # p1 = [ 18 10  7 17 ]
        # 8 stones left

        # p2 = [  7 12  5 19 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(12)], value=12)))
        # p2 = [  7     5 19 ]
        push!(lifo_stack, cv(11))  # p2 = [  7 11  5 19 ]
        # 7 stones left
        
        # p1 = [ 18 10  7 17 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(10), cv(18)], value=28)))
        # p1 = [        7 17 ]
        push!(lifo_stack, cv(13))  # p1 = [ 13     7 17 ]
        push!(lifo_stack, cv(2))   # p1 = [ 13  2  7 17 ]
        # 6 stones left

        # p2 = [  7 11  5 19 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(7)], value=12)))
        # p2 = [    11    19 ]
        push!(lifo_stack, cv(16))  # p2 = [ 16 11    19 ]
        push!(lifo_stack, cv(17))  # p2 = [ 16 11 17 19 ]
        # 5 stones left

        # p1 = [ 13  2  7 17 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(7)], value=7)))
        # p1 = [ 13  2    17 ]
        push!(lifo_stack, cv(18))  # p1 = [ 13  2 18 17 ]
        # 4 stones left

        # p2 = [ 16 11 17 19 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(11), cv(19)], value=30)))
        # p2 = [ 16    17    ]
        push!(lifo_stack, cv(15))  # p2 = [ 16 15 17    ]
        push!(lifo_stack, cv(18))  # p2 = [ 16 15 17 18 ]
        # 3 stones left

        # p1 = [ 13  2 18 17 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(2), cv(13), cv(17)], value=32)))
        # p1 = [       18    ]
        push!(lifo_stack, cv(2))  # p1 = [  2    18    ]
        push!(lifo_stack, cv(2))  # p1 = [  2  2 18    ]
        push!(lifo_stack, cv(2))  # p1 = [  2  2 18  2 ]
        # 2 stones left

        # p2 = [ 16 15 17 18 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(15), cv(17)], value=32)))
        # p2 = [ 16       18 ]
        push!(lifo_stack, cv(1))  # p2 = [ 16  1    18 ]
        push!(lifo_stack, cv(1))  # p2 = [ 16  1  1 18 ]
        # 1 stones left

        # player2 additional turn
        # p1 = [  2  2 18  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(18)], value=18)))
        # p1 = [  2  2     2 ]
        push!(lifo_stack, cv(2))  # p1 = [  2  2  2  2 ]
        # 0 stones left

        # end
        # p1 = [  2  2  2  2 ]
        # p2 = [ 16  1  1 18 ]

        # prepare stack data structure
        stack = Stack{CardValue}()
        for c in lifo_stack
            push!(stack, c)
        end

        #  1 14 30 24 19  8
        # 33 11  9 16 35 21
        #  6 27 31 20  3 12
        # 15 32  5 29 17 26
        # 22 10 18 36 25  2
        # 28  7 23  4 13 34

        spec = Just4FunSpec(stack) # 5 stones only => 10 truns
        player1 = AlphaZero.ScriptedPlayer(player1_actions, nothing)
        player2 = AlphaZero.ScriptedPlayer(player2_actions, nothing)

        try
            Scripts.play_game(spec, TwoPlayers(player1, player2))
        catch e
            if !(isa(e, ArgumentError) && e.msg == "Deque must be non-empty")
                rethrow(e)
            end
        finally
            GI.render(player1.game, debug=true)
        end

        # p1 = [  2  2  2  2 ]
        # p2 = [ 16  1  1 18 ]
        # check sate
        @test player1.game.player_cards == @SMatrix [
            cv(2) cv(16) ;
            cv(2) cv(1)  ;
            cv(2) cv(1)  ;
            cv(2) cv(18)
        ]
        
        #@test game.field_stones TODO:
        @test player1.game.player_stones == SVector{2,Stones}(0, 0)
        @test length(player1.game.stack) == 0
        @test player1.game.curplayer == Just4Fun.Player(Just4Fun.YELLOW) # no next player set when the last stone was placed
        @test player1.game.winner == Just4Fun.Player(Just4Fun.YELLOW)
        @test player1.game.state == end_by_points
        
        # check api
        @test GI.game_terminated(player1.game) == true
        @test GI.game_terminated(player2.game) == true
        @test GI.white_reward(player1.game) == 1.0
        @test GI.white_reward(player2.game) == 1.0
        @test length(player1_actions) == 0
        @test length(player2_actions) == 0
    end

    @testset "Redraw - player 2 - end with out of stones" begin
        #GI.render(GI.init(Just4FunSpec()))

        cv(v::Int)::CardValue = CardValue(v)

        lifo_stack = Stack{CardValue}()
        # supply a order of actions for each player to the game
        player1_actions = Queue{CardsAction}()
        player2_actions = Queue{CardsAction}()

        # deal 19 to p2 until all reachable fields are dominated, go on in order to force a redraw for p2
        # -- Initial deal below --------
        push!(lifo_stack, cv(1))  # p1
        push!(lifo_stack, cv(19)) # p2
        push!(lifo_stack, cv(2))  # p1
        push!(lifo_stack, cv(19)) # p2
        push!(lifo_stack, cv(3))  # p1
        push!(lifo_stack, cv(19)) # p2
        push!(lifo_stack, cv(4))  # p1
        push!(lifo_stack, cv(19)) # p2
        
        # 40 stones left
        # p1 = [  1  2  3  4 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(1)],  value=1)))
        # p1 = [     2  3  4 ]
        push!(lifo_stack, cv(5)) # p1 = [  5  2  3  4 ]
        # 39 stones left
        
        # p2 = [ 19 19 19 19 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(19)], value=19)))
        # p2 = [    19 19 19 ]
        push!(lifo_stack, cv(19)) # p2 = [ 19 19 19 19 ]
        # 38 stones left
        
        # p1 = [  5  2  3  4 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(2)], value=2)))
        # p1 = [  5     3  4 ]
        push!(lifo_stack, cv(6))# p1 = [  5  6  3  4 ]
        # 37 stones left

        # p2 = [ 19 19 19 19 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(19)], value=19)))
        # p2 = [    19 19 19 ]
        push!(lifo_stack, cv(19))# p2 = [ 19 19 19 19 ]
        # 36 stones left

        # p1 = [  5  6  3  4 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(4), cv(6)], value=10)))
        # p1 = [  5     3    ]
        push!(lifo_stack, cv(1)) # p1 = [  5  1  3    ]
        push!(lifo_stack, cv(8)) # p1 = [  5  1  3  8 ]
        # 35 stones left

        # p2 = [ 19 19 19 19 ]
        enqueue!(player2_actions, CardsAction((cards=[], value=0))) # redraw
        # p2 = [             ] 
        push!(lifo_stack, cv(1)) # p2 = [  1          ]
        push!(lifo_stack, cv(2)) # p2 = [  1  2       ]
        push!(lifo_stack, cv(3)) # p2 = [  1  2  3    ]
        push!(lifo_stack, cv(4)) # p2 = [  1  2  3  4 ]
        # 35 stones left
        
        # p1 = [  5  1  3  8 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(5), cv(8)], value=13)))
        # p1 = [     1  3    ]
        push!(lifo_stack, cv(4)) # p1 = [  4  1  3    ]
        push!(lifo_stack, cv(7)) # p1 = [  4  1  3  7 ]
        # 34 stones left
        
        # p2 = [  1  2  3  4 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(3), cv(4)], value=9)))
        # p2 = [  1          ] 
        push!(lifo_stack, cv(9)) # p2 = [  1  9       ]
        push!(lifo_stack, cv(3)) # p2 = [  1  9  3    ]
        push!(lifo_stack, cv(7)) # p2 = [  1  9  3  7 ]
        # 33 stones left

        # p1 = [  4  1  3  7 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(3), cv(4), cv(7)], value=15)))
        # p1 = [             ]
        push!(lifo_stack, cv(14))# p1 = [ 14          ]
        push!(lifo_stack, cv(12))# p1 = [ 14 12       ]
        push!(lifo_stack, cv(1)) # p1 = [ 14 12  1    ]
        push!(lifo_stack, cv(2)) # p1 = [ 14 12  1  2 ]
        # 32 stones left

        # p2 = [  1  9  3  7 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(7), cv(9)], value=17)))
        # p2 = [        3    ]
        push!(lifo_stack, cv(13)) # p2 = [ 13     3    ]
        push!(lifo_stack, cv(1))  # p2 = [ 13  1  3    ]
        push!(lifo_stack, cv(2))  # p2 = [ 13  1  3  2 ]
        # 31 stones left

        # p1 = [ 14 12  1  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(14)], value=15)))
        # p1 = [    12     2 ]
        push!(lifo_stack, cv(1)) # p1 = [  1 12     2 ]
        push!(lifo_stack, cv(10))# p1 = [  1 12 10  2 ]
        # 30 stones left

        # p2 = [ 13  1  3  2 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(1), cv(13)], value=14)))
        # p2 = [        3  2 ]
        push!(lifo_stack, cv(11)) # p2 = [ 11     3  2 ]
        push!(lifo_stack, cv(7))  # p2 = [ 11  7  3  2 ]
        # 29 stones left

        # p1 = [  1 12 10  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(1)], value=1)))
        # p1 = [    12 10  2 ]
        push!(lifo_stack, cv(13)) # p1 = [ 13 12 10  2 ]
        # 28 stones left

        # p2 = [ 11  7  3  2 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(7)], value=9)))
        # p2 = [ 11     3    ]
        push!(lifo_stack, cv(6)) # p2 = [ 11  6  3    ]
        push!(lifo_stack, cv(18))# p2 = [ 11  6  3 18 ]
        # 27 stones left

        # p1 = [ 13 12 10  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(10)], value=10)))
        # p1 = [ 13 12     2 ]
        push!(lifo_stack, cv(15)) # p1 = [ 13 12 15  2 ]
        # 26 stones left

        # p2 = [ 11  6  3 18 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(6), cv(11)], value=17)))
        # p2 = [        3 18 ]
        push!(lifo_stack, cv(17)) # p2 = [ 17     3 18 ]
        push!(lifo_stack, cv(7))  # p2 = [ 17  7  3 18 ]
        # 25 stones left
        
        # p1 = [ 13 12 15  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(13)], value=13)))
        # p1 = [    12 15  2 ]
        push!(lifo_stack, cv(6)) # p1 = [  6 12 15  2 ]
        # 24 stones left

        # p2 = [ 17  7  3 18 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(3), cv(18)], value=21)))
        # p2 = [ 17  7       ]
        push!(lifo_stack, cv(9)) # p2 = [ 17  7  9    ]
        push!(lifo_stack, cv(13))# p2 = [ 17  7  9 13 ]
        # 23 stones left

        # p1 = [  6 12 15  2 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(2)], value=2)))
        # p1 = [  6 12 15    ]
        push!(lifo_stack, cv(16)) # p1 = [  6 12 15 16 ]
        # 22 stones left

        # p2 = [ 17  7  9 13 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(9), cv(17)], value=26)))
        # p2 = [     7    13 ]
        push!(lifo_stack, cv(8)) # p2 = [  8  7    13 ]
        push!(lifo_stack, cv(7)) # p2 = [  8  7  7 13 ]
        # 21 stones left

        # p1 = [  6 12 15 16 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(6), cv(15)], value=21)))
        # p1 = [    12   16 ]
        push!(lifo_stack, cv(10)) # p1 = [ 10 12    16 ]
        push!(lifo_stack, cv(13)) # p1 = [ 10 12 13 16 ]
        # 20 stones left

        # p2 = [  8  7  7 13 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(7), cv(8), cv(13)], value=28)))
        # p2 = [        7    ]
        push!(lifo_stack, cv(10)) # p2 = [ 10     7    ]
        push!(lifo_stack, cv(18)) # p2 = [ 10 18  7    ]
        push!(lifo_stack, cv(5))  # p2 = [ 10 18  7  5 ]
        # 19 stones left
        
        # p1 = [ 10 12 13 16 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(10), cv(16)], value=26)))
        # p1 = [    12 13    ]
        push!(lifo_stack, cv(3)) # p1 = [  3 12 13    ]
        push!(lifo_stack, cv(7)) # p1 = [  3 12 13  7 ]
        # 18 stones left

        # p2 = [ 10 18  7  5 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(7)], value=7)))
        # p2 = [ 10 18     5 ]
        push!(lifo_stack, cv(19)) # p2 = [ 10 18 19  5 ]
        # 17 stones left

        # p1 = [  3 12 13  7 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(3), cv(12), cv(13)], value=28)))
        # p1 = [           7 ]
        push!(lifo_stack, cv(18)) # p1 = [ 18        7 ]
        push!(lifo_stack, cv(15)) # p1 = [ 18 15     7 ]
        push!(lifo_stack, cv(19)) # p1 = [ 18 15 19  7 ]
        # 16 stones left

        # p2 = [ 10 18 19  5 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(10), cv(18)], value=33)))
        # p2 = [       19    ]
        push!(lifo_stack, cv(10)) # p2 = [ 10    19    ]
        push!(lifo_stack, cv(5))  # p2 = [ 10  5 19    ]
        push!(lifo_stack, cv(12)) # p2 = [ 10  5 19 12 ]
        # 15 stones left

        # p1 = [ 18 15 19  7 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(7)], value=7)))
        # p1 = [ 18 15 19    ]
        push!(lifo_stack, cv(14)) # p1 = [ 18 15 19 14 ]
        # 14 stones left

        # p2 = [ 10  5 19 12 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(5), cv(10), cv(19)], value=34)))
        # p2 = [          12 ]
        push!(lifo_stack, cv(7))  # p2 = [  7       12 ]
        push!(lifo_stack, cv(7))  # p2 = [  7  7    12 ]
        push!(lifo_stack, cv(16)) # p2 = [  7  7 16 12 ]
        # 13 stones left

        # p1 = [ 18 15 19 14 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(15), cv(18)], value=33)))
        # p1 = [       19 14 ]
        push!(lifo_stack, cv(1))  # p1 = [  1    19 14 ]
        push!(lifo_stack, cv(12)) # p1 = [  1 12 19 14 ]
        # 12 stones left

        # p2 = [  7  7 16 12 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(12)], value=12)))
        # p2 = [  7  7 16    ]
        push!(lifo_stack, cv(14))  # p2 = [  7  7 16 14 ]
        # 11 stones left

        # p1 = [  1 12 19 14 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(1), cv(14), cv(19)], value=34)))
        # p1 = [    12       ]
        push!(lifo_stack, cv(7)) # p1 = [  7 12       ]
        push!(lifo_stack, cv(5)) # p1 = [  7 12  5    ]
        push!(lifo_stack, cv(19))# p1 = [  7 12  5 19 ]
        # 10 stones left

        # p2 = [  7  7 16 14 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(7), cv(7)], value=14)))
        # p2 = [       16 14 ]
        push!(lifo_stack, cv(18))  # p2 = [ 18    16 14 ]
        push!(lifo_stack, cv(10))  # p2 = [ 18 10 16 14 ]
         # 9 stones left

        # p1 = [  7 12  5 19 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(12)], value=12)))
        # p1 = [  7     5 19 ]
        push!(lifo_stack, cv(11))  # p1 = [  7 11  5 19 ]
        # 8 stones left
        
        # p2 = [ 18 10 16 14 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(14), cv(16)], value=30)))
        # p2 = [ 18 10       ]
        push!(lifo_stack, cv(7))  # p2 = [ 18 10  7    ]
        push!(lifo_stack, cv(17)) # p2 = [ 18 10  7 17 ]
        # 7 stones left

        # p1 = [  7 11  5 19 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(5), cv(7)], value=12)))
        # p1 = [    11    19 ]
        push!(lifo_stack, cv(16))  # p1 = [ 16 11    19 ]
        push!(lifo_stack, cv(17))  # p1 = [ 16 11 17 19 ]
        # 6 stones left

        # p2 = [ 18 10  7 17 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(10), cv(18)], value=28)))
        # p2 = [        7 17 ]
        push!(lifo_stack, cv(13))  # p2 = [ 13     7 17 ]
        push!(lifo_stack, cv(2))   # p2 = [ 13  2  7 17 ]
        # 5 stones left
        
        # p1 = [ 16 11 17 19 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(11), cv(19)], value=30)))
        # p1 = [ 16    17    ]
        push!(lifo_stack, cv(15))  # p1 = [ 16 15 17    ]
        push!(lifo_stack, cv(18))  # p1 = [ 16 15 17 18 ]
        # 4 stones left

        # p2 = [ 13  2  7 17 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(7)], value=7)))
        # p2 = [ 13  2    17 ]
        push!(lifo_stack, cv(18))  # p2 = [ 13  2 18 17 ]
        # 3 stones left

        # p1 = [ 16 15 17 18 ]
        enqueue!(player1_actions, CardsAction((cards=[cv(15), cv(16)], value=31)))
        # p1 = [       17 18 ]
        push!(lifo_stack, cv(2))  # p1 = [  2    17 18 ]
        push!(lifo_stack, cv(2))  # p1 = [  2  2 17 18 ]
        # 2 stones left

        # p2 = [ 13  2 18 17 ]
        enqueue!(player2_actions, CardsAction((cards=[cv(18)], value=18)))
        # p2 = [ 13  2    17 ]
        push!(lifo_stack, cv(2))  # p2 = [ 13  2  2 17 ]
        # 1 stones left

        # p2 = [ 13  2  2 17 ] # p2's second turn in a row because of the redraw
        enqueue!(player2_actions, CardsAction((cards=[cv(2), cv(2)], value=4)))
        # p2 = [ 13       17 ]
        push!(lifo_stack, cv(2))  # p2 = [ 13  2    17 ]
        push!(lifo_stack, cv(2))  # p2 = [ 13  2  2 17 ]
        # 0 stones left

        # end
        # p1 = [  2  2 17 18 ]
        # p2 = [ 13  2  2 17 ]

        # prepare stack data structure
        stack = Stack{CardValue}()
        for c in lifo_stack
            push!(stack, c)
        end

        #  1 14 30 24 19  8
        # 33 11  9 16 35 21
        #  6 27 31 20  3 12
        # 15 32  5 29 17 26
        # 22 10 18 36 25  2
        # 28  7 23  4 13 34

        spec = Just4FunSpec(stack) # 5 stones only => 10 truns
        player1 = AlphaZero.ScriptedPlayer(player1_actions, nothing)
        player2 = AlphaZero.ScriptedPlayer(player2_actions, nothing)

        try
            Scripts.play_game(spec, TwoPlayers(player1, player2))
        catch e
            if !(isa(e, ArgumentError) && e.msg == "Deque must be non-empty")
                rethrow(e)
            end
        finally
            GI.render(player1.game, debug=true)
        end

        # p1 = [  2  2 17 18 ]
        # p2 = [ 13  2  2 17 ]
        # check sate
        @test player1.game.player_cards == @SMatrix [
            cv(2)  cv(13) ;
            cv(2)  cv(2)  ;
            cv(17) cv(2)  ;
            cv(18) cv(17)
        ]
        
        #@test game.field_stones TODO:
        @test player1.game.player_stones == SVector{2,Stones}(0, 0)
        @test length(player1.game.stack) == 0
        @test player1.game.curplayer == Just4Fun.Player(Just4Fun.RED) # no next player set when the last stone was placed
        @test player1.game.winner == Just4Fun.Player(Just4Fun.RED)
        @test player1.game.state == end_by_points
        
        # check api
        @test GI.game_terminated(player1.game) == true
        @test GI.game_terminated(player2.game) == true
        @test GI.white_reward(player1.game) == -1.0
        @test GI.white_reward(player2.game) == -1.0
        @test length(player1_actions) == 0
        @test length(player2_actions) == 0
    end
end