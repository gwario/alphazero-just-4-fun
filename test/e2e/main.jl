using Test
import Just4Fun
using StaticArrays

@testset verbose = true "Just4Fun Integration Tests" begin
    #@testset verbose = true "Scenario 1" begin
    #    include("scenario-1.e2e.jl")
    #end
    #@testset verbose = true "Scenario win by points" begin
    #    include("scenario-win_by_points.e2e.jl")
    #end
    #@testset verbose = true "Scenario win by highest position" begin
    #    include("scenario-win_by_highest_position.e2e.jl")
    #end
    #@testset verbose = true "Scenario redraw" begin
    #    include("scenario-redraw.e2e.jl")
    #end
    @testset verbose = true "Networks learn" begin
        
        include("../utils/ui/explorer.jl")
        include("../utils/utils.jl")

        @testset verbose = true "ResNet" begin    
            # loading the custom neural network architecture
            include("../../src/networks/flux/common.jl")
            # customisations for the network
            include("../../src/resnet/network/network.jl")
            include("../../src/resnet/network/learning.jl")

            include("resnet-learns-1.e2e.jl")
        end
    end
end




