gspec = Just4FunSpec()


#push!(Just4Fun.PER_GAME_SEEDS, 999)

network = FieldNet

#####
##### Training hyperparameters
#####

netparams = FieldNetHP(
  num_filters=45,
  num_blocks=4,
  conv_kernel_size=(3, 3),
  num_policy_head_filters=16,
  num_value_head_filters=1
)

self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=50,
    num_workers=128,
    batch_size=128,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    num_iters_per_turn=20,
    dirichlet_noise_ϵ=0.25,
    dirichlet_noise_α=1.0
  )
)

arena_params = ArenaParams(
  sim=SimParams(
    num_games=50,
    num_workers=128,
    batch_size=128,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.05
)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  use_position_averaging=false,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=128,
  loss_computation_batch_size=128,
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=2000,
  num_checkpoints=1
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=30,
  ternary_rewards=true,
  mem_buffer_size=PLSchedule(
    [    0,     5,    10,        30],
    [1_000, 3_000, 5_000, 1_000_000]
  )
)
