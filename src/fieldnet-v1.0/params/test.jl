gspec = Just4FunSpec()

network = FieldNet

#####
##### Training hyperparameters
#####

netparams = FieldNetHP(
  num_filters=1,
  num_blocks=1,
  conv_kernel_size=(3, 3),
  num_policy_head_filters=1,
  num_value_head_filters=1
)

self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=2,
    num_workers=64,
    batch_size=64,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    num_iters_per_turn=2,
    dirichlet_noise_ϵ=0.25,
    dirichlet_noise_α=1.0
  )
)

arena_params = ArenaParams(
  sim=SimParams(
    num_games=2,
    num_workers=64,
    batch_size=64,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.05
)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=1024,
  loss_computation_batch_size=1024,
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  nonvalidity_penalty=1.,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=2000,
  num_checkpoints=1
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=1,
  ternary_rewards=true,
  mem_buffer_size=PLSchedule(
    [      0,        15],
    [400_000, 1_000_000]
  )
)
