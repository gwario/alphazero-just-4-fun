module test
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/test.jl")
end
export load, params, network, netparams
end

###################################################################
module v0_1_0
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.1.0-custom-nn.jl")
end
export load, params, network, netparams
end
###################################################################

module v0_2_0
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.2.0-1gen.jl")
end
export load, params, network, netparams
end
###################################################################

module v0_2_1
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.2.1-1gen.jl")
end
export load, params, network, netparams
end
###################################################################

# based on Experiment_v0_2_1 with possibility to fix the random seed
module v0_2_1_single_seeds
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/field-input_v0.0.1/inputs.jl")
    include("./networks/field-input_v0.0.1/network.jl")
    include("./networks/field-input_v0.0.1/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.2.1-1gen-fast-single-seed.jl")
end
export load, params, network, netparams
end
###################################################################

# based on Experiment_v0_2_1 with possibility to fix the random seed
module v0_2_2_single_seed
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.2.2-1gen-fast-single-seed.jl")
end
export load, params, network, netparams
end
###################################################################

# based on Experiment_v0_2_1 with possibility to fix the random seed
module v0_2_2_multiple_seeds
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.2.2-1gen-fast-multiple-seeds.jl")
end
export load, params, network, netparams
end
###################################################################

"""
v0_2_3

Initial report
  
Number of network parameters: 330,401
Number of regularized network parameters: 328,512
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 330,401×3 = 1m samples i.e. at least 60k games
At 800 games per iteration about 75 iterations

800 games self-play with 30 rollouts approx. 1h => more than 80 hours training (+benchmark and checkpoint eval.)
"""
module v0_2_3
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.2.3.jl")
end
export load, params, network, netparams
end
###################################################################

"""
v0_3_0

Less parameters than 0.2.3 but more evaluation and more rollouts.

Initial report
  
Number of network parameters: 51,619
Number of regularized network parameters: 51,040
Memory footprint per MCTS node: 94806 bytes

51,619 * 3 = 150k -> after 15 iterations experience buffer large enough

1 iter = 2h

"""
module v0_3_0
function load()
    # loading the neural network utlis
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./networks/inputs/ma-do.jl")
    include("./fieldnet-v1.0/network/network.jl")
    include("./fieldnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./fieldnet-v1.0/params/v0.3.0.jl")
end
export load, params, network, netparams
end