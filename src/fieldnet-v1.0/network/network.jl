using Base: @kwdef
import JSON3
using Just4Fun
using Flux
using Flux: relu, rrelu, softmax, flatten, batch
using Flux: Chain, Dense, Conv, BatchNorm, SkipConnection
using AlphaZero: Network, CyclicNesterov, CyclicSchedule, Adam
using AlphaZero.NetLib: TwoHeadNetwork, lossgrads


"""
FieldNetHP (copy of ResNetHP)

Hyperparameters for the convolutional resnet architecture.

| Parameter                 | Type                | Default   |
|:--------------------------|:--------------------|:----------|
| `num_blocks`              | `Int`               |  -        |
| `num_filters`             | `Int`               |  -        |
| `conv_kernel_size`        | `Tuple{Int, Int}`   |  -        |
| `num_policy_head_filters` | `Int`               | `2`       |
| `num_value_head_filters`  | `Int`               | `1`       |
| `batch_norm_momentum`     | `Float32`           | `0.6f0`   |

The trunk of the two-head network consists of `num_blocks` consecutive blocks.
Each block features two convolutional layers with `num_filters` filters and
with kernel size `conv_kernel_size`. Note that both kernel dimensions must be
odd.

During training, the network is evaluated in training mode on the whole
dataset to compute the loss before it is switched to test model, using
big batches. Therefore, it makes sense to use a high batch norm momentum
(put a lot of weight on the latest measurement).

# AlphaGo Zero Parameters

The network in the original paper from Deepmind features 20 blocks with 256
filters per convolutional layer.
"""
@kwdef struct FieldNetHP
  num_blocks :: Int
  num_filters :: Int
  conv_kernel_size :: Tuple{Int, Int}
  num_policy_head_filters :: Int = 2
  num_value_head_filters :: Int = 1
  batch_norm_momentum :: Float32 = 0.6f0
  #hidden_value_head_layer :: Union{Int,Nothing} = nothing
end

for T in [
  # Network Hyperparameters
  FieldNetHP
]
@eval JSON3.StructType(::Type{<:$T}) = JSON3.Struct()
end

"""
    FieldNet <: TwoHeadNetwork

The convolutional residual network architecture that is used
in the original AlphaGo Zero paper.
"""
mutable struct FieldNet <: TwoHeadNetwork
  gspec
  hyper
  common
  vhead
  phead
end

function FieldNetBlock(size, n, bnmom)
  pad = size .÷ 2
  layers = Chain(
    Conv(size, n=>n, pad=pad),
    BatchNorm(n, relu, momentum=bnmom),
    Conv(size, n=>n, pad=pad),
    BatchNorm(n, momentum=bnmom))
  return Chain(
    SkipConnection(layers, +),
    x -> relu.(x))
end

function FieldNet(gspec::AbstractGameSpec, hyper::FieldNetHP)
  indim = GI.state_dim(gspec)
  outdim = length(Just4Fun.FIELD_VALUES)
  
  ksize = hyper.conv_kernel_size
  
  @assert all(ksize .% 2 .== 1)
  pad = ksize .÷ 2
  
  nf = hyper.num_filters
  npf = hyper.num_policy_head_filters
  nvf = hyper.num_value_head_filters
  bnmom = hyper.batch_norm_momentum
  common = Chain(
    Conv(ksize, indim[3]=>nf, pad=pad),
    BatchNorm(nf, relu, momentum=bnmom),
    [FieldNetBlock(ksize, nf, bnmom) for i in 1:hyper.num_blocks]...
  )
  phead = Chain(
    Conv((1, 1), nf=>npf),
    BatchNorm(npf, relu, momentum=bnmom),
    flatten,
    Dense(indim[1] * indim[2] * npf, outdim),
    softmax
  )
  vhead = Chain(
    Conv((1, 1), nf=>nvf),
    BatchNorm(nvf, relu, momentum=bnmom),
    flatten,
    Dense(indim[1] * indim[2] * nvf, nf, relu),
    Dense(nf, 1, tanh)
  )
  FieldNet(gspec, hyper, common, vhead, phead)
end

Network.HyperParams(::Type{<:FieldNet}) = FieldNetHP

function Base.copy(nn::FieldNet)
  return FieldNet(
    nn.gspec,
    nn.hyper,
    deepcopy(nn.common),
    deepcopy(nn.vhead),
    deepcopy(nn.phead)
  )
end

"""
    forward_normalized(network::AbstractNetwork, states, actions_mask, mask=true)

Evaluate a batch of vectorized states. This function is a wrapper
on [`forward`](@ref) that puts a zero weight on invalid actions.

# Arguments

  - `states` is a tensor whose last dimension has size `bach_size`
  - `actions_mask` is a binary matrix of size `(num_actions, batch_size)`

# Returned value

Return a `(P, V, Pinv)` triple where:

  - `P` is a matrix of size `(num_actions, batch_size)`.
  - `V` is a row vector of size `(1, batch_size)`.
  - `Pinv` is a row vector of size `(1, batch_size)`
     that indicates the total probability weight put by the network
     on invalid actions for each sample.

All tensors manipulated by this function have elements of type `Float32`.

NOTE: default implementation but typed to the network arch.
"""
function Network.forward_normalized(nn::FieldNet, state_cards, actions_mask, mask=true)
  p, v = Network.forward(nn, state_cards)
  p = p .* (mask ? actions_mask : ones(Float64, size(actions_mask)))
  sp = sum(p, dims=1)
  p = p ./ (sp .+ eps(eltype(p)))
  p_invalid = 1 .- sp
  return (p, v, p_invalid)
end

"""
    evaluate(::FieldNet, state, mask=true, available_only=true)

    (nn::FieldNet)(state, mask=true, available_only=true) = evaluate(nn, state, mask, available_only)

Evaluate the neural network as an MCTS oracle on a single state.

Note, however, that evaluating state positions once at a time is slow and so you
may want to use a `BatchedOracle` along with an inference server that uses
[`evaluate_batch`](@ref).
"""
function Network.evaluate(nn::FieldNet, state, mask=true, available_only=true)
  gspec = Network.game_spec(nn)
  available_actions = GI.available_actions(GI.init(gspec, state))

  x = GI.vectorize_state(gspec, state)
  # project the cards actions down to a board based action mask
  a_board = board_position_mask(available_actions)
  
  xnet, anet_board = Network.to_singletons.(Network.convert_input_tuple(nn, (x, Float32.(a_board))))

  net_output_board = Network.forward_normalized(nn, xnet, anet_board, mask)

  p_board, v, _ = Network.from_singletons.(Network.convert_output_tuple(nn, net_output_board))
  # project the board based probabilities up to a cards actions mask
  available_actions_indices = cards_actions_indices(available_actions)
  return (available_only ? p_board[available_actions_indices] : p_board, v[1])
end

(nn::FieldNet)(state, mask=true, available_only=true) = Network.evaluate(nn, state, mask, available_only)

"""
    evaluate_batch(::AbstractNetwork, batch)

Evaluate the neural network as an MCTS oracle on a batch of states at once.

Take a list of states as input and return a list of `(P, V)` pairs as defined in the
MCTS oracle interface.
"""
function Network.evaluate_batch(nn::FieldNet, batch)
  gspec = Network.game_spec(nn)
  
  X = Flux.batch([GI.vectorize_state(gspec, b) for b in batch])
  # project the cards actions down to a board based action masks
  A_board = Flux.batch(board_position_mask(gspec, b) for b in batch)

  Xnet_board, Anet_board = Network.convert_input_tuple(nn, (X, Float32.(A_board)))
  
  P_board, V, _ = Network.convert_output_tuple(nn, Network.forward_normalized(nn, Xnet_board, Anet_board))
  
  # project the board based probabilities up to a cards actions masks
  i_ca(b) = cards_actions_indices(GI.available_actions(GI.init(gspec, b)))

  return [(P_board[i_ca(b),i], V[1,i]) for (i, b) in enumerate(batch)]
end