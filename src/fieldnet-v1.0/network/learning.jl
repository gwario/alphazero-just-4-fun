using AlphaZero: TrainingSample, SamplesWeighingPolicy

#####
##### Converting samples
#####

# A samples collection is represented on the learning side as a (W, X, A, P, V)
# named-tuple. Each component is a `Float32` tensor whose last dimension corresponds
# to the sample index. Writing `n` the number of samples and `a` the total
# number of actions:
# - W (size 1×n) contains the samples weights
# - X (size …×n) contains the board representations
# - A (size a×n) contains the action masks (values are either 0 or 1)
# - P (size a×n) contains the recorded MCTS policies
# - V (size 1×n) contains the recorded values
# Note that the weight of a sample is computed as an increasing
# function of its `n` field.

function AlphaZero.convert_sample(
  gspec::AbstractGameSpec,
  wp::SamplesWeighingPolicy,
  e::TrainingSample)

if wp == CONSTANT_WEIGHT
  w = Float32[1]
elseif wp == LOG_WEIGHT
  w = Float32[log2(e.n) + 1]
else
  @assert wp == LINEAR_WEIGHT
  w = Float32[e.n]
end
x = GI.vectorize_state(gspec, e.s)
g = GI.init(gspec, e.s)
# convert to board mask
a = board_position_mask(gspec, e.s)
# convert to board policy
#π = unique_value_π(g, e.π) was wrong
π = board_π(g, e.π) # NOTE: didnt work too well ?! but it was correct all the time.... :'D

p = zeros(size(a))
p[a] = π
v = [e.z]
return (; w, x, a, p, v)
end