using Base: @kwdef
import JSON3
using Just4Fun
using Flux
using Flux: relu, rrelu, softmax, flatten, batch
using Flux: Chain, Dense, Conv, BatchNorm, SkipConnection
using AlphaZero: Network, CyclicNesterov, CyclicSchedule, Adam
using AlphaZero.NetLib: TwoHeadNetwork, lossgrads


"""
CardNetHP (copy of CardNetHP)

Hyperparameters for the convolutional resnet architecture.
TODO: change
| Parameter                 | Type                | Default   |
|:--------------------------|:--------------------|:----------|
| `num_blocks`              | `Int`               |  -        |
| `num_filters`             | `Int`               |  -        |
| `conv_kernel_size`        | `Tuple{Int, Int}`   |  -        |
| `num_policy_head_filters` | `Int`               | `2`       |
| `num_value_head_filters`  | `Int`               | `1`       |
| `batch_norm_momentum`     | `Float32`           | `0.6f0`   |

The trunk of the two-head network consists of `num_blocks` consecutive blocks.
Each block features two convolutional layers with `num_filters` filters and
with kernel size `conv_kernel_size`. Note that both kernel dimensions must be
odd.

During training, the network is evaluated in training mode on the whole
dataset to compute the loss before it is switched to test model, using
big batches. Therefore, it makes sense to use a high batch norm momentum
(put a lot of weight on the latest measurement).

# AlphaGo Zero Parameters

The network in the original paper from Deepmind features 20 blocks with 256
filters per convolutional layer.
"""
@kwdef struct CardNetHP
  num_card_trunk_layers     :: Int = 1
  num_card_trunk_layer_size :: Int = 36

  num_policy_head_layers    :: Int = 1
  num_policy_head_layer_size:: Int = 2
  
  num_value_head_layers     :: Int = 1
  num_value_head_layer_size :: Int = 2
end

for T in [
  # Network Hyperparameters
  CardNetHP
]
@eval JSON3.StructType(::Type{<:$T}) = JSON3.Struct()
end

"""
CardNet <: CardTrunkTwoHeadNetwork

A dense network architecture.
"""
mutable struct CardNet <: CardTrunkTwoHeadNetwork
  gspec
  hyper
  cards_trunk
  value_head
  policy_head
end

function CardNet(gspec::AbstractGameSpec, hyper::CardNetHP)
  cards_indim = length(GI.vectorize_state(gspec, GI.current_state(GI.init(gspec))))
  
  outdim = length(Just4Fun.FIELD_VALUES)
 
  ncts = hyper.num_card_trunk_layer_size

  nps = hyper.num_policy_head_layer_size
  nvs = hyper.num_value_head_layer_size

  # in (55) (1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4 5 ... 17 18 19)
  cards_trunk = Chain(
    Dense(cards_indim, ncts),
    [
      Dense(ncts, ncts)
      for _ in 1:hyper.num_card_trunk_layers
    ]...,
    Dense(ncts, ncts, rrelu)# TODO: think about batch norm here (like in the board trunk)
  )

  policy_head = Chain(
    Dense(ncts, nps, rrelu),
    [
      Dense(nps, nps)
      for _ in 1:hyper.num_policy_head_layers
    ]...,
    Dense(nps, outdim, relu),
    softmax
  )

  value_head = Chain(
    Dense(ncts, nvs, relu),
    [
      Dense(nvs, nvs)
      for _ in 1:hyper.num_value_head_layers
    ]...,
    Dense(nvs, nvs, relu),
    Dense(nvs, 1, tanh)
  )
  
  CardNet(gspec, hyper, cards_trunk, value_head, policy_head)
end

Network.HyperParams(::Type{<:CardNet}) = CardNetHP

function Base.copy(nn::CardNet)
  return CardNet(
    nn.gspec,
    nn.hyper,
    deepcopy(nn.cards_trunk),
    deepcopy(nn.value_head),
    deepcopy(nn.policy_head)
  )
end

"""
    forward_normalized(network::AbstractNetwork, states, actions_mask, mask=true)

Evaluate a batch of vectorized states. This function is a wrapper
on [`forward`](@ref) that puts a zero weight on invalid actions.

# Arguments

  - `states` is a tensor whose last dimension has size `bach_size`
  - `actions_mask` is a binary matrix of size `(num_actions, batch_size)`

# Returned value

Return a `(P, V, Pinv)` triple where:

  - `P` is a matrix of size `(num_actions, batch_size)`.
  - `V` is a row vector of size `(1, batch_size)`.
  - `Pinv` is a row vector of size `(1, batch_size)`
     that indicates the total probability weight put by the network
     on invalid actions for each sample.

All tensors manipulated by this function have elements of type `Float32`.

NOTE: default implementation but typed to the network arch.
"""
function Network.forward_normalized(nn::CardNet, state_cards, actions_mask, mask=true)
  p, v = Network.forward(nn, state_cards)
  p = p .* (mask ? actions_mask : ones(Float64, size(actions_mask)))
  sp = sum(p, dims=1)
  p = p ./ (sp .+ eps(eltype(p)))
  p_invalid = 1 .- sp
  return (p, v, p_invalid)
end

"""
    evaluate(::CardNet, state, mask=true, available_only=true)

    (nn::CardNet)(state, mask=true) = evaluate(nn, state, mask, available_only)

Evaluate the neural network as an MCTS oracle on a single state.

Note, however, that evaluating state positions once at a time is slow and so you
may want to use a `BatchedOracle` along with an inference server that uses
[`evaluate_batch`](@ref).
"""
function Network.evaluate(nn::CardNet, state, mask=true, available_only=true)
  gspec = Network.game_spec(nn)
  available_actions = GI.available_actions(GI.init(gspec, state))
  
  x = GI.vectorize_state(gspec, state)
  # project the cards actions down to a board based action mask
  a_board = board_position_mask(available_actions)
  
  xnet, anet_board = Network.to_singletons.(Network.convert_input_tuple(nn, (x, Float32.(a_board))))

  net_output_board = Network.forward_normalized(nn, xnet, anet_board, mask)

  p_board, v, _ = Network.from_singletons.(Network.convert_output_tuple(nn, net_output_board))
  # project the board based probabilities up to a cards actions mask
  available_actions_indices = cards_actions_indices(available_actions)
  return (available_only ? p_board[available_actions_indices] : p_board, v[1])
end

(nn::CardNet)(state, mask=true, available_only=true) = Network.evaluate(nn, state, mask, available_only)

"""
    evaluate_batch(::CardNet, batch)

Evaluate the neural network as an MCTS oracle on a batch of states at once.

Take a list of states as input and return a list of `(P, V)` pairs as defined in the
MCTS oracle interface.
"""
function Network.evaluate_batch(nn::CardNet, batch)
  gspec = Network.game_spec(nn)
  
  X = Flux.batch([GI.vectorize_state(gspec, b) for b in batch])
  # project the cards actions down to a board based action masks
  A_board = Flux.batch(board_position_mask(gspec, b) for b in batch)

  Xnet_board, Anet_board = Network.convert_input_tuple(nn, (X, Float32.(A_board)))

  P_board, V, _ = Network.convert_output_tuple(nn, Network.forward_normalized(nn, Xnet_board, Anet_board))
  
  # project the board based probabilities up to a cards actions masks
  i_ca(b) = cards_actions_indices(GI.available_actions(GI.init(gspec, b)))

  return [(P_board[i_ca(b),i], V[1,i]) for (i, b) in enumerate(batch)]
end
