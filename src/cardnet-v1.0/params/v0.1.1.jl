gspec = Just4FunSpec()

#####
##### Training hyperparameters
#####

# does not learn

network = CardNet

netparams = CardNetHP(
  num_card_trunk_layers=4,
  num_card_trunk_layer_size=64,

  num_policy_head_layers=2,
  num_policy_head_layer_size=45,
  
  num_value_head_layers=5,
  num_value_head_layer_size=45
)

self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=800,
    num_workers=64,
    batch_size=64,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    num_iters_per_turn=30,
    dirichlet_noise_ϵ=0.25,
    dirichlet_noise_α=1.0
  )
)

arena_params = ArenaParams(
  sim=SimParams(
    num_games=300,
    num_workers=64,
    batch_size=64,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.03)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=64,
  loss_computation_batch_size=64,
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  nonvalidity_penalty=1.,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=200,
  num_checkpoints=1
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=50,
  ternary_rewards=true,
  mem_buffer_size=PLSchedule(
    [    0,      5,      8,     12,      20,      30],
    [8_000, 15_000, 30_000, 60_000, 150_000, 200_000]
  )
)