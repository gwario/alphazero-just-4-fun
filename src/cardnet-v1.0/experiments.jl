module test
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/CardTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./cardnet-v1.0/network/inputs.jl")
    include("./cardnet-v1.0/network/network.jl")
    include("./cardnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./cardnet-v1.0/experiment/test.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
Just the modified example from example.jl (also accessible via "just4fun")
"""
module v0_0_1
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/CardTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./cardnet-v1.0/network/inputs.jl")
    include("./cardnet-v1.0/network/network.jl")
    include("./cardnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./cardnet-v1.0/experiment/v0.0.1-example.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
based on Experiment_v0_0_1_network_learning_less_cp_deeper but with even deeper trunk
"""
module v0_0_1_network_learning_less_cp_deeper_2
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/CardTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./cardnet-v1.0/network/inputs.jl")
    include("./cardnet-v1.0/network/network.jl")
    include("./cardnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./cardnet-v1.0/experiment/v0.0.1-network-learning-less-cp-deeper-2.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
Configuration with more evaluation games and more MCTS iterations for a more
serious evaluation of the CardNet.
"""
module v0_1_0
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/CardTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./cardnet-v1.0/network/inputs.jl")
    include("./cardnet-v1.0/network/network.jl")
    include("./cardnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./cardnet-v1.0/experiment/v0.1.0.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
Initial report
  
Number of network parameters: 48,496
Number of regularized network parameters: 47,625
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 48,496×3 = 145k samples i.e. at least 10k games

At 800 games per iteration about 15 iterations

800 games self-play with 30 rollouts approx. 15m => more than 4 hours training (+benchmark and checkpoint eval.)

Configuration with more evaluation games and more MCTS iterations for a more
serious evaluation of the CardNet. The cards trunk has a smaller layer size
"""
module v0_1_1
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/CardTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ao.jl")
    include("./cardnet-v1.0/network/network.jl")
    include("./cardnet-v1.0/network/learning.jl")
    # experiment parameters
    include("./cardnet-v1.0/params/v0.1.1.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################