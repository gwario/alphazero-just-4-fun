# First experiment for the 2nd gen architecture and inputs
module test
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/test.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

# First experiment for the 2nd gen architecture and inputs
module v1_0_0_2gen
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.0-2gen.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

# Like Experiment_v1_0_0_2gen but with common dense trunk
module v1_1_0_2gen
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.1-2gen-larger.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end

###################################################################
# Like Experiment_v1_1_0 but with many selfplay rollouts
module v1_1_0_2gen_high_selfplay_rollout
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.2_2gen_high-rollout-selfplay.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

# Rollouts and games similar to Experiment_v0_0_1_network_learning_less_cp_deeper_2
module v1_1_0_2gen_network_learning_less_cp_deeper_2
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.2_2gen_network-learning-less-cp-deeper-2.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

module v1_0_3
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.3.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_4

Initial report

Number of network parameters: 163,081
Number of regularized network parameters: 161,866
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 163,081×3 = 490k samples i.e. at least 40k games

At 600 games per iteration about 65 iterations

600 games self-play with 30 rollouts approx. 1h => more than 70 hours training (+benchmark and checkpoint eval.)


the amount of samples greatly outnumbering the parameters, did not work advantageously. performance is not better.
increasing the number of selfplay games however did greatly increase the number of samples as expected
the incresed number of benchmark games did smooth the win ration noticibly.
"""
module v1_0_4
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.4.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_5

Initial report

Number of network parameters: 163,081
Number of regularized network parameters: 161,866
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 163,081×3 = 490k samples i.e. at least 40k games

At 600 games per iteration about 65 iterations

600 games self-play with 25 rollouts approx. 1h => more than 70 hours training (+benchmark and checkpoint eval.)


the amount of samples greatly outnumbering the parameters, did not work advantageously. performance is not better.
increasing the number of selfplay games however did greatly increase the number of samples as expected
the incresed number of benchmark games did smooth the win ration noticibly.

Changes:
* Added earlier increase of sample number
* Building of the tree over 10 games
* Increasing exploration constant slightly
* Increasing number of checkpoint and banchmark evaluations
"""
module v1_0_5
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.5.jl")
end
export load, gspec, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_6

Initial report

Number of network parameters: 163,081
Number of regularized network parameters: 161,866
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 163,081×3 = 490k samples i.e. at least 18k games (at 16k samples per 600 games)

Drastically more self-play rollouts, since v0_0_5 did not show more performance despite better evaluation and more samples.

At 600 games per iteration => about 16k samples per iteration => about 35 iterations

600 games self-play with 100 rollouts approx. 3h30m => more than 130 hours training (+benchmark and checkpoint eval.)
"""
module v1_0_6
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.6.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_7

Initial report
  
Number of network parameters: 25,177
Number of regularized network parameters: 24,730
Memory footprint per MCTS node: 94806 bytes


Should achieve at least 25,177×3 = 150k samples i.e. at least xxk games (at 16k samples per 600 games)

Drastically more self-play rollouts, since v0_0_5 did not show more performance despite better evaluation and more samples.

At 400 games per iteration => about 16k samples per iteration => about 35 iterations

400 games self-play with 100 rollouts approx. 3h30m => more than 130 hours training (+benchmark and checkpoint eval.)

Changes to 1.0.6
* No position averaging.
* Drastically batches on loss computation
* Smaller network
* less self-play games
"""
module v1_0_7
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.7.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################


"""
v1_0_8

Initial report
  
Number of network parameters: 25,177
Number of regularized network parameters: 24,730
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.7
* reset_every = 1 during self-play as well
* add nw vs nw and rand vs rand to banchmark
* drastic increase of banchmark games
* increase of arena_params games
"""
module v1_0_8
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
    # customisations for the network
    include("./networks/inputs/ma-do-fv_ao.jl")
    include("./cardfieldnet-v1.0/network/network.jl")
    include("./cardfieldnet-v1.0/network/learning.jl")
    include("./cardfieldnet-v1.0/network/training.jl")
    # experiment parameters
    include("./cardfieldnet-v1.0/params/v1.0.8.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_9

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.8
* smaller network
* smaller nonvalidity_penalty - the idea is to train the network on patterns,
  freeze the board trunk parameters and then train again with nonvalidity_penalty
* slightly less mcts iterations
* exploration constant 1
"""
module v1_0_9
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.9.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_10

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.9
* even smaller nonvalidity_penalty
"""
module v1_0_10
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.10.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_11

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.10
* restrict the number of different games to restrict randomness impact and get hopfully a better network policy
* no nonvalidity_penalty
"""
module v1_0_11
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.11.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_12

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.10
* no nonvalidity_penalty
* higher learning rate (0.1)
* fewer self-play and arena_params games
"""
module v1_0_12
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.12.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_13

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.12
* lower learning rate (0.01)
"""
module v1_0_13
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.13.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_14

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.13
* much lower learning rate (0.5e-3)
"""
module v1_0_14
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.14.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_15

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.13
* slightly higher learning rate (0.05)
"""
module v1_0_15
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.15.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_1_0

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.15
* slightly higher learning rate (0.07)
* minimal network
"""
module v1_1_0
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.1.0.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_1_1

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.0
* slightly higher momentum
* memory schedule adapted
"""
module v1_1_1
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.1.1.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_1_2

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.1
* slightly higher momentum
"""
module v1_1_2
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.1.2.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_1_3

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.2
* much more mcts iterations per turn
"""
module v1_1_3
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.1.3.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_1_4

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.2
* batch size of 1
"""
module v1_1_4
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.1.4.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_1_5

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.3
* sample policy CONSTANT_WEIGHT
"""
module v1_1_5
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.1.5.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_1_6

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.5
* sample policy LINEAR_WEIGHT
"""
module v1_1_6
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.1.6.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_16

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.11
* exploration constant 0.7
* hyperparameters from v1.1.6
"""
module v1_0_16
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.16.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_0_17

Initial report
  
Number of network parameters: 7,777
Number of regularized network parameters: 7,522
Memory footprint per MCTS node: 94806 bytes

Changes to 1.0.16
* exploration constant 0.8
* hyperparameters from v1.1.6
"""
module v1_0_17
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.0.17.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_2_0

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.6
* sample policy LOG_WEIGHT and slightly improved parameters
* marker for inputs fix (add player layer)
"""
module v1_2_0
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.2.0.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################


"""
v1_2_1

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.2.0
* just a marker for custom conv filters
"""
module v1_2_1
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.2.1.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_3_0

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.1.6
* sample policy LOG_WEIGHT and slightly improved parameters
* marker for cur player only inputs
"""
module v1_3_0
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.3.0.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_3_1

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.3.0
* huge cpuct
"""
module v1_3_1
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.3.1.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_3_2

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.3.0
* smaller gamma value of 0.9
* smaller number of iterations per turn for speed-up
"""
module v1_3_2
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.3.2.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_4_0_ma_do_ao

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.3.2
* similar params to resnet v0.0.5
"""
module v1_4_0_ma_do_ao
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.4.0.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_4_0_ma_do_fv_ao

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.3.2
* similar params to resnet v0.0.5
"""
module v1_4_0_ma_do_fv_ao
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.4.0.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_4_0_ma_do_pl_fv_ao

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.3.2
* similar params to resnet v0.0.5
"""
module v1_4_0_ma_do_pl_fv_ao
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-pl-fv_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.4.0.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################

"""
v1_4_0_ma_do_pl_ao

Initial report
  
Number of network parameters: 6,849
Number of regularized network parameters: 6,558
Memory footprint per MCTS node: 94806 bytes

Changes to 1.3.2
* similar params to resnet v0.0.5
"""
module v1_4_0_ma_do_pl_ao
function load()
# loading the custom neural network architecture
include("./networks/flux/common.jl")
include("./networks/flux/TwoTrunkTwoHeadNetwork.jl")
# customisations for the network
include("./networks/inputs/ma-do-pl_ao.jl")
include("./cardfieldnet-v1.0/network/network.jl")
include("./cardfieldnet-v1.0/network/learning.jl")
include("./cardfieldnet-v1.0/network/training.jl")
# experiment parameters
include("./cardfieldnet-v1.0/params/v1.4.0.jl")
end
export load, params, self_play_params, arena_params, network, netparams
end
###################################################################