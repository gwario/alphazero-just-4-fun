using AlphaZero: SamplesWeighingPolicy, TrainingSample

#####
##### Converting samples
#####

# A samples collection is represented on the learning side as a (W, X, A, P, V)
# named-tuple. Each component is a `Float32` tensor whose last dimension corresponds
# to the sample index. Writing `n` the number of samples and `a` the total
# number of actions:
# - W (size 1×n) contains the samples weights
# - X (size …×n) contains the board representations
# - A (size a×n) contains the action masks (values are either 0 or 1)
# - P (size a×n) contains the recorded MCTS policies
# - V (size 1×n) contains the recorded values
# Note that the weight of a sample is computed as an increasing
# function of its `n` field.

function AlphaZero.convert_sample(
  gspec::AbstractGameSpec,
  wp::SamplesWeighingPolicy,
  e::TrainingSample)
  if wp == AlphaZero.CONSTANT_WEIGHT
    w = Float32[1]
  elseif wp == AlphaZero.LOG_WEIGHT
    w = Float32[log2(e.n) + 1]
  else
    @assert wp == AlphaZero.LINEAR_WEIGHT
    w = Float32[e.n]
  end
  x_board, x_cards = GI.vectorize_state(gspec, e.s)
  g = GI.init(gspec, e.s)
  # convert to board mask
  a = board_position_mask(gspec, e.s)
  # convert to board policy
  π = board_π(g, e.π) # NOTE: didnt work too well ?! but it was correct all the time.... :'D

  p = zeros(size(a))
  p[a] = π
  v = [e.z]
  return (; w, x_board, x_cards, a, p, v)
end

function AlphaZero.convert_samples(
  gspec::AbstractGameSpec,
  wp::SamplesWeighingPolicy,
  es::Vector{<:TrainingSample})
  ces = [AlphaZero.convert_sample(gspec, wp, e) for e in es]
  W = Flux.batch((e.w for e in ces))
  X_board = Flux.batch((e.x_board for e in ces))
  X_cards = Flux.batch((e.x_cards for e in ces))
  A = Flux.batch((e.a for e in ces))
  P = Flux.batch((e.p for e in ces))
  V = Flux.batch((e.v for e in ces))
  f32(arr) = convert(AbstractArray{Float32}, arr)
  return map(f32, (; W, X_board, X_cards, A, P, V))
end

#####
##### Loss Function
#####

# TODO: is custom loss on cards actions really necessary?
function AlphaZero.losses(nn, regws, params, Wmean, Hp, (W, X_board, X_cards, A, P, V))
  gspec = Network.game_spec(nn)
  # `regws` must be equal to `Network.regularized_params(nn)`
  creg = params.l2_regularization
  cinv = params.nonvalidity_penalty
  ## project the cards actions down to a board based action masks
  #A_board = board_position_masks(gspec, A)
  #P̂_board, V̂, p_board_invalid = Network.forward_normalized(nn, X_board, X_cards, A_board)
  #P̂_board, V̂, p_board_invalid = Network.forward_normalized(nn, X_board, X_cards, A)
  P̂, V̂, p_invalid = Network.forward_normalized(nn, X_board, X_cards, A)
  # project board P up to cards actions based P
  #P̂_cards = cards_actions_policies(gspec, P̂_board)
  V = V ./ params.rewards_renormalization
  V̂ = V̂ ./ params.rewards_renormalization
  #Lp = AlphaZero.klloss_wmean(P̂_board, P, W) - Hp
  Lp = AlphaZero.klloss_wmean(P̂, P, W) - Hp
  Lv = AlphaZero.mse_wmean(V̂, V, W)
  Lreg = iszero(creg) ?
    zero(Lv) :
    creg * sum(sum(w .* w) for w in regws)
  Linv = iszero(cinv) ?
    zero(Lv) :
    #cinv * AlphaZero.wmean(p_board_invalid, W)
    cinv * AlphaZero.wmean(p_invalid, W)
  L = (AlphaZero.mean(W) / Wmean) * (Lp + Lv + Lreg + Linv)
  return (L, Lp, Lv, Lreg, Linv)
end

#####
##### Trainer Utility - NOTE: this is necessary since we can't override the Trainer
#####

struct Trainer
  network :: TwoTrunkTwoHeadNetwork
  samples :: Vector{<:TrainingSample}
  params :: LearningParams
  data :: NamedTuple # (W, X_board, X_cards, A, P, V) tuple obtained after converting `samples`
  Wmean :: Float32
  Hp :: Float32
  batches_stream # infinite stateful iterator of training batches
  function Trainer(gspec, network, samples, params; test_mode=false)
    if params.use_position_averaging
      samples = AlphaZero.merge_by_state(samples)
    end
    data = AlphaZero.convert_samples(gspec, params.samples_weighing_policy, samples)
    network = Network.copy(network, on_gpu=params.use_gpu, test_mode=test_mode)
    W, X_board, X_cards, A, P, V = data
    Wmean = AlphaZero.mean(W)
    Hp = AlphaZero.entropy_wmean(P, W)
    # Create a batches stream
    batchsize = min(params.batch_size, length(W))
    batches = Flux.Data.DataLoader(data; batchsize, partial=false, shuffle=true)
    batches_stream = map(batches) do b
      Network.convert_input_tuple(network, b)
    end |> Util.cycle_iterator |> Iterators.Stateful
    return new(network, samples, params, data, Wmean, Hp, batches_stream)
  end
end

data_weights(tr::Trainer) = tr.data.W

num_samples(tr::Trainer) = length(data_weights(tr))

num_batches_total(tr::Trainer) = num_samples(tr) ÷ tr.params.batch_size

function get_trained_network(tr::Trainer)
  return Network.copy(tr.network, on_gpu=false, test_mode=true)
end

function batch_updates!(tr::Trainer, n)
  regws = Network.regularized_params(tr.network)
  L(batch...) = AlphaZero.losses(tr.network, regws, tr.params, tr.Wmean, tr.Hp, batch)[1]
  data = Iterators.take(tr.batches_stream, n)
  ls = Vector{Float32}()
  Network.train!(tr.network, tr.params.optimiser, L, data, n) do i, l
    push!(ls, l)
  end
  Network.gc(tr.network)
  return ls
end

#####
##### Generating debugging reports
#####

function AlphaZero.learning_status(tr::Trainer, samples)
  # As done now, this is slighly inefficient as we solve the
  # same neural network inference problem twice
  #W, X_board, X_cards, A_cards, P, V = samples
  W, X_board, X_cards, A, P, V = samples
  regws = Network.regularized_params(tr.network)
  Ls = AlphaZero.losses(tr.network, regws, tr.params, tr.Wmean, tr.Hp, samples)
  Ls = Network.convert_output_tuple(tr.network, Ls)
  ## project the cards actions down to a board based action mask
  #A_board = board_position_masks(gspec, A_cards)
  #Pnet, _ = Network.forward_normalized(tr.network, X_board, X_cards, A_board)
  Pnet, _ = Network.forward_normalized(tr.network, X_board, X_cards, A)
  Hpnet = AlphaZero.entropy_wmean(Pnet, W)
  Hpnet = Network.convert_output(tr.network, Hpnet)
  return Report.LearningStatus(Report.Loss(Ls...), tr.Hp, Hpnet)
end

function AlphaZero.learning_status(tr::Trainer)
  batchsize = min(tr.params.loss_computation_batch_size, num_samples(tr))
  batches = Flux.Data.DataLoader(tr.data; batchsize, partial=true)
  reports = map(batches) do batch
    batch = Network.convert_input_tuple(tr.network, batch)
    return AlphaZero.learning_status(tr, batch)
  end
  ws = [sum(batch.W) for batch in batches]
  return AlphaZero.mean_learning_status(reports, ws)
end