# Necessary to force usage of local Trainer (since Trainer has to be modified but can't
# be extended or overwritten we have to define our own)
function AlphaZero.learning_step!(env::Env, handler)
  ap = env.params.arena
  lp = env.params.learning
  checkpoints = Report.Checkpoint[]
  losses = Float32[]
  tloss, teval, ttrain = 0., 0., 0.
  experience = get_experience(env.memory)
  if env.params.use_symmetries
    experience = augment_with_symmetries(env.gspec, experience)
  end
  if isempty(experience)
    # Skipping the learning phase
    return dummy_learning_report()
  end
  trainer, tconvert = @timed Trainer(env.gspec, env.curnn, experience, lp)
  init_status = AlphaZero.learning_status(trainer)
  status = init_status
  Handlers.learning_started(handler)
  # Compute the number of batches between each checkpoint
  nbatches = lp.max_batches_per_checkpoint
  if !iszero(lp.min_checkpoints_per_epoch)
    ntotal = num_batches_total(trainer)
    nbatches = min(nbatches, ntotal ÷ lp.min_checkpoints_per_epoch)
  end
  # Loop state variables
  best_evalr = isnothing(ap) ? nothing : ap.update_threshold
  nn_replaced = false

  for k in 1:lp.num_checkpoints
    # Execute a series of batch updates
    Handlers.updates_started(handler, status)
    dlosses, dttrain = @timed batch_updates!(trainer, nbatches)
    status, dtloss = @timed AlphaZero.learning_status(trainer)
    Handlers.updates_finished(handler, status)
    tloss += dtloss
    ttrain += dttrain
    append!(losses, dlosses)
    # Run a checkpoint evaluation if the arena_params parameter is provided
    if isnothing(ap)
      env.curnn = get_trained_network(trainer)
      env.bestnn = copy(env.curnn)
      nn_replaced = true
    else
      Handlers.checkpoint_started(handler)
      env.curnn = get_trained_network(trainer)
      eval_report =
        AlphaZero.compare_networks(env.gspec, env.curnn, env.bestnn, ap, handler)
      teval += eval_report.time
      # If eval is good enough, replace network
      success = (eval_report.avgr >= best_evalr)
      if success
        nn_replaced = true
        env.bestnn = copy(env.curnn)
        best_evalr = eval_report.avgr
      end
      checkpoint_report =
        Report.Checkpoint(k * nbatches, eval_report, status, success)
      push!(checkpoints, checkpoint_report)
      Handlers.checkpoint_finished(handler, checkpoint_report)
    end
  end
  report = Report.Learning(
    tconvert, tloss, ttrain, teval,
    init_status, losses, checkpoints, nn_replaced)
  Handlers.learning_finished(handler, report)
  return report
end