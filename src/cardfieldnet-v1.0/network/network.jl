using Base: @kwdef
using Zygote: @ignore
import JSON3
using Just4Fun
using Flux
using Flux: relu, rrelu, softmax, flatten, batch
using Flux: Chain, Dense, Conv, BatchNorm, SkipConnection
using AlphaZero: Network

"""
CardFieldNetHP (copy of FieldNetHP)

Hyperparameters for the convolutional resnet architecture.

| Parameter                 | Type                | Default   |
|:--------------------------|:--------------------|:----------|
| `num_blocks`              | `Int`               |  -        |
| `num_filters`             | `Int`               |  -        |
| `conv_kernel_size`        | `Tuple{Int, Int}`   |  -        |
| `num_policy_head_filters` | `Int`               | `2`       |
| `num_value_head_filters`  | `Int`               | `1`       |
| `batch_norm_momentum`     | `Float32`           | `0.6f0`   |

The trunk of the two-head network consists of `num_blocks` consecutive blocks.
Each block features two convolutional layers with `num_filters` filters and
with kernel size `conv_kernel_size`. Note that both kernel dimensions must be
odd.

During training, the network is evaluated in training mode on the whole
dataset to compute the loss before it is switched to test model, using
big batches. Therefore, it makes sense to use a high batch norm momentum
(put a lot of weight on the latest measurement).

# AlphaGo Zero Parameters

The network in the original paper from Deepmind features 20 blocks with 256
filters per convolutional layer.
"""
@kwdef struct CardFieldNetHP
  conv_kernel_size          :: Tuple{Int, Int}

  custom_convolution_filters:: Array{AbstractArray} = []

  num_board_blocks          :: Int = 2
  num_board_filters         :: Int = 45
  num_board_trunk_filters   :: Int = 45
    
  num_card_trunk_layers     :: Int = 1
  num_card_trunk_layer_size :: Int = 36
  
  num_common_layers         :: Int = 1
  num_common_layer_size     :: Int = 2

  num_policy_head_layers    :: Int = 1
  num_policy_head_layer_size:: Int = 2
  
  num_value_head_layers     :: Int = 1
  num_value_head_layer_size :: Int = 2

  batch_norm_momentum       :: Float32 = 0.6f0
end

for T in [
  # Network Hyperparameters
  CardFieldNetHP
]
  @eval JSON3.StructType(::Type{<:$T}) = JSON3.Struct()
end

"""
CardFieldNet <: TwoHeadNetwork

The convolutional residual network architecture that is used
in the original AlphaGo Zero paper.
"""
mutable struct CardFieldNet <: TwoTrunkTwoHeadNetwork
  gspec
  hyper
  board_trunk
  cards_trunk
  common
  value_head
  policy_head
end

function CardFieldNetBlock(size, n, bnmom)
  pad = size .÷ 2
  layers = Chain(
    Conv(size, n=>n, pad=pad),
    BatchNorm(n, relu, momentum=bnmom),
    Conv(size, n=>n, pad=pad),
    BatchNorm(n, momentum=bnmom))
  return Chain(
    SkipConnection(layers, +),
    x -> relu.(x))
end

function CardFieldNet(gspec::AbstractGameSpec, hyper::CardFieldNetHP)
  sample_vector = GI.vectorize_state(gspec, GI.current_state(GI.init(gspec)))
  @assert length(sample_vector) == 2 "Board tensor and cards vector"
  
  board_indim = size(sample_vector[1])
  cards_indim = length(sample_vector[2])

  outdim = length(Just4Fun.FIELD_VALUES)

  ksize = hyper.conv_kernel_size
  
  @assert all(ksize .% 2 .== 1)
  pad = ksize .÷ 2
  
  nbf = hyper.num_board_filters
  nbtf = hyper.num_board_trunk_filters
  
  ncts = hyper.num_card_trunk_layer_size

  ncs = hyper.num_common_layer_size

  nps = hyper.num_policy_head_layer_size
  nvs = hyper.num_value_head_layer_size

  bnmom = hyper.batch_norm_momentum
  
  #test_layer = Conv(ksize, nbf=>nbf, pad=pad) # size(custom_conv.weight) = (3,3,4,4)
  #@show board_indim
  #@show size(test_layer.weight) 
  convolution_layers = []
  
  if isempty(hyper.custom_convolution_filters)
    push!(convolution_layers, Conv(ksize, board_indim[3]=>nbf, pad=pad))
  else
    # Make it (widht height,channel,filters) ... channel would be 2 and filters 4 for hz, vt, d1, d2
    channel_filters = []
    #@show board_indim[3]
    for in_channel in 1:board_indim[3]
      push!(channel_filters, cat(hyper.custom_convolution_filters...; dims=4))
      #@show size(channel_filters[end]) # expect 3 3 1 4
    end
    #@show size(channel_filters) # expect 2
    weights = cat(channel_filters...; dims=3)
    #@show size(weights) # expect 3 3 2 4
    push!(convolution_layers, Conv(weights, pad=pad))
  end
  #@show size(convolution_layers[1].weight)  #(3, 3, 2, 4)

  # in (6, 6, bs)
  board_trunk = Chain(
    convolution_layers...,
    BatchNorm(nbf, relu, momentum=bnmom),
    [
      CardFieldNetBlock(ksize, nbf, bnmom)
      for _ in 1:hyper.num_board_blocks
    ]...,
    Conv((1, 1), nbf=>nbtf),
    BatchNorm(nbtf, relu, momentum=bnmom),
    flatten,
    Dense(prod(board_indim[1:2]) * nbtf, ncs)
  )

  # in (55) (1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4 5 ... 17 18 19)
  cards_trunk = Chain(
    Dense(cards_indim, ncts),
    [
      Dense(ncts, ncts)
      for _ in 1:hyper.num_card_trunk_layers
    ]...,
    Dense(ncts, ncs, rrelu)# TODO: think about batch norm here (like in the board trunk)
  )

  common = Chain(
    Dense(2ncs, ncs),
    [
      Dense(ncs, ncs)
      for _ in 1:hyper.num_common_layers
    ]...
  )

  policy_head = Chain(
    Dense(ncs, nps, rrelu),
    [
      Dense(nps, nps)
      for _ in 1:hyper.num_policy_head_layers
    ]...,
    Dense(nps, outdim, relu),
    softmax
  )

  value_head = Chain(
    Dense(ncs, nvs, relu),
    [
      Dense(nvs, nvs)
      for _ in 1:hyper.num_value_head_layers
    ]...,
    Dense(nvs, nvs, relu),
    Dense(nvs, 1, tanh)
  )
  
  CardFieldNet(gspec, hyper, board_trunk, cards_trunk, common, value_head, policy_head)
end

Network.HyperParams(::Type{<:CardFieldNet}) = CardFieldNetHP

function Base.copy(nn::CardFieldNet)
  return CardFieldNet(
    nn.gspec,
    nn.hyper,
    deepcopy(nn.board_trunk),
    deepcopy(nn.cards_trunk),
    deepcopy(nn.common),
    deepcopy(nn.value_head),
    deepcopy(nn.policy_head)
  )
end

unzip(a) = map(x -> getfield.(a, x), [1, 2])

"""
  forward_normalized(nn::CardFieldNet, state_board, state_cards, actions_mask, mask=true)

Evaluate a batch of vectorized states. This function is a wrapper
on [`forward`](@ref) that puts a zero weight on invalid actions.

# Arguments

  - `states` is a tensor whose last dimension has size `bach_size`
  - `actions_mask` is a binary matrix of size `(num_actions, batch_size)`

# Returned value

Return a `(P, V, Pinv)` triple where:

  - `P` is a matrix of size `(num_actions, batch_size)`.
  - `V` is a row vector of size `(1, batch_size)`.
  - `Pinv` is a row vector of size `(1, batch_size)`
     that indicates the total probability weight put by the network
     on invalid actions for each sample.

All tensors manipulated by this function have elements of type `Float32`.

NOTE: default implementation but typed to the network arch.
"""
function Network.forward_normalized(nn::CardFieldNet, state_board, state_cards, actions_mask, mask=true)
  p, v = Network.forward(nn, (state_board, state_cards))
  p = p .* (mask ? actions_mask : ones(Float64, size(actions_mask)))
  sp = sum(p, dims=1)
  p = p ./ (sp .+ eps(eltype(p)))
  p_invalid = 1 .- sp
  return (p, v, p_invalid)
end

"""
    evaluate(::CardFieldNet, state, mask=true, available_only=true)

    (nn::CardFieldNet)(state, mask=true, available_only=true) = evaluate(nn, state, mask, available_only)

Evaluate the neural network as an MCTS oracle on a single state.

Note, however, that evaluating state positions once at a time is slow and so you
may want to use a `BatchedOracle` along with an inference server that uses
[`evaluate_batch`](@ref).
"""
function Network.evaluate(nn::CardFieldNet, state, mask=true, available_only=true)
  gspec = Network.game_spec(nn)
  available_actions = GI.available_actions(GI.init(gspec, state))

  x_board, x_cards = GI.vectorize_state(gspec, state)
  # project the cards actions down to a board based action mask
  a_board = board_position_mask(available_actions)
  
  xnet_board, xnet_cards, anet_board = Network.to_singletons.(Network.convert_input_tuple(nn, (x_board, x_cards, Float32.(a_board))))

  net_output_board = Network.forward_normalized(nn, xnet_board, xnet_cards, anet_board, mask)
  
  p_board, v, _ = Network.from_singletons.(Network.convert_output_tuple(nn, net_output_board))
  # project the board based probabilities up to a cards actions mask
  available_actions_indices = cards_actions_indices(available_actions)
  return (available_only ? p_board[available_actions_indices] : p_board, v[1])
end

(nn::CardFieldNet)(state, mask=true, available_only=true) = Network.evaluate(nn, state, mask, available_only)

"""
    evaluate_batch(::CardFieldNet, batch)

Evaluate the neural network as an MCTS oracle on a batch of states at once.

Take a list of states as input and return a list of `(P, V)` pairs as defined in the
MCTS oracle interface.
"""
function Network.evaluate_batch(nn::CardFieldNet, batch)
  gspec = Network.game_spec(nn)
  
  batch_tuples = [GI.vectorize_state(gspec, b) for b in batch]
  batch_state_unzpped = unzip(batch_tuples)
  X_board = batch_state_unzpped[1]
  X_cards = batch_state_unzpped[2]
  X_board_batch = Flux.batch(X_board)
  X_cards_batch = Flux.batch(X_cards)
  # project the cards actions down to a board based action masks
  A_board = Flux.batch(board_position_mask(gspec, b) for b in batch)

  Xnet_board, Xnet_cards, Anet_board = Network.convert_input_tuple(nn, (X_board_batch, X_cards_batch, Float32.(A_board)))
  
  P_board, V, _ = Network.convert_output_tuple(nn, Network.forward_normalized(nn, Xnet_board, Xnet_cards, Anet_board))
  
  # project the board based probabilities up to a cards actions masNetwork.forward_normalized
  i_ca(b) = cards_actions_indices(GI.available_actions(GI.init(gspec, b)))

  return [(P_board[i_ca(b),i], V[1,i]) for (i, b) in enumerate(batch)]
end