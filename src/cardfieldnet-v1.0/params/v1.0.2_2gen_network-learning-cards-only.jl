gspec = Just4FunSpec()

#####
##### Training hyperparameters
#####
network = CardFieldNet

netparams = CardFieldNetHP(
  conv_kernel_size=(3, 3),  
  num_board_filters=64,
  num_board_blocks=4,
  
  num_board_trunk_filters=36,

  num_card_trunk_layers=4,
  num_card_trunk_layer_size=36,

  num_common_layers=2,
  num_common_layer_size=36,
  
  num_policy_head_layers=2,
  num_policy_head_layer_size=36,
  
  num_value_head_layers=2,
  num_value_head_layer_size=36
)

# parameters for self play training (create a new AZ-agent)
self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=50,
    num_workers=10,
    batch_size=10,
    reset_every=nothing, # TODO: check average time until reshuffle
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    num_iters_per_turn=20,
    dirichlet_noise_ϵ=0.25, # more exploration in selfplay training than in arena_params
    dirichlet_noise_α=1.0
  )
)

# parameters for comparing the current with the previous AZ-agent
arena_params = ArenaParams(
  sim=SimParams(
    num_games=10,
    num_workers=10,
    batch_size=10,
    reset_every=nothing,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.05 # maybe use a smaller one since network might not be replaced at all...
)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  use_position_averaging=true, # TODO: check with and without maybe big batch size if true otherwise small batchsize
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=16,
  loss_computation_batch_size=16,
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=256,
  num_checkpoints=2
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=100,
  ternary_rewards=true,
  mem_buffer_size=PLSchedule(
    [      0,        15],
    [400_000, 1_000_000]
  )
)
