gspec = Just4FunSpec()

#####
##### Training hyperparameters
#####
network = CardFieldNet

netparams = CardFieldNetHP(
  conv_kernel_size=(3, 3),  
  num_board_filters=4,
  num_board_blocks=3,
  batch_norm_momentum=0.2,
  
  num_board_trunk_filters=4,

  num_card_trunk_layers=1,
  num_card_trunk_layer_size=16,

  num_common_layers=3,
  num_common_layer_size=16,
  
  num_policy_head_layers=1,
  num_policy_head_layer_size=6,
  
  num_value_head_layers=1,
  num_value_head_layer_size=6
)

# parameters for self play training (create a new AZ-agent)
self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=300,
    num_workers=64,
    batch_size=64,
    reset_every=1,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    num_iters_per_turn=200,
    cpuct=1.,
    dirichlet_noise_ϵ=0.25, # more exploration in selfplay training than in arena_params
    dirichlet_noise_α=1.0
  )
)

# parameters for comparing the current with the previous AZ-agent
arena_params = ArenaParams(
  sim=SimParams(
    num_games=300,
    num_workers=64,
    batch_size=64,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.02 # maybe use a smaller one since network might not be replaced at all...
)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  use_position_averaging=false,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=64,
  nonvalidity_penalty=1,
  loss_computation_batch_size=64,
  optimiser=Network.Adam(lr=0.065),
  l2_regularization=1e-3,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=1024,
  num_checkpoints=1
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=30,
  ternary_rewards=true,
  mem_buffer_size=PLSchedule(
    [     0,      5,     15,      30],
    [10_000, 20_000, 50_000, 200_000]
  )
)