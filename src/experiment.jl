module Experiments
using ..Just4Fun

    """
    Network architecture that uses cards as input.
    """
    module CardNet
    using ..Just4Fun
    include("./cardnet-v1.0/experiments.jl")
    export load
    end

    """
    Network architecture that uses fields as input.
    """
    module FieldNet
    using ..Just4Fun
    include("./fieldnet-v1.0/experiments.jl")
    export load
    end

    """
    Network architecture that uses fields as input and all possible card combinations (3.9k).
    """
    module ResNet
    using ..Just4Fun
    include("./resnet/experiments.jl")
    export load
    end

    """
    Network architecture that uses cards and fields as input.
    """
    module CardFieldNet
    using ..Just4Fun
    using AlphaZero
    include("./cardfieldnet-v1.0/experiments.jl")
    export load
    end
end