using AlphaZero
using AlphaZero: Network, NetLib

"""
    forward_normalized(network::AbstractNetwork, states, actions_mask, mask=true)

Evaluate a batch of vectorized states. This function is a wrapper
on [`forward`](@ref) that puts a zero weight on invalid actions.

# Arguments

  - `states` is a tensor whose last dimension has size `bach_size`
  - `actions_mask` is a binary matrix of size `(num_actions, batch_size)`

# Returned value

Return a `(P, V, Pinv)` triple where:

  - `P` is a matrix of size `(num_actions, batch_size)`.
  - `V` is a row vector of size `(1, batch_size)`.
  - `Pinv` is a row vector of size `(1, batch_size)`
     that indicates the total probability weight put by the network
     on invalid actions for each sample.

All tensors manipulated by this function have elements of type `Float32`.

NOTE: default implementation but typed to the network arch.
"""
function Network.forward_normalized(nn::ResNet, state_board, actions_mask, mask=true)
  p, v = Network.forward(nn, state_board)
  p = p .* (mask ? actions_mask : ones(Float64, size(actions_mask)))
  sp = sum(p, dims=1)
  p = p ./ (sp .+ eps(eltype(p)))
  p_invalid = 1 .- sp
  return (p, v, p_invalid)
end

"""
    evaluate(::ResNet, state, mask=true, available_only=true)

    (nn::ResNet)(state, mask=true, available_only=true) = evaluate(nn, state, mask, available_only)

Evaluate the neural network as an MCTS oracle on a single state.

Note, however, that evaluating state positions once at a time is slow and so you
may want to use a `BatchedOracle` along with an inference server that uses
[`evaluate_batch`](@ref).
"""
function Network.evaluate(nn::ResNet, state, mask=true, available_only=true)
  gspec = Network.game_spec(nn)
  actions_mask = GI.actions_mask(GI.init(gspec, state))
  x = GI.vectorize_state(gspec, state)
  a = Float32.(actions_mask)
  xnet, anet = Network.to_singletons.(Network.convert_input_tuple(nn, (x, a)))
  net_output = Network.forward_normalized(nn, xnet, anet, mask)
  p, v, _ = Network.from_singletons.(Network.convert_output_tuple(nn, net_output))
  return (available_only ? p[actions_mask] : p, v[1])
end

(nn::ResNet)(state, mask=true, available_only=true) = Network.evaluate(nn, state, mask, available_only)