gspec = Just4FunSpec()

#####
##### Training hyperparameters
#####

# does not learn

network = NetLib.ResNet

netparams = NetLib.ResNetHP(
  num_filters=1,
  num_blocks=1,
  conv_kernel_size=(3, 3),
  num_policy_head_filters=1,
  num_value_head_filters=1,
  batch_norm_momentum=0.1
)

self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=50,
    num_workers=10,
    batch_size=10,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    num_iters_per_turn=20,
    dirichlet_noise_ϵ=0.25,
    dirichlet_noise_α=1.0
  )
)

arena_params = ArenaParams(
  sim=SimParams(
    num_games=10,
    num_workers=10,
    batch_size=10,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.05)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=16,
  loss_computation_batch_size=16,
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  nonvalidity_penalty=1.,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=2000,
  num_checkpoints=2
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=100,
  ternary_rewards=true,
  mem_buffer_size=PLSchedule(
    [      0,        15],
    [400_000, 1_000_000]
  )
)
