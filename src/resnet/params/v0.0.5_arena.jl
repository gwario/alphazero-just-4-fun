gspec = Just4FunSpec()

#####
##### Training hyperparameters
#####

network = NetLib.ResNet

netparams = NetLib.ResNetHP(
  num_filters=2,
  num_blocks=1,
  conv_kernel_size=(3, 3),
  num_policy_head_filters=2,
  num_value_head_filters=1
)

self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=900,
    num_workers=64,#256 -> 6h, 128 -> 1h15
    batch_size=64,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    num_iters_per_turn=20,
    cpuct=1.5,
    prior_temperature=1.0,
    temperature=PLSchedule([0, 20, 30], [1.0, 1.0, 0.3]),
    dirichlet_noise_ϵ=0.25,
    dirichlet_noise_α=1.0
  )
)

arena_params = ArenaParams(
  sim=SimParams(
    num_games=200,
    num_workers=64,
    batch_size=64,
    use_gpu=USE_GPU
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    cpuct=1.,
    temperature=ConstSchedule(0.2),
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.05
)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=64, # other (smaller) values seem to make problems
  loss_computation_batch_size=64, # other (smaller) values seem to make problems
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=500, #no stack StackOverflowError with 64 and 500 but seems to be asleep forever
  num_checkpoints=1
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=40,
  ternary_rewards=true,
  mem_buffer_size=PLSchedule(
    [     0,      6,      10,     15,        40],
    [30_000, 70_000, 150_000, 300_000, 1_000_000]
  )
)
