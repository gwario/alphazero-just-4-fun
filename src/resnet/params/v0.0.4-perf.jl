gspec = Just4FunSpec()

#####
##### Training hyperparameters
#####

network = NetLib.ResNet

netparams = NetLib.ResNetHP(
  num_filters=45,
  num_blocks=5,
  conv_kernel_size=(3, 3),
  num_policy_head_filters=16,
  num_value_head_filters=45,
  batch_norm_momentum=0.1
)

self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=60,
    num_workers=3,
    batch_size=3,
    use_gpu=USE_GPU,
    reset_every=nothing,
    flip_probability=0.,
    alternate_colors=false
  ),
  mcts=MctsParams(
    num_iters_per_turn=25,
    cpuct=3.0,
    prior_temperature=1.0,
    temperature=PLSchedule([0, 20, 30], [1.0, 1.0, 0.3]),
    dirichlet_noise_ϵ=0.25,
    dirichlet_noise_α=1.0
  )
)

arena_params = ArenaParams(
  sim=SimParams(
    num_games=30,
    num_workers=3,
    batch_size=3,
    use_gpu=USE_GPU,
    reset_every=nothing,
    flip_probability=0.,
    alternate_colors=true
  ),
  mcts=MctsParams(
    self_play_params.mcts,
    cpuct=1.5,
    temperature=ConstSchedule(0.2),
    dirichlet_noise_ϵ=0.05
  ),
  update_threshold=0.05
)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  use_position_averaging=true,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=1024,
  loss_computation_batch_size=1024,
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  nonvalidity_penalty=1.5,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=2000,
  num_checkpoints=1
)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=30,
  ternary_rewards=true,
  use_symmetries=true,
  mem_buffer_size=PLSchedule(
    [      0,        15],
    [400_000, 1_000_000]
  )
)
