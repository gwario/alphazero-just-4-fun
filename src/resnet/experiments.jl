module test
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/test.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# Just the modified example from example.jl (also accessible via "just4fun")
module v0_0_1
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-example.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based on Experiment_v0_0_1 but with slightly more rollouts and iterations
module v0_0_1_network_learning
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-network-learning.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based on Experiment_v0_0_1_network_learning but with slightly less checkpoints
module v0_0_1_network_learning_less_cp
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-network-learning-less-cp.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based on Experiment_v0_0_1_network_learning_less_cp but with deeper network and sensible number of filters
module v0_0_1_network_learning_less_cp_deeper
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-network-learning-less-cp-deeper.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based on Experiment_v0_0_1_network_learning_less_cp_deeper but with even deeper trunk
module v0_0_1_network_learning_less_cp_deeper_2
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-network-learning-less-cp-deeper-2.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based on Experiment_v0_0_1_network_learning_less_cp_deeper but with even deeper trunk
module v0_0_1_network_learning_less_cp_deeper_2_gpu_test
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-network-learning-less-cp-deeper-2-gpu-test.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based Experiment_v0_0_1 with more iterations and setting a single per game seed from the experiment 
module v0_0_1_single_seed
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-fast-single-seed.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based Experiment_v0_0_1 with more iterations and setting multiple per game seeds (that are randomly chosen) from the experiment 
module v0_0_1_multiple_seeds
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-fast-multiple-seeds.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

# based Experiment_v0_0_1 with more iterations and setting many per game seeds (that are randomly chosen) from the experiment 
module v0_0_1_many_seeds
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.1-fast-many-seeds.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

module v0_0_2
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.2-example.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

module v0_0_3
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.3.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

module v0_0_4_perf
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.4-perf.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
    v0_0_5_arena

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 286,688×3 = 860k samples i.e. at least 55k games

At 800 games per iteration about 70 iterations

800 games self-play with 30 rollouts approx. 30m => more than 40 hours training (+benchmark and checkpoint eval.)
"""
module v0_0_5_arena
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.5_arena.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_0_5_ma_pl

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Change to v0.0.5:
* no arena, network is always updated

"""
module v0_0_5_ma_pl
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-pl.jl")
    include("./resnet/params/v0.0.5.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_0_5_ma_do

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 286,688×3 = 860k samples i.e. at least 55k games

Change to v0_0_5_ma_pl:
* No change, just a marker for differnent inputs

"""
module v0_0_5_ma_do
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-do.jl")
    include("./resnet/params/v0.0.5.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_0_5_ma_do_fv

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 286,688×3 = 860k samples i.e. at least 55k games

Change to v0_0_5_ma_pl:
* No change, just a marker for differnent inputs

"""
module v0_0_5_ma_do_fv
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-do-fv.jl")
    include("./resnet/params/v0.0.5.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_0_5_st_ma_do_fv

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 286,688×3 = 860k samples i.e. at least 55k games

Change to v0_0_5_ma_pl:
* No change, just a marker for differnent inputs

"""
module v0_0_5_st_ma_do_fv
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/st-ma-do-fv.jl")
    include("./resnet/params/v0.0.5.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_0_5_st_ma_do_fv_pl

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 286,688×3 = 860k samples i.e. at least 55k games

Change to v0_0_5_ma_pl:
* No change, just a marker for differnent inputs

"""
module v0_0_5_st_ma_do_fv_pl
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/st-ma-do-fv-pl.jl")
    include("./resnet/params/v0.0.5.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_0_5_st_fv_pl

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 286,688×3 = 860k samples i.e. at least 55k games

Change to v0_0_5_ma_pl:
* No change, just a marker for differnent inputs

"""
module v0_0_5_st_fv_pl
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/st-fv-pl.jl")
    include("./resnet/params/v0.0.5.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_0_5_st_fv

Initial report
  
Number of network parameters: 286,688
Number of regularized network parameters: 282,734
Memory footprint per MCTS node: 94806 bytes

Should achieve at least 286,688×3 = 860k samples i.e. at least 55k games

Change to v0_0_5_ma_pl:
* No change, just a marker for differnent inputs

"""
module v0_0_5_st_fv
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/st-fv.jl")
    include("./resnet/params/v0.0.5.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end
###################################################################

"""
v0_1_0_ma_do

Initial report
  
    Number of network parameters: 2,265,440
    Number of regularized network parameters: 2,261,376
    Memory footprint per MCTS node: 94806 bytes
  
Running benchmark: Network Only against Random

Change to v0_0_5:
* Adapted memory schedule
* increased network size
* exploration constant of 1

"""
module v0_1_0_ma_do
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-do.jl")
    include("./resnet/params/v0.1.0.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end

"""
v0_1_0_ma_do_fv

Initial report
  
    Number of network parameters: 2,265,440
    Number of regularized network parameters: 2,261,376
    Memory footprint per MCTS node: 94806 bytes
  
Running benchmark: Network Only against Random

Change to v0_0_5:
* Adapted memory schedule
* increased network size
* exploration constant of 1

"""
module v0_1_0_ma_do_fv
function load()
    # loading the custom neural network architecture
    include("./networks/flux/common.jl")
    # customisations for the network
    include("./resnet/network/network.jl")
    include("./resnet/network/learning.jl")
    include("./networks/inputs/ma-do-fv.jl")
    include("./resnet/params/v0.1.0.jl")
end
export load, params, arena_params, self_play_params, network, netparams
end