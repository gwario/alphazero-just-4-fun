using AlphaZero: Network
using AlphaZero.FluxLib: FluxNetwork
using Flux: Chain
import Zygote


"""
CardTrunkTwoHeadNetwork <: FluxNetwork

An abstract type for card-trunk two-head neural networks implemented with Flux.

Subtypes are assumed to have fields
`hyper`, `gspec`, `cards_trunk`, `value_head` and
`policy_head`. Based on those, an implementations provided
for [`Network.hyperparams`](@ref), [`Network.game_spec`](@ref),
[`Network.forward`](@ref) and [`Network.on_gpu`](@ref), leaving only
[`Network.HyperParams`](@ref) to be implemented.
"""
abstract type CardTrunkTwoHeadNetwork <: FluxNetwork end

function model(nn::CardTrunkTwoHeadNetwork)::Chain
  # TODO: should also work as in resnet with c = nn.cards_trunk(state); p = nn.policy_head(c); v = nn.value_head(c)
  Chain(
    nn.cards_trunk,
    Split(nn.policy_head, nn.value_head)
  )
end

function Network.forward(nn::CardTrunkTwoHeadNetwork, state_cards)
  ((p, v),) = model(nn)(state_cards)
  return (p, v)
end

# Flux.@functor does not work with abstract types
function Flux.functor(nn::Net) where Net <: CardTrunkTwoHeadNetwork
  children = (nn.cards_trunk, nn.value_head, nn.policy_head)
  constructor = cs -> Net(nn.gspec, nn.hyper, cs...)
  return (children, constructor)
end

Network.hyperparams(nn::CardTrunkTwoHeadNetwork) = nn.hyper

Network.game_spec(nn::CardTrunkTwoHeadNetwork) = nn.gspec

Network.on_gpu(nn::CardTrunkTwoHeadNetwork) = array_on_gpu(nn.value_head[end].bias)