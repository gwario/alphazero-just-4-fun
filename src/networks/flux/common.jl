using Just4Fun
using AlphaZero
using JSON3
using AlphaZero.FluxLib: FluxNetwork
using CUDA
using Base: @kwdef
import Flux

array_on_gpu(::Array) = false
array_on_gpu(::CuArray) = true
array_on_gpu(arr) = error("Usupported array type: ", typeof(arr))


struct Split{T}
  paths::T
end

Split(paths...) = Split(paths)

Flux.@functor Split

(m::Split)(x) = tuple(map(f -> f(x), m.paths))


"""
  CARD_VECTOR_ORDER

Reprensents a card value to position mapping for neural network inputs.
"""
const CARD_VECTOR_ORDER = [
    1, 1, 1, 1, 2, 2, 2, 2,
    3, 3, 3, 3, 4, 4, 4, 4,
    5, 5, 5, 5, 6, 6, 6, 6,
    7, 7, 7, 7, 8, 8, 8, 8,
    9, 9, 9, 9, 10, 10, 10, 10,
    11, 11, 11, 11, 12, 12, 12, 12,
    13, 14, 15, 16, 17, 18, 19
]

"""
  NN_OUTPUT_FIELD_VALUE_MAPPING

In case the output of the neural network is field probabilities, this map
ensures the probabilities are mapped to the field values based on their board
position. This makes the network learn the right positions not the right
numbers, an thus allows the network to work with different field value
distributions.
"""
const NN_OUTPUT_FIELD_VALUE_MAPPING = vec(Just4Fun.FIELD_VALUES)


"""
  board_position_mask(g::Just4FunEnv)::Vector{Bool}

Maps the cards action mask to a field action mask
"""
function board_position_mask(available_actions::Vector{CardsAction})::Vector{Bool}
  available_actions_values = map(a -> a.value, available_actions)
  available_actions_indices = findall(field_value -> field_value in available_actions_values, NN_OUTPUT_FIELD_VALUE_MAPPING)

  mask = falses(length(NN_OUTPUT_FIELD_VALUE_MAPPING))
  mask[available_actions_indices] .= true

  return mask
end


"""
  board_position_mask(gspec, state)::Vector{Bool}

Maps the cards action mask to a field action mask.

The mapping is performed according to position (not field value). So the index
of a neural network output always points to the same position on the board (not
neccessarily the same value).
"""
function board_position_mask(gspec, state)::Vector{Bool}
  available_actions = GI.available_actions(GI.init(gspec, state))
  available_actions_values = map(a -> a.value, available_actions)
  available_actions_indices = findall(field_value -> field_value in available_actions_values, NN_OUTPUT_FIELD_VALUE_MAPPING)

  mask = falses(length(NN_OUTPUT_FIELD_VALUE_MAPPING))
  mask[available_actions_indices] .= true

  return mask
end


"""
unique_value_π(gspec::AbstractGameSpec, π)

Reduces the cards-based policy down to the board-based policy, i.e. removing
actions with duplicate values as they are the same on the board.

NOTE: UNUSED
NOTE: assumes that all combinations with the same value have the same probability (as it should be according to network output)

Incorrect since the policy pi must match the size of GI.available_actions! Proof: e.g. AlphaZero.jl/games/connect-four/solver.jl
"""
function unique_value_π(game::AbstractGameEnv, π)
  throw(AssertionError("Don't use! - see docstring"))
  available_actions = filter(a -> a.value != 0, GI.available_actions(game))
  @assert length(available_actions) == length(π)
  available_actions_unique = unique(a -> a.value, available_actions)
  π_unique = zeros(Float32, length(available_actions_unique))
  for (i, unique_action) in enumerate(available_actions_unique)
    action_index = findfirst(a -> a.cards == unique_action.cards, available_actions)
    π_unique[i] = π[action_index]
  end
  return π_unique
end

"""
board_π(gspec::AbstractGameSpec, π)

Reduces the cards-based policy down to the board-based policy, i.e. removing
actions with duplicate values as they are the same on the board.

The reduced policy repects the the to position (not field value). So the order
is according to the field value distribution (not neccessarily the field
value).

NOTE: assumes that all combinations with the same value have the same probability (as it should be according to network output)
"""
function board_π(game::AbstractGameEnv, π)
  available_actions = filter(a -> a.value != 0, GI.available_actions(game))
  @assert length(available_actions) == length(π)
  π_board_indices = filter(!isnothing, [
    findfirst(action -> action.value == board_action_value, available_actions)
    for board_action_value in NN_OUTPUT_FIELD_VALUE_MAPPING
  ])
  π_unique_ordered = π[π_board_indices]
  return π_unique_ordered
end


"""
  cards_actions_indices(available_actions::Vector{CardsAction})::Vector{Int64}

Finds for all available actions the index in on the board.
FIXME: consider using the iterate-over-NN_OUTPUT_FIELD_VALUE_MAPPING approach
"""
function cards_actions_indices(available_actions::Vector{CardsAction})::Vector{Int64}
  return [findfirst(isequal(action.value), NN_OUTPUT_FIELD_VALUE_MAPPING) for action in filter(a -> a.value != 0, available_actions)]
end