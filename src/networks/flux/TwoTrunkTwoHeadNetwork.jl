using AlphaZero: Network
using AlphaZero.FluxLib: FluxNetwork
using Flux: Chain
import Zygote


"""
TwoTrunkTwoHeadNetwork <: FluxNetwork

An abstract type for two-trunk two-head neural networks implemented with Flux.

Subtypes are assumed to have fields
`hyper`, `gspec`, `board_trunk`, `cards_trunk`, `common`, `value_head` and
`policy_head`. Based on those, an implementations provided
for [`Network.hyperparams`](@ref), [`Network.game_spec`](@ref),
[`Network.forward`](@ref) and [`Network.on_gpu`](@ref), leaving only
[`Network.HyperParams`](@ref) to be implemented.
"""
abstract type TwoTrunkTwoHeadNetwork <: FluxNetwork end

function model(nn::TwoTrunkTwoHeadNetwork)::Chain
  m = Chain(
    Parallel(vcat, nn.board_trunk, nn.cards_trunk),
    nn.common,
    Split(nn.policy_head, nn.value_head)
  )

  if !isempty(nn.hyper.custom_convolution_filters)
    # Fix only the layer with handcrafted wieghts
    custom_conv = nn.board_trunk[1]
    #@show size(custom_conv.weight)
    custom_conv_params = Flux.params(custom_conv)
    m_params = Flux.params(m)
    delete!(m_params, custom_conv_params)
    Flux.trainable(m_params)
  end

  return m
end

function Network.forward(nn::TwoTrunkTwoHeadNetwork, state_board_cards)
  ((p, v),) = model(nn)(state_board_cards)
  return (p, v)
end

# Flux.@functor does not work with abstract types
function Flux.functor(nn::Net) where Net <: TwoTrunkTwoHeadNetwork
  children = (nn.board_trunk, nn.cards_trunk, nn.common, nn.value_head, nn.policy_head)
  constructor = cs -> Net(nn.gspec, nn.hyper, cs...)
  return (children, constructor)
end

Network.hyperparams(nn::TwoTrunkTwoHeadNetwork) = nn.hyper

Network.game_spec(nn::TwoTrunkTwoHeadNetwork) = nn.gspec

Network.on_gpu(nn::TwoTrunkTwoHeadNetwork) = array_on_gpu(nn.value_head[end].bias)