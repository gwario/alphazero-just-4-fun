using AlphaZero
using Just4Fun: SIDE_LENGTH, NUM_PLAYERS, FIELD_VALUES, has_majority, dominant_player, to_index, YELLOW, Player


@enum Layers stones majority dominance field_values cur_player

player_layer_types = [stones]
n_player_layer_types = length(player_layer_types)


player_layers = collect(Iterators.flatten(zip(player_layer_types, player_layer_types)))
player_layers_players = repeat(Player(1):Player(NUM_PLAYERS), n_player_layer_types)

board_layers = [field_values, cur_player]
board_layers_players = [Player(0), Player(0)] # not used for those two layers

all_layers = vcat(player_layers, board_layers)
all_layers_players = vcat(player_layers_players, board_layers_players)

current_player_layer(curplayer::Player)::Float32 = curplayer == Player(YELLOW) ? 1.0 : 0.0

"""
Retruns the inputs to the neural network.
"""
function GI.vectorize_state(spec::Just4FunSpec, state)
    board_vector = Float32[
        layer == stones         ? state.field_stones[y, x, to_index(player)] :
        layer == field_values   ? FIELD_VALUES[y, x] :
        layer == cur_player     ? current_player_layer(state.curplayer) :
        throw(AssertionError("Unhandled layer $index !"))
        for y in 1:SIDE_LENGTH,
            x in 1:SIDE_LENGTH,
            (layer, player) in zip(all_layers, all_layers_players)
    ]
    #(stones, 1)
    #(stones, 2)
    #(field_values, 0)
    #(cur_player, 0)
    return board_vector
end