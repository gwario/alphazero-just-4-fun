using AlphaZero
using Just4Fun: SIDE_LENGTH, NUM_PLAYERS, has_majority, Player, YELLOW, Just4FunSpec, USE_GPU

const L_CUR_PLAYER = 10

current_player_layer(curplayer::Player)::Float32 = curplayer == Player(YELLOW) ? 1.0 : 0.0

function GI.vectorize_state(spec::Just4FunSpec, state)
  vector = Float32[
    l <= NUM_PLAYERS ? has_majority(state.field_stones[y, x, :], Player(l)) :
    l == L_CUR_PLAYER ? current_player_layer(state.curplayer) :
    throw(AssertionError("Unhandled layer $l !"))
    for y in 1:SIDE_LENGTH,
        x in 1:SIDE_LENGTH,
        l in vcat(1:NUM_PLAYERS, L_CUR_PLAYER)
  ]
  return vector
end