using AlphaZero
using Just4Fun: Just4FunSpec

"""
Retruns the inputs to the neural network.
All cards ordered for the current player.
"""
function GI.vectorize_state(spec::Just4FunSpec, state)
    cards_vector = zeros(Float32, 55)
    for player_card in state.player_cards[:, state.curplayer]
        positions = findall(isequal(player_card), CARD_VECTOR_ORDER)
        for pos in positions
            if cards_vector[pos] == 0.0
                cards_vector[pos] = 1.0
                break
            end
        end
    end
    return cards_vector
end