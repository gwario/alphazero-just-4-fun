using AlphaZero
using Just4Fun: SIDE_LENGTH, NUM_PLAYERS, FIELD_VALUES, has_majority, dominant_player, to_index, YELLOW, Player


@enum Layers majority dominance cur_player

player_layer_types = instances(Layers)[1:end-1]
n_player_layer_types = length(player_layer_types)


player_layers = collect(Iterators.flatten(zip(player_layer_types, player_layer_types)))
player_layers_players = repeat(Player(1):Player(NUM_PLAYERS), n_player_layer_types)

board_layers = [cur_player]
board_layers_players = [Player(0)] # not used for those two layers

all_layers = vcat(player_layers, board_layers)
all_layers_players = vcat(player_layers_players, board_layers_players)

current_player_layer(curplayer::Player)::Float32 = curplayer == Player(YELLOW) ? 1.0 : 0.0

"""
Retruns the inputs to the neural network.
"""
function GI.vectorize_state(spec::Just4FunSpec, state)
    board_vector = Float32[
        layer == majority       ? has_majority(state.field_stones[y, x, :], player) :
        layer == dominance      ? dominant_player(state.field_stones[y, x, :]) == player :
        layer == cur_player     ? current_player_layer(state.curplayer) :
        throw(AssertionError("Unhandled layer $index !"))
        for y in 1:SIDE_LENGTH,
            x in 1:SIDE_LENGTH,
            (layer, player) in zip(all_layers, all_layers_players)
    ]
    cards_vector = zeros(Float32, 55)
    for player_card in state.player_cards[:, state.curplayer]
        positions = findall(isequal(player_card), CARD_VECTOR_ORDER)
        for pos in positions
            if cards_vector[pos] == 0.0
                cards_vector[pos] = 1.0
                break
            end
        end
    end
    #(majority, 1)
    #(majority, 2)
    #(dominance, 1)
    #(dominance, 2)
    #(cur_player, 0)
    return (board_vector, cards_vector)
end