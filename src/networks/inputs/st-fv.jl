using AlphaZero
using Just4Fun: SIDE_LENGTH, NUM_PLAYERS, FIELD_VALUES, has_majority, dominant_player, to_index, YELLOW, Player


@enum Layers stones majority dominance field_values cur_player

"""
Retruns the inputs to the neural network.
"""
function GI.vectorize_state(spec::Just4FunSpec, state)
    board_vector = Float32[
        layer == stones         ? state.field_stones[y, x, to_index(state.curplayer)] :
        layer == field_values   ? FIELD_VALUES[y, x] :
        throw(AssertionError("Unhandled layer $index !"))
        for y in 1:SIDE_LENGTH,
            x in 1:SIDE_LENGTH,
            layer in [stones, field_values]
    ]
    #(stones, 1)
    #(field_values, 0)
    return board_vector
end