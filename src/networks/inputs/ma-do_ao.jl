using AlphaZero
using Just4Fun: SIDE_LENGTH, NUM_PLAYERS, FIELD_VALUES, has_majority, dominant_player


@enum Layers majority dominance

"""
Retruns the inputs to the neural network.
"""
function GI.vectorize_state(spec::Just4FunSpec, state)
    board_vector = Float32[
        layer == majority       ? has_majority(state.field_stones[y, x, :], state.curplayer) :
        layer == dominance      ? dominant_player(state.field_stones[y, x, :]) == state.curplayer :
        throw(AssertionError("Unhandled layer $index !"))
        for y in 1:SIDE_LENGTH,
            x in 1:SIDE_LENGTH,
            layer in instances(Layers)
    ]

    cards_vector = zeros(Float32, 55)
    for player_card in state.player_cards[:, state.curplayer]
        positions = findall(isequal(player_card), CARD_VECTOR_ORDER)
        for pos in positions
            if cards_vector[pos] == 0.0
                cards_vector[pos] = 1.0
                break
            end
        end
    end
    return (board_vector, cards_vector)
end