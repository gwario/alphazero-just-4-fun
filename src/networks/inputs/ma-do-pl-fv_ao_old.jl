using AlphaZero
using Just4Fun: SIDE_LENGTH, NUM_PLAYERS, FIELD_VALUES, has_majority, dominant_player, YELLOW

const L_FIELD_VALUES = 10
const L_CUR_PLAYER = 11

current_player_layer(curplayer::Player)::Float32 = curplayer == Player(YELLOW) ? 1.0 : 0.0

"""
Retruns the inputs to the neural network.
TODO: rotate according to curplayer, YELLOW = 0, RED = -1, .. -2 ...
"""
function GI.vectorize_state(spec::Just4FunSpec, state)
    board_vector = Float32[
        index <=     NUM_PLAYERS    ? has_majority(state.field_stones[y, x, :], Player(player)) :
        index <= 2 * NUM_PLAYERS    ? dominant_player(state.field_stones[y, x, :]) == Player(player) :
        player == L_CUR_PLAYER      ? current_player_layer(state.curplayer) :
        player == L_FIELD_VALUES    ? FIELD_VALUES[y, x] :
        throw(AssertionError("Unhandled layer $index !"))
        for y in 1:SIDE_LENGTH,
            x in 1:SIDE_LENGTH,
            (index, player) in enumerate(vcat(
                repeat(unique(vcat(state.curplayer, 1:NUM_PLAYERS)), 2), # player layers
                [L_CUR_PLAYER, L_FIELD_VALUES]                                         # general layers
                )) # [(1, 2),(2, 1),(3, 2),(4, 1),(5,L_CUR_PLAYER),(6,L_FIELD_VALUES)] or [(1, 1),(2, 2),(3, 1),(4, 2),(5,L_CUR_PLAYER),(6,L_FIELD_VALUES)]
    ]

    cards_vector = zeros(Float32, 55)
    for player_card in state.player_cards[:, state.curplayer]
        positions = findall(isequal(player_card), CARD_VECTOR_ORDER)
        for pos in positions
            if cards_vector[pos] == 0.0
                cards_vector[pos] = 1.0
                break
            end
        end
    end
    return (board_vector, cards_vector)
end