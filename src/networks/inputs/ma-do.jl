using AlphaZero
using Just4Fun: SIDE_LENGTH, NUM_PLAYERS, FIELD_VALUES, has_majority, dominant_player


@enum Layers majority dominance

"""
Retruns the inputs to the neural network.

NOTE: modified copy of cardfieldnet's input file with the same name.
"""
function GI.vectorize_state(spec::Just4FunSpec, state)
    board_vector = Float32[
        layer == majority       ? has_majority(state.field_stones[y, x, :], state.curplayer) :
        layer == dominance      ? dominant_player(state.field_stones[y, x, :]) == state.curplayer :
        throw(AssertionError("Unhandled layer $index !"))
        for y in 1:SIDE_LENGTH,
            x in 1:SIDE_LENGTH,
            layer in instances(Layers)
    ]
    return board_vector
end