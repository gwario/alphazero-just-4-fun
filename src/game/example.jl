gspec = Just4FunSpec()

#####
##### Training hyperparameters
#####

Network = NetLib.ResNet

const USE_GPU = false

netparams = NetLib.ResNetHP(
  num_filters=3,
  num_blocks=4,
  conv_kernel_size=(3, 3),
  num_policy_head_filters=2,
  num_value_head_filters=2,
  batch_norm_momentum=0.1)

self_play_params = SelfPlayParams(
  sim=SimParams(
    num_games=8,
    num_workers=8,
    batch_size=8,
    use_gpu=USE_GPU,
    reset_every=2,
    flip_probability=0.,
    alternate_colors=false),
  mcts=MctsParams(
    num_iters_per_turn=40,
    cpuct=2.0,
    prior_temperature=1.0,
    temperature=PLSchedule([0, 20, 30], [1.0, 1.0, 0.3]),
    dirichlet_noise_ϵ=0.25,
    dirichlet_noise_α=1.0))

arena_params = ArenaParams(
  sim=SimParams(
    num_games=8,
    num_workers=8,
    batch_size=8,
    use_gpu=USE_GPU,
    reset_every=2,
    flip_probability=0.,
    alternate_colors=true),
  mcts=MctsParams(
    self_play_params.mcts,
    temperature=ConstSchedule(0.2),
    dirichlet_noise_ϵ=0.05),
  update_threshold=0.05)

learning_params = LearningParams(
  use_gpu=USE_GPU,
  use_position_averaging=true,
  samples_weighing_policy=LOG_WEIGHT,
  batch_size=1024,
  loss_computation_batch_size=1024,
  optimiser=Network.Adam(lr=2e-3),
  l2_regularization=1e-4,
  nonvalidity_penalty=1.,
  min_checkpoints_per_epoch=1,
  max_batches_per_checkpoint=2000,
  num_checkpoints=1)

params = Params(
  arena=arena_params,
  self_play=self_play_params,
  learning=learning_params,
  num_iters=1,
  ternary_rewards=true,
  use_symmetries=true,
  memory_analysis=nothing,
  mem_buffer_size=PLSchedule(
  [      0,        15],
  [400_000, 1_000_000]))

#####
##### Evaluation benchmark
#####

mcts_baseline =
  Benchmark.MctsRollouts(
    MctsParams(
      arena_params.mcts,
      num_iters_per_turn=40,
      cpuct=1.))

minmax_baseline = Benchmark.MinMaxTS(
  depth=5,
  τ=0.2,
  amplify_rewards=true)

alphazero_player = Benchmark.Full(arena_params.mcts)

network_player = Benchmark.NetworkOnly(τ=0.5)

benchmark_sim = SimParams(
  arena_params.sim;
  num_games=16,
  num_workers=16,
  batch_size=16,
  alternate_colors=false)

benchmark = [
  Benchmark.Duel(alphazero_player, mcts_baseline,   benchmark_sim),
# Benchmark.Duel(alphazero_player, minmax_baseline, benchmark_sim),
  Benchmark.Duel(network_player,   mcts_baseline,   benchmark_sim),
# Benchmark.Duel(network_player,   minmax_baseline, benchmark_sim)
]

#####
##### Wrapping up in an experiment
#####

experiment = Experiment(
  "just4fun",
  Just4FunSpec(),
  params, Network, netparams, benchmark
)