@info ""
@info "Just4Fun.jl"
@info ""
module Just4Fun

export Just4FunEnv, Just4FunSpec, CardValue, Cards, Player, Stones, Deck, FieldValue, Action, CardsAction, NoCardsAction, USE_GPU

using Combinatorics
using DataStructures
using StaticArrays
using AlphaZero
using AlphaZero: Benchmark

# loading the game implementation
include("./game/main.jl")

# TODO: for integration with AlphaZero.jl
#module Training
#  using AlphaZero
#  import ..Just4FunSpec
#  include("./game/example.jl")
#end

using Base
import Random
using Logging
using Plots
using PrettyPrinting

plotlyjs()


include("../test/utils/utils.jl")

include("./experiment.jl")

include("../test/utils/scripts.jl")
include("../test/utils/run_modes.jl")
include("../test/utils/report.jl")
include("../test/utils/benchmark.jl")
include("../test/utils/ui/explorer.jl")
include("../test/utils/ui/plots.jl")
include("../test/utils/ui/session.jl")

const USE_GPU = true
const SEED = 1234

# The seed is set on game initialization and can be used to reduce the amount of different game executions
# This might get modified in experiments
const PER_GAME_SEEDS = []

# General note on samples: One game in self-play results in 14.52 samples on average (taken of 100 games at 25 rollouts)
# resnet
#const active_experiment = Experiments.ResNet.test # works
#const active_experiment = Experiments.ResNet.v0_0_1_multiple_seeds
#const active_experiment = Experiments.ResNet.v0_0_4_perf
#const active_experiment = Experiments.ResNet.v0_0_5

# TODO: redo with new benchamrk settings
#const active_experiment = Experiments.ResNet.v0_0_5_st_fv # not so good (no full bebchmark)
# TODO: redo with new benchamrk settings
#const active_experiment = Experiments.ResNet.v0_0_5_st_fv_pl # better than random (no full bebchmark)

# TODO: redo with new benchamrk settings
#const active_experiment = Experiments.ResNet.v0_0_5_ma_pl # gets worse for the first 30 iterations then shows signs of a change of trend

#const active_experiment = Experiments.ResNet.v0_1_0_ma_do_fv
## DONE #
#const active_experiment = Experiments.ResNet.v0_0_5_ma_do # get worse during the first 10 iterations, then gets significantly better and peaks apparently at iteration 60
const active_experiment = Experiments.ResNet.v0_1_0_ma_do

#########
#const active_experiment = Experiments.ResNet.v0_0_5_ma_do_fv

#const active_experiment = Experiments.ResNet.v0_0_5_st_ma_do_fv
#const active_experiment = Experiments.ResNet.v0_0_5_st_ma_do_fv_pl

# field output
#const active_experiment = Experiments.FieldNet.test # works
#const active_experiment = Experiments.FieldNet.v0_2_2_single_seed # does not work - StackOverflowError
#const active_experiment = Experiments.FieldNet.v0_3_0 # does not work
# card input
#const active_experiment = Experiments.CardNet.test # works
#const active_experiment = Experiments.CardNet.v0_1_0 # works
#const active_experiment = Experiments.CardNet.v0_1_1 # does not work - StackOverflowError
# field card input
#const active_experiment = Experiments.CardFieldNet.test # works
#const active_experiment = Experiments.CardFieldNet.v1_2_0
#const active_experiment = Experiments.CardFieldNet.v1_2_1
#const active_experiment = Experiments.CardFieldNet.v1_3_0 #- no nw op
#const active_experiment = Experiments.CardFieldNet.v1_3_1 #- bias tow 1
#const active_experiment = Experiments.CardFieldNet.v1_3_2
#const active_experiment = Experiments.CardFieldNet.v1_4_0_ma_do_ao
#const active_experiment = Experiments.CardFieldNet.v1_4_0_ma_do_fv_ao
#const active_experiment = Experiments.CardFieldNet.v1_4_0_ma_do_pl_fv_ao --bug fxed-try sgsin
#const active_experiment = Experiments.CardFieldNet.v1_4_0_ma_do_pl_ao -- not learning

active_experiment.load()


#####
##### Evaluation benchmark
#####

# baseline slightly more mcts iterations than training and arena_params... az is still expected to be much stronger now (if nn gives an advantage)
const mcts_baseline = Benchmark.MctsRollouts(
  MctsParams(
    active_experiment.self_play_params.mcts,
    num_iters_per_turn=10,
    cpuct=1.
  )
)

const alphazero_player = Benchmark.Full(active_experiment.self_play_params.mcts)

const network_player = Benchmark.NetworkOnly()

const random_player = Benchmark.RandomPlayer()


const benchmark_sim = SimParams(
  active_experiment.arena_params.sim;
  num_games=1000,
  num_workers=64,
  batch_size=64
)

const benchmark = [
  #Benchmark.Duel(alphazero_player,  random_player,   benchmark_sim),
  #Benchmark.Duel(random_player,     random_player,   benchmark_sim),
  Benchmark.Duel(network_player,    random_player,   benchmark_sim),
  #Benchmark.Duel(network_player,    network_player,  benchmark_sim),
  #Benchmark.Duel(mcts_baseline,     random_player,   benchmark_sim),
  #Benchmark.Duel(minmax_baseline,   random_player,   benchmark_sim),
  ##Benchmark.Duel(alphazero_player,  mcts_baseline,   benchmark_sim),
  ##Benchmark.Duel(network_player,    mcts_baseline,   benchmark_sim),
  #Benchmark.Duel(alphazero_player, minmax_baseline, benchmark_sim),
  #Benchmark.Duel(network_player,   minmax_baseline, benchmark_sim)
]


benchmark_stats = Vector{BenchmarkStats}()

# Interactive
function run()
  @info "$(StackTraces.stacktrace()[2].func)() from $(pwd())"
  @info ""
  print_mode()
  print_experiment(active_experiment)
  
  set_seed!(SEED)

  operations = target_opperation()
  
  session = execute(operations, active_experiment, benchmark, mode(), mode_param())
  
  # test_game, unit and e2e tests doe't return a session
  if typeof(session) == Session
    print_stats(session)
  end
end

function run_tests()
  @info "$(StackTraces.stacktrace()[2].func)() from $(pwd())"
  @info ""
  print_mode()
  print_experiment(active_experiment)

  if target() in ["train", "play"]
    set_seed!(SEED)
  end

  operations = target_opperation()
  
  # assumed to be called from PROJECT_ROOT
  session = execute(operations, active_experiment, benchmark, mode(), mode_param())
  
  # test_game, unit and e2e tests doe't return a session
  if typeof(session) == Session
    print_stats(session)
  end
end

function print_stats(session::Session)
  Log.section(session.logger, 1, "Overall stats:")
  print_stats(session.logger, benchmark_stats)
end

end
