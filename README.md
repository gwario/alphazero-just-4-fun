# AlphaZero Just 4 Fun

Implementation of game play agent for the game Just 4 Fun using the AlphaZero.jl implementation. 

**DEPRECATION NOTICE:** This package is deprecated in favour of a more modular approach. It's been replaced by [Just4Fun.jl](https://gitlab.com/gwario/Just4Fun.jl), [AlphaZeroJust4Fun.jl](https://gitlab.com/gwario/AlphaZeroJust4Fun.jl) and [AlphaZeroJust4FunNetworking.jl](https://gitlab.com/gwario/AlphaZeroJust4FunNetworking.jl)


# Preparation

1. Clone this repo
If project manifest not present or needs to be created:
2. cd to this repo and run `julia --project` 
3. type `]` to go to the pkg repl
4. add the modified version of AlphaZero.jl by typing `add https://github.com/gwario/AlphaZero.jl#Just4Fun_new`

# How to Run Tests

All test: `./scripts/just4fun_run_tests.sh`

Framework tests: `./scripts/just4fun_1_test.sh`
Custom Unit tests: `./scripts/just4fun_2_test-custom-unit.sh`
Custom e2e tests: `./scripts/just4fun_3_test-custom-integration.sh`
Run all Custom tests: `./scripts/just4fun_23_test-custom.sh`
Dummy run: `./scripts/just4fun_4_dummy-run.sh`
Gradient update tests: `./scripts/just4fun_5_test-grad-updates.sh`

# How to Run Training with profiling using julia and coverage

The following scripts support julia memory analysis:

* `./scripts/just4fun_1_test.sh`
* `./scripts/just4fun_2_test-custom-unit.sh`
* `./scripts/just4fun_3_test-custom-integration.sh`
* `./scripts/just4fun_23_test-custom.sh`
* `./scripts/just4fun_pkg-test.sh`
* `./scripts/just4fun_run_tests.sh`
* `./scripts/just4fun_train.sh`

To collect memory allocation information, add the parameter `"mem-analysis"` when running a script.
Keep in mind, that this will take significantly longer!
For the custom unit test, it takes 8.01 times more CPU time and 8.15 times more wall clock time.
For train with experiment v0.0.1-example it even takes 46 times more CPU time than witout allocation tracking.

# How to Run Tests Manually

Run `julia --compiled-modules=yes --project -e 'import Pkg; Pkg.instantiate(); import Just4Fun; Just4Fun.run_tests()' [<target> [<mode> [<param>]]]`

Targets are `"all"`, `"framework"`, `"unit"`, `"integration"`, `"dummy-run"`, `"grad-updates"`
Modes are `"benchmark"`, `"time-allocated"`, `"profile"`.
Mode params for `"profile"`: `"interactive"`, `"text"`, `"svg"`, `"html"`.
Mode params for `"benchmark"`: `<#evaluations>`-

# How to Run game agains an ai opponent 

Run `julia --compiled-modules=yes --project -e 'import Pkg; Pkg.instantiate(); import Just4Fun; Just4Fun.run()' [<target> [<mode> [<param>]]]`
The params set in `experiments/experiment.jl` will be used.

Targets are `"play"`, `"train"`, `"explore"`
Modes are `"benchmark"`, `"time-allocated"`, `"profile"`.
Mode params for `"profile"`: `"interactive"`, `"text"`, `"svg"`, `"html"`.
Mode params for `"benchmark"`: `<#evaluations>`-

# Troubleshooting

TODO
